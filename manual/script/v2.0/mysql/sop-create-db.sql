/*
Navicat MySQL Data Transfer

Source Server         : bpm
Source Server Version : 50716
Source Host           : 120.77.241.255:3306
Source Database       : sop-db

Target Server Type    : MYSQL
Target Server Version : 50716
File Encoding         : 65001

Date: 2020-12-14 09:00:03
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for bpa_base_info
-- ----------------------------
DROP TABLE IF EXISTS `bpa_base_info`;
CREATE TABLE `bpa_base_info` (
  `bpa_id` varchar(32) NOT NULL COMMENT '批次标识',
  `bpa_name` varchar(40) DEFAULT NULL COMMENT '批次名称',
  `subs_code` varchar(16) DEFAULT NULL COMMENT '子系统代码',
  `launch_type` char(1) DEFAULT NULL COMMENT '发起方式',
  `bpa_cron_value` varchar(40) DEFAULT NULL COMMENT '定时周期',
  `valid_date` char(10) DEFAULT NULL COMMENT '生效日期',
  `equally_task_amount` int(11) DEFAULT NULL COMMENT '任务并发数',
  `remark` varchar(254) DEFAULT NULL COMMENT '备注',
  `is_run_again` char(1) DEFAULT NULL COMMENT '是否允许重跑',
  `agent_id` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`bpa_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='01.批次信息配置';

-- ----------------------------
-- Table structure for bpa_inst_info
-- ----------------------------
DROP TABLE IF EXISTS `bpa_inst_info`;
CREATE TABLE `bpa_inst_info` (
  `bpa_serial_no` varchar(32) NOT NULL COMMENT '批次流水号',
  `bpa_id` varchar(32) DEFAULT NULL COMMENT '批次标识',
  `bpa_order` int(11) DEFAULT NULL COMMENT '批次序号',
  `bpa_name` varchar(40) DEFAULT NULL COMMENT '批次名称',
  `bpa_date` char(10) DEFAULT NULL COMMENT '批次日期',
  `subs_code` varchar(16) DEFAULT NULL COMMENT '子系统代码',
  `stage_id` varchar(24) DEFAULT NULL COMMENT '阶段编号',
  `stage_name` varchar(40) DEFAULT NULL COMMENT '阶段名称',
  `need_run_count` int(11) DEFAULT NULL COMMENT '需调度任务数',
  `exist_run_count` int(11) DEFAULT NULL COMMENT '运行中任务数',
  `not_run_count` int(11) DEFAULT NULL COMMENT '未运行任务数',
  `succeed_run_count` int(11) DEFAULT NULL COMMENT '运行成功任务数',
  `faild_run_count` int(11) DEFAULT NULL COMMENT '运行失败任务数',
  `warn_run_count` int(11) DEFAULT NULL COMMENT '运行警告任务数',
  `skip_run_count` int(11) DEFAULT NULL COMMENT '运行置过任务数',
  `start_time` varchar(24) DEFAULT NULL COMMENT '启动时间',
  `end_time` varchar(24) DEFAULT NULL COMMENT '结束时间',
  `bpa_state` char(1) DEFAULT NULL COMMENT '批次状态',
  `bpa_intervene_state` char(1) DEFAULT NULL COMMENT '干预状态',
  `cost_time` decimal(10,2) DEFAULT NULL COMMENT '运行时长',
  `remark` varchar(254) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`bpa_serial_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='01.批次实例信息';

-- ----------------------------
-- Table structure for bpa_inst_info_h
-- ----------------------------
DROP TABLE IF EXISTS `bpa_inst_info_h`;
CREATE TABLE `bpa_inst_info_h` (
  `bpa_serial_no` varchar(32) NOT NULL COMMENT '批次流水号',
  `bpa_id` varchar(32) DEFAULT NULL COMMENT '批次标识',
  `bpa_order` int(11) DEFAULT NULL COMMENT '批次序号',
  `bpa_name` varchar(40) DEFAULT NULL COMMENT '批次名称',
  `bpa_date` char(10) DEFAULT NULL COMMENT '批次日期',
  `subs_code` varchar(16) DEFAULT NULL COMMENT '子系统代码',
  `stage_id` varchar(24) DEFAULT NULL COMMENT '阶段编号',
  `stage_name` varchar(40) DEFAULT NULL COMMENT '阶段名称',
  `need_run_count` int(11) DEFAULT NULL COMMENT '需调度任务数',
  `exist_run_count` int(11) DEFAULT NULL COMMENT '运行中任务数',
  `not_run_count` int(11) DEFAULT NULL COMMENT '未运行任务数',
  `succeed_run_count` int(11) DEFAULT NULL COMMENT '运行成功任务数',
  `faild_run_count` int(11) DEFAULT NULL COMMENT '运行失败任务数',
  `warn_run_count` int(11) DEFAULT NULL COMMENT '运行警告任务数',
  `skip_run_count` int(11) DEFAULT NULL COMMENT '运行置过任务数',
  `start_time` varchar(24) DEFAULT NULL COMMENT '启动时间',
  `end_time` varchar(24) DEFAULT NULL COMMENT '结束时间',
  `bpa_state` char(1) DEFAULT NULL COMMENT '批次状态',
  `bpa_intervene_state` char(1) DEFAULT NULL COMMENT '干预状态',
  `cost_time` decimal(10,2) DEFAULT NULL COMMENT '运行时长',
  `remark` varchar(254) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`bpa_serial_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='01.批次历史信息';

-- ----------------------------
-- Table structure for bpa_inst_task
-- ----------------------------
DROP TABLE IF EXISTS `bpa_inst_task`;
CREATE TABLE `bpa_inst_task` (
  `bpa_serial_no` varchar(32) NOT NULL COMMENT '批次流水号',
  `task_id` varchar(32) NOT NULL COMMENT '任务编号',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称',
  `bpa_id` varchar(32) DEFAULT NULL COMMENT '批次标识',
  `bpa_date` char(10) DEFAULT NULL COMMENT '批次日期',
  `bpa_order` int(11) DEFAULT NULL COMMENT '批次序号',
  `stage_id` varchar(24) DEFAULT NULL COMMENT '阶段编号',
  `stage_name` varchar(40) DEFAULT NULL COMMENT '阶段名称',
  `warn_count` int(11) DEFAULT NULL COMMENT '警告次数',
  `start_time` varchar(24) DEFAULT NULL COMMENT '启动时间',
  `end_time` varchar(24) DEFAULT NULL COMMENT '结束时间',
  `cost_time` decimal(10,2) DEFAULT NULL COMMENT '运行时长',
  `task_run_state` char(1) DEFAULT NULL COMMENT '任务执行状态',
  `task_intervene_state` char(1) DEFAULT NULL COMMENT '任务干预状态',
  `state_desc` varchar(254) DEFAULT NULL COMMENT '状态描述',
  `other` varchar(40) DEFAULT NULL COMMENT '其它',
  `plugin_param` text COMMENT '年龄',
  PRIMARY KEY (`bpa_serial_no`,`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='02.批次任务实例';

-- ----------------------------
-- Table structure for bpa_inst_task_h
-- ----------------------------
DROP TABLE IF EXISTS `bpa_inst_task_h`;
CREATE TABLE `bpa_inst_task_h` (
  `bpa_serial_no` varchar(32) NOT NULL COMMENT '批次流水号',
  `task_id` varchar(32) NOT NULL COMMENT '任务编号',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称',
  `bpa_id` varchar(32) DEFAULT NULL COMMENT '批次标识',
  `bpa_date` char(10) DEFAULT NULL COMMENT '批次日期',
  `bpa_order` int(11) DEFAULT NULL COMMENT '批次序号',
  `stage_id` varchar(24) DEFAULT NULL COMMENT '阶段编号',
  `stage_name` varchar(40) DEFAULT NULL COMMENT '阶段名称',
  `warn_count` int(11) DEFAULT NULL COMMENT '警告次数',
  `start_time` varchar(24) DEFAULT NULL COMMENT '启动时间',
  `end_time` varchar(24) DEFAULT NULL COMMENT '结束时间',
  `cost_time` decimal(10,2) DEFAULT NULL COMMENT '运行时长',
  `task_run_state` char(1) DEFAULT NULL COMMENT '任务执行状态',
  `task_intervene_state` char(1) DEFAULT NULL COMMENT '任务干预状态',
  `state_desc` varchar(254) DEFAULT NULL COMMENT '状态描述',
  `other` varchar(40) DEFAULT NULL COMMENT '其它',
  `plugin_param` text COMMENT '年龄',
  PRIMARY KEY (`bpa_serial_no`,`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='02.批次任务历史';

-- ----------------------------
-- Table structure for bpa_stage_info
-- ----------------------------
DROP TABLE IF EXISTS `bpa_stage_info`;
CREATE TABLE `bpa_stage_info` (
  `stage_id` varchar(24) NOT NULL COMMENT '阶段编号',
  `stage_name` varchar(40) DEFAULT NULL COMMENT '阶段名称',
  `bpa_id` varchar(32) NOT NULL COMMENT '批次标识',
  `remark` varchar(254) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`stage_id`,`bpa_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='01.批次任务阶段';

-- ----------------------------
-- Table structure for bpa_task_info
-- ----------------------------
DROP TABLE IF EXISTS `bpa_task_info`;
CREATE TABLE `bpa_task_info` (
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称',
  `task_id` varchar(32) NOT NULL,
  `previous_task_id` varchar(24) DEFAULT NULL COMMENT '前一任务编号',
  `bpa_id` varchar(32) DEFAULT NULL COMMENT '批次标识',
  `stage_id` varchar(24) DEFAULT NULL COMMENT '阶段编号',
  `locale_id` varchar(24) DEFAULT NULL COMMENT '执行场所编号',
  `plugin_id` int(11) DEFAULT NULL COMMENT '应用插件id',
  `plugin_source_type` char(1) DEFAULT NULL COMMENT '插件配置数据来源方式',
  `plugin_para_flag` varchar(40) DEFAULT NULL COMMENT '插件配置数据标识',
  `task_pri` char(1) DEFAULT NULL COMMENT '任务优先级',
  `task_run_type` char(1) DEFAULT NULL COMMENT '任务执行类型',
  `task_cycle_type` char(1) DEFAULT NULL COMMENT '任务周期类型',
  `task_cron_value` varchar(40) DEFAULT NULL COMMENT '任务定时周期',
  `task_delay_time` int(11) DEFAULT NULL COMMENT '任务延时执行时间(秒)',
  `task_skip_tactic` char(1) DEFAULT NULL COMMENT '任务失败跳过策略',
  `again_run_space` int(11) DEFAULT NULL COMMENT '任务重复调起时间间隔(秒)',
  `task_estimate_time` int(11) DEFAULT NULL COMMENT '任务预计执行时间(秒)',
  `task_use_state` char(1) DEFAULT NULL COMMENT '任务使用状态',
  `remark` varchar(254) DEFAULT NULL COMMENT '备注',
  `max_run_count` int(11) DEFAULT NULL COMMENT '任务最大重复调起次数',
  `task_timeout_time` int(11) DEFAULT NULL COMMENT '任务运行超时时间(秒)',
  `task_timeout_tactic` char(1) DEFAULT NULL COMMENT '任务运行超时策略',
  `subs_ds_code` varchar(40) DEFAULT NULL COMMENT '子系统数据源代码',
  `task_use_area` char(1) DEFAULT NULL COMMENT '任务使用场景',
  `plugin_type` char(1) DEFAULT NULL COMMENT '应用插件类型',
  `max_wait_time` int(11) DEFAULT NULL COMMENT '最长等待时间',
  `cycle_inteval` int(11) DEFAULT NULL COMMENT '轮询间隔(秒)',
  `plugin_param` text COMMENT '插件参数',
  PRIMARY KEY (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='02.批次任务配置';

-- ----------------------------
-- Table structure for bpm_base_info
-- ----------------------------
DROP TABLE IF EXISTS `bpm_base_info`;
CREATE TABLE `bpm_base_info` (
  `bpm_id` varchar(32) NOT NULL COMMENT '流程标识',
  `bpm_name` varchar(80) DEFAULT NULL COMMENT '流程名称',
  `equally_task_amount` int(11) DEFAULT NULL COMMENT '任务并发数',
  `is_run_again` char(1) DEFAULT NULL COMMENT '是否',
  `table_model_id` varchar(40) DEFAULT NULL COMMENT '表模型ID',
  `table_model_code` varchar(40) DEFAULT NULL COMMENT '表模型代码',
  `table_model_name` varchar(80) DEFAULT NULL COMMENT '表模型名称，中文名称',
  `create_user` varchar(64) DEFAULT NULL COMMENT 'pre_nls_credit_info',
  `create_time` varchar(24) DEFAULT NULL COMMENT 'pre_nls_credit_info',
  `last_update_user` varchar(64) DEFAULT NULL COMMENT 'pre_nls_credit_info',
  `last_update_time` varchar(24) DEFAULT NULL COMMENT 'coopr_org_info',
  `bpm_type` varchar(1) DEFAULT NULL COMMENT '1、自动流程 2、审批流程',
  `bpm_version` varchar(8) DEFAULT '0.0.1',
  `bpm_state` char(1) DEFAULT NULL COMMENT '0、未发布 1、已发布',
  `subs_code` varchar(16) NOT NULL COMMENT '子系统代码',
  `category_tag` varchar(16) DEFAULT NULL COMMENT '分类标签',
  `agent_id` varchar(80) DEFAULT NULL,
  `bpm_code` varchar(32) DEFAULT NULL,
  `effect_state` char(1) DEFAULT NULL,
  `latest_state` char(1) DEFAULT NULL,
  PRIMARY KEY (`bpm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='流程基本信息';

-- ----------------------------
-- Table structure for bpm_linked_info
-- ----------------------------
DROP TABLE IF EXISTS `bpm_linked_info`;
CREATE TABLE `bpm_linked_info` (
  `linked_id` varchar(32) NOT NULL COMMENT '链接标识',
  `linked_type` char(3) DEFAULT NULL COMMENT '链接类型',
  `source_node_id` varchar(32) DEFAULT NULL COMMENT '源节点标识',
  `target_node_id` varchar(32) DEFAULT NULL COMMENT '目标节点标识',
  `linked_desc` varchar(80) DEFAULT NULL COMMENT '链接描述',
  `x1` int(11) DEFAULT NULL COMMENT '坐标1X轴',
  `y1` int(11) DEFAULT NULL COMMENT '坐标1Y轴',
  `x2` int(11) DEFAULT NULL COMMENT '坐标2X轴',
  `y2` int(11) DEFAULT NULL COMMENT '坐标2Y轴',
  PRIMARY KEY (`linked_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='流程链接信息';

-- ----------------------------
-- Table structure for bpm_node_info
-- ----------------------------
DROP TABLE IF EXISTS `bpm_node_info`;
CREATE TABLE `bpm_node_info` (
  `bpm_node_id` varchar(32) NOT NULL COMMENT '流程节点标识',
  `bpm_node_code` varchar(40) DEFAULT NULL COMMENT '流程节点代码',
  `bpm_node_name` varchar(80) DEFAULT NULL COMMENT '流程节点名称',
  `bpm_node_type` char(3) DEFAULT NULL COMMENT '流程节点类型',
  `bpm_id` varchar(32) DEFAULT NULL COMMENT '流程标识',
  `x` int(11) DEFAULT NULL COMMENT 'X坐标',
  `y` int(11) DEFAULT NULL COMMENT 'Y坐标',
  `h` int(11) DEFAULT NULL COMMENT 'H高',
  `w` int(11) DEFAULT NULL COMMENT 'W宽',
  `bpm_node_param` text,
  PRIMARY KEY (`bpm_node_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='流程节点信息';

-- ----------------------------
-- Table structure for dbo_backup_conf
-- ----------------------------
DROP TABLE IF EXISTS `dbo_backup_conf`;
CREATE TABLE `dbo_backup_conf` (
  `base_id` varchar(32) NOT NULL COMMENT 'mysql数据库IP',
  `database_name` varchar(16) NOT NULL COMMENT ' 数据库名称',
  `username` varchar(16) NOT NULL COMMENT '用户名',
  `password` varchar(40) NOT NULL COMMENT '用户密码',
  `save_path` varchar(40) NOT NULL COMMENT '备份路径',
  `job_code` varchar(40) NOT NULL COMMENT '定时作业编号',
  `subs_code` varchar(16) DEFAULT NULL COMMENT '所属系统',
  PRIMARY KEY (`job_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='数据运维定时备份配置';

-- ----------------------------
-- Table structure for dbo_sql_log
-- ----------------------------
DROP TABLE IF EXISTS `dbo_sql_log`;
CREATE TABLE `dbo_sql_log` (
  `record_keyid` varchar(32) NOT NULL COMMENT '数据记录标识',
  `dbo_sql_content` text COMMENT '数据运维语句',
  `dbo_result` char(1) DEFAULT NULL COMMENT '数据运维执行结果,Y：成功，N：失败',
  `dbo_affected_num` int(11) DEFAULT NULL COMMENT '数据运维影响记录数',
  `dbo_err_msg` longtext COMMENT '数据运维错误信息',
  `start_time` varchar(24) DEFAULT NULL COMMENT '开始时间',
  `end_time` varchar(24) DEFAULT NULL COMMENT '结束时间',
  `cost_time` decimal(10,2) DEFAULT NULL COMMENT '执行时长（秒）',
  `oper_userid` varchar(32) DEFAULT NULL COMMENT '操作人',
  `ds_code` varchar(40) DEFAULT NULL COMMENT '数据源代码',
  `dbo_remarks` varchar(254) NOT NULL COMMENT '数据运维备注',
  PRIMARY KEY (`record_keyid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='数据运维日志';

-- ----------------------------
-- Table structure for ecs_audit_log
-- ----------------------------
DROP TABLE IF EXISTS `ecs_audit_log`;
CREATE TABLE `ecs_audit_log` (
  `record_keyid` varchar(32) NOT NULL COMMENT '记录唯一编号',
  `oper_time` varchar(24) DEFAULT NULL COMMENT '操作时间',
  `ecs_id` varchar(32) DEFAULT NULL COMMENT 'ecs标识',
  `ecs_oper_action` varchar(16) DEFAULT NULL COMMENT '服务器操作',
  `ecs_msg` varchar(254) DEFAULT NULL COMMENT '服务器响应信息',
  `oper_userid` varchar(32) DEFAULT NULL COMMENT '操作人',
  PRIMARY KEY (`record_keyid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='服务器操作日志';

-- ----------------------------
-- Table structure for ecs_base_info
-- ----------------------------
DROP TABLE IF EXISTS `ecs_base_info`;
CREATE TABLE `ecs_base_info` (
  `ecs_id` varchar(32) NOT NULL COMMENT 'ecs标识',
  `ecs_ip` varchar(24) DEFAULT NULL COMMENT '服务器ip',
  `ecs_name` varchar(40) DEFAULT NULL COMMENT '服务器名称',
  `ecs_desc` varchar(80) DEFAULT NULL COMMENT '描述',
  `ecs_region` varchar(16) DEFAULT NULL COMMENT '所在位置',
  `ecs_os` varchar(16) DEFAULT NULL COMMENT '操作系统',
  `ecs_vcpu` int(11) DEFAULT NULL COMMENT 'cpu(核)',
  `ecs_memory` int(11) DEFAULT NULL COMMENT '内存(g)',
  `ecs_disk` int(11) DEFAULT NULL COMMENT '磁盘(g)',
  `ecs_purpose` char(2) DEFAULT NULL COMMENT '服务器用途',
  `ecs_login_method` char(1) DEFAULT NULL COMMENT '服务器登录方式',
  `ecs_login_user` varchar(24) DEFAULT NULL COMMENT '服务器登录用户',
  `ecs_login_pwd` varchar(128) DEFAULT NULL COMMENT '服务器登录密码',
  `ecs_state` char(2) DEFAULT NULL COMMENT '状态',
  `ecs_msg` varchar(254) DEFAULT NULL COMMENT '服务器响应信息',
  PRIMARY KEY (`ecs_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='服务器信息';

-- ----------------------------
-- Table structure for ecs_port_info
-- ----------------------------
DROP TABLE IF EXISTS `ecs_port_info`;
CREATE TABLE `ecs_port_info` (
  `ecs_id` varchar(32) NOT NULL COMMENT 'ecs标识',
  `ecs_port` int(11) DEFAULT NULL COMMENT '服务器开放端口',
  `remarks` varchar(254) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`ecs_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='服务器开放端口';

-- ----------------------------
-- Table structure for plugin_approve_conf
-- ----------------------------
DROP TABLE IF EXISTS `plugin_approve_conf`;
CREATE TABLE `plugin_approve_conf` (
  `plugin_conf_id` varchar(32) NOT NULL COMMENT '配置标识符',
  `approve_org_id` varchar(32) DEFAULT NULL COMMENT '审批机构标识',
  `approve_org_name` varchar(40) DEFAULT NULL COMMENT '审批机构名称',
  `approve_duty_id` varchar(32) DEFAULT NULL COMMENT '审批岗位',
  `approve_duty_name` varchar(40) DEFAULT NULL COMMENT '审批岗位名称',
  `approve_user_id` varchar(32) DEFAULT NULL COMMENT '审批人标识',
  `approve_user_name` varchar(32) DEFAULT NULL COMMENT '审批人名称',
  `approve_avail_opinion` varchar(40) DEFAULT NULL COMMENT '可用审批意见',
  `copy_user_id` varchar(32) DEFAULT NULL COMMENT '抄送人标识',
  `copy_user_name` varchar(40) DEFAULT NULL COMMENT '抄送人名称',
  `copy_avail_opinion` varchar(40) DEFAULT NULL COMMENT '抄送人可用审批意见',
  `countersign_strategy` char(1) DEFAULT NULL COMMENT '会签策略',
  PRIMARY KEY (`plugin_conf_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='审批插件配置';

-- ----------------------------
-- Table structure for plugin_assign_conf
-- ----------------------------
DROP TABLE IF EXISTS `plugin_assign_conf`;
CREATE TABLE `plugin_assign_conf` (
  `plugin_conf_id` varchar(32) NOT NULL COMMENT '配置标识符',
  `assign_mode` char(1) DEFAULT NULL COMMENT '分单方式',
  `assign_duty_id` varchar(32) DEFAULT NULL COMMENT '认领岗位标识',
  `assign_duty_name` varchar(40) DEFAULT NULL COMMENT '认领岗位名称',
  `assign_rule_content` text COMMENT '分单规则',
  PRIMARY KEY (`plugin_conf_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='分单插件配置';

-- ----------------------------
-- Table structure for plugin_check_conf
-- ----------------------------
DROP TABLE IF EXISTS `plugin_check_conf`;
CREATE TABLE `plugin_check_conf` (
  `plugin_conf_id` varchar(24) NOT NULL COMMENT '配置标识符',
  `conf_sort` int(11) NOT NULL COMMENT '配置顺序',
  `check_item_name` varchar(64) DEFAULT NULL COMMENT '检查项名称',
  `check_item_sql` varchar(2000) DEFAULT NULL COMMENT '检查项sql',
  `check_suc_condition` varchar(254) DEFAULT NULL COMMENT '检查成功条件',
  `check_err_desc` varchar(254) DEFAULT NULL COMMENT '错误描述',
  `check_faild_deal` char(1) DEFAULT NULL COMMENT '检查失败处理方式',
  `valid_date` char(10) DEFAULT NULL COMMENT '生效日期',
  `invalid_date` char(10) DEFAULT NULL COMMENT '失效日期',
  `last_modify_date` char(10) DEFAULT NULL COMMENT '最后更新日期',
  PRIMARY KEY (`plugin_conf_id`,`conf_sort`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='01.数据检查配置表';

-- ----------------------------
-- Table structure for plugin_contention_conf
-- ----------------------------
DROP TABLE IF EXISTS `plugin_contention_conf`;
CREATE TABLE `plugin_contention_conf` (
  `plugin_conf_id` varchar(32) NOT NULL COMMENT '配置标识符',
  `contention_strategy` char(1) DEFAULT NULL COMMENT '竟办策略',
  PRIMARY KEY (`plugin_conf_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='竟办插件配置';

-- ----------------------------
-- Table structure for plugin_countersign_conf
-- ----------------------------
DROP TABLE IF EXISTS `plugin_countersign_conf`;
CREATE TABLE `plugin_countersign_conf` (
  `plugin_conf_id` varchar(32) NOT NULL COMMENT '配置标识符',
  `approve_org_id` varchar(32) DEFAULT NULL COMMENT '审批机构标识',
  `approve_org_name` varchar(40) DEFAULT NULL COMMENT '审批机构名称',
  `approve_duty_id` varchar(32) DEFAULT NULL COMMENT '审批岗位标识',
  `approve_duty_name` varchar(40) DEFAULT NULL COMMENT '审批岗位名称',
  `sponsor_user_id` varchar(32) DEFAULT NULL COMMENT '发起人标识',
  `sponsor_user_name` varchar(40) DEFAULT NULL COMMENT '发起人名称',
  `head_user_id` varchar(32) DEFAULT NULL COMMENT '牵头人标识',
  `head_user_name` varchar(40) DEFAULT NULL COMMENT '牵头人名称',
  `countersign_number` int(11) DEFAULT NULL COMMENT '会签人数',
  `vote_number` int(11) DEFAULT NULL COMMENT '投票人数',
  `agree_number` int(11) DEFAULT NULL COMMENT '同意票数',
  `negative_number` int(11) DEFAULT NULL COMMENT '反对票数',
  `reconsider_number` int(11) DEFAULT NULL COMMENT '复议票数',
  `restrict_days` int(11) DEFAULT NULL COMMENT '要求办理天数',
  PRIMARY KEY (`plugin_conf_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='会签插件配置';

-- ----------------------------
-- Table structure for plugin_define
-- ----------------------------
DROP TABLE IF EXISTS `plugin_define`;
CREATE TABLE `plugin_define` (
  `plugin_id` int(11) NOT NULL COMMENT '应用插件id',
  `plugin_name` varchar(64) DEFAULT NULL COMMENT '应用插件名称',
  `plugin_class` varchar(254) DEFAULT NULL COMMENT '应用插件实现类',
  `plugin_desc` varchar(254) DEFAULT NULL COMMENT '应用插件描述',
  `need_other_ds_var` char(1) DEFAULT NULL COMMENT '是否需要其它数据源变量',
  `plugin_config_url` varchar(64) DEFAULT NULL COMMENT '应用插件配置地址',
  `plugin_catalog` char(2) DEFAULT NULL COMMENT '应用插件分类',
  PRIMARY KEY (`plugin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='01.应用插件信息';

-- ----------------------------
-- Table structure for plugin_excel_conf
-- ----------------------------
DROP TABLE IF EXISTS `plugin_excel_conf`;
CREATE TABLE `plugin_excel_conf` (
  `plugin_conf_id` varchar(24) NOT NULL COMMENT '配置标识符',
  `conf_sort` int(11) NOT NULL COMMENT '配置顺序',
  `excel_oper_type` char(2) DEFAULT NULL COMMENT 'excel操作类型',
  `param_key_value` varchar(512) DEFAULT NULL COMMENT '参数键值对',
  `configure_file` varchar(128) DEFAULT NULL COMMENT 'excel操作配置文件',
  `faild_deal` char(1) DEFAULT NULL COMMENT '失败处理方式',
  `last_modify_date` char(10) DEFAULT NULL COMMENT '最后更新日期',
  PRIMARY KEY (`plugin_conf_id`,`conf_sort`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='01.excel操作配置表';

-- ----------------------------
-- Table structure for plugin_exec_log
-- ----------------------------
DROP TABLE IF EXISTS `plugin_exec_log`;
CREATE TABLE `plugin_exec_log` (
  `batch_sn` varchar(32) DEFAULT NULL COMMENT '批次流水号',
  `plugin_id` int(11) DEFAULT NULL COMMENT '应用插件id',
  `plugin_name` varchar(64) DEFAULT NULL COMMENT '应用插件名称',
  `action_name` varchar(254) DEFAULT NULL COMMENT '操作名称',
  `action_result` char(1) DEFAULT NULL COMMENT '操作结果',
  `action_detail_info` varchar(4000) DEFAULT NULL COMMENT '操作详情',
  `task_id` varchar(32) DEFAULT NULL COMMENT '任务编号',
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称',
  `record_time` varchar(24) DEFAULT NULL COMMENT '记录时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='02.应用插件执行日志';

-- ----------------------------
-- Table structure for plugin_exec_log_h
-- ----------------------------
DROP TABLE IF EXISTS `plugin_exec_log_h`;
CREATE TABLE `plugin_exec_log_h` (
  `batch_sn` varchar(32) DEFAULT NULL COMMENT '批次流水号',
  `plugin_id` int(11) DEFAULT NULL COMMENT '应用插件id',
  `plugin_name` varchar(64) DEFAULT NULL COMMENT '应用插件名称',
  `action_name` varchar(254) DEFAULT NULL COMMENT '操作名称',
  `action_result` char(1) DEFAULT NULL COMMENT '操作结果',
  `action_detail_info` varchar(4000) DEFAULT NULL COMMENT '操作详情',
  `task_id` varchar(32) DEFAULT NULL,
  `task_name` varchar(64) DEFAULT NULL COMMENT '任务名称',
  `record_time` varchar(24) DEFAULT NULL COMMENT '记录时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='02.应用插件执行日志历史';

-- ----------------------------
-- Table structure for plugin_export_conf
-- ----------------------------
DROP TABLE IF EXISTS `plugin_export_conf`;
CREATE TABLE `plugin_export_conf` (
  `plugin_conf_id` varchar(24) NOT NULL COMMENT '配置标识符',
  `conf_sort` int(11) NOT NULL COMMENT '配置顺序',
  `export_mode` char(2) DEFAULT NULL COMMENT '数据导出方式',
  `export_target` varchar(512) DEFAULT NULL COMMENT '数据表/sql语句',
  `export_to_file` varchar(100) DEFAULT NULL COMMENT '文件存放路径',
  `field_separator` varchar(16) DEFAULT NULL COMMENT '字段分隔符',
  `file_charset` varchar(16) DEFAULT NULL COMMENT '文件编码格式',
  `is_ok_file` varchar(10) DEFAULT NULL COMMENT '是否需要生成OK文件',
  `ok_file_full_name` varchar(255) DEFAULT NULL COMMENT 'OK文件存放路径名称',
  `ok_file_content` varchar(1024) DEFAULT NULL COMMENT 'OK文件内容配置',
  `is_deal_empty_str` varchar(10) DEFAULT NULL COMMENT '是否处理空字符串',
  `deal_empty_str` varchar(128) DEFAULT NULL COMMENT '空串替换值',
  PRIMARY KEY (`plugin_conf_id`,`conf_sort`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='01.导出为文本文件插件';

-- ----------------------------
-- Table structure for plugin_fileop_conf
-- ----------------------------
DROP TABLE IF EXISTS `plugin_fileop_conf`;
CREATE TABLE `plugin_fileop_conf` (
  `plugin_conf_id` varchar(24) NOT NULL COMMENT '配置标识符',
  `conf_sort` int(11) NOT NULL COMMENT '配置顺序',
  `file_op` char(2) DEFAULT NULL COMMENT '文件操作',
  `op_cycle_day` int(11) DEFAULT NULL COMMENT '操作周期',
  `last_op_date` char(10) DEFAULT NULL COMMENT '上次操作日期',
  `last_modify_date` char(10) DEFAULT NULL COMMENT '最后更新日期',
  `distance_day` int(11) DEFAULT NULL COMMENT '目标距离(天)',
  `file_target` varchar(128) DEFAULT NULL COMMENT '目标文件',
  PRIMARY KEY (`plugin_conf_id`,`conf_sort`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='01.文件操作配置';

-- ----------------------------
-- Table structure for plugin_ftp_conf
-- ----------------------------
DROP TABLE IF EXISTS `plugin_ftp_conf`;
CREATE TABLE `plugin_ftp_conf` (
  `plugin_conf_id` varchar(24) NOT NULL COMMENT '配置标识符',
  `conf_sort` int(11) NOT NULL COMMENT '配置顺序',
  `sftp_server_ip` varchar(32) DEFAULT NULL COMMENT '地址',
  `ftp_server_port` int(10) DEFAULT NULL COMMENT '端口',
  `ftp_server_user` varchar(200) DEFAULT NULL COMMENT '用户名',
  `ftp_server_pwd` varchar(200) DEFAULT NULL COMMENT '密码',
  `load_type` varchar(10) DEFAULT NULL COMMENT '操作类型',
  `remote_file_name` varchar(1000) DEFAULT NULL COMMENT '服务器文件名称',
  `local_file_name` varchar(1000) DEFAULT NULL COMMENT '本地文件名称',
  `ftp_file_path` varchar(500) DEFAULT NULL COMMENT '服务器文件路径',
  `local_file_path` varchar(500) DEFAULT NULL COMMENT '本地路径',
  `check_exist` varchar(10) DEFAULT NULL COMMENT '是否需要检查文件存在',
  `server_type` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`plugin_conf_id`,`conf_sort`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for plugin_job_conf
-- ----------------------------
DROP TABLE IF EXISTS `plugin_job_conf`;
CREATE TABLE `plugin_job_conf` (
  `plugin_conf_id` varchar(24) NOT NULL COMMENT '配置标识符',
  `conf_sort` int(11) NOT NULL COMMENT '配置顺序',
  `job_name` varchar(32) DEFAULT NULL COMMENT '作业名称',
  `sql_purpose` char(1) DEFAULT NULL COMMENT 'sql脚本用途',
  `sql_content` varchar(4000) DEFAULT NULL COMMENT 'sql脚本',
  `param_group_id` varchar(40) DEFAULT NULL COMMENT '参数组标识',
  `job_faild_deal` char(1) DEFAULT NULL COMMENT '作业失败处理方式',
  `last_modify_date` char(10) DEFAULT NULL COMMENT '最后更新日期',
  `job_implement` varchar(128) DEFAULT NULL COMMENT '作业实现',
  PRIMARY KEY (`plugin_conf_id`,`conf_sort`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='01.数据作业配置';

-- ----------------------------
-- Table structure for plugin_job_param
-- ----------------------------
DROP TABLE IF EXISTS `plugin_job_param`;
CREATE TABLE `plugin_job_param` (
  `param_group_id` varchar(40) NOT NULL COMMENT '参数组标识',
  `job_param_name` varchar(32) NOT NULL COMMENT '作业参数名称',
  `job_param_value` varchar(512) DEFAULT NULL COMMENT '作业参数值',
  PRIMARY KEY (`param_group_id`,`job_param_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='02.数据作业参数配置';

-- ----------------------------
-- Table structure for plugin_load_conf
-- ----------------------------
DROP TABLE IF EXISTS `plugin_load_conf`;
CREATE TABLE `plugin_load_conf` (
  `table_name` varchar(38) NOT NULL COMMENT '数据库表名',
  `table_cnname` varchar(80) DEFAULT NULL COMMENT '表中文名称',
  `table_type` char(1) DEFAULT NULL COMMENT '数据表类型',
  `table_load_mode` char(1) DEFAULT NULL COMMENT '数据装载方式',
  `up_sysname` varchar(80) DEFAULT NULL COMMENT '供数系统名称',
  `load_from_file` varchar(100) DEFAULT NULL COMMENT '文件路径',
  `file_row_flag` char(1) DEFAULT NULL COMMENT '是否允许条数为零',
  `load_warn_flag` char(1) DEFAULT NULL COMMENT '是否忽略警告',
  `diff_comp_method` char(2) DEFAULT NULL COMMENT '差异比较方式',
  `limit_percent` int(11) DEFAULT NULL COMMENT '比较阀值(%)',
  `diff_deal_method` char(1) DEFAULT NULL COMMENT '差异处理方式',
  `last_modify_date` char(10) DEFAULT NULL COMMENT '最后更新日期',
  `plugin_conf_id` varchar(24) NOT NULL COMMENT '配置标识符',
  `conf_sort` int(11) DEFAULT NULL COMMENT '配置顺序',
  `load_separator` varchar(16) DEFAULT NULL COMMENT '字段分隔符',
  `before_load_sql` varchar(2000) DEFAULT NULL COMMENT '装载前执行语句',
  `after_load_sql` varchar(2000) DEFAULT NULL COMMENT '装载后执行语句',
  `file_charset` varchar(16) DEFAULT NULL COMMENT '文件编码格式',
  `load_buffer_size` int(11) DEFAULT NULL COMMENT '缓冲区大小(k)',
  `load_faild_deal` char(1) DEFAULT NULL COMMENT '装载失败处理方式',
  `load_fields` varchar(2000) DEFAULT NULL COMMENT '指定字段',
  `create_table_ddl` varchar(2000) DEFAULT NULL COMMENT '建表语句',
  `is_deal_empty_str` varchar(10) DEFAULT NULL COMMENT '是否处理空字符串',
  `deal_empty_str` varchar(128) DEFAULT NULL COMMENT '空串替换值',
  `is_first_line` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`table_name`,`plugin_conf_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='01.数据装载配置表';

-- ----------------------------
-- Table structure for plugin_load_result
-- ----------------------------
DROP TABLE IF EXISTS `plugin_load_result`;
CREATE TABLE `plugin_load_result` (
  `batch_serial_no` varchar(32) NOT NULL COMMENT '批次流水号',
  `batch_date` char(10) DEFAULT NULL COMMENT '批次日期',
  `table_name` varchar(38) NOT NULL COMMENT '数据库表名',
  `table_cnname` varchar(80) DEFAULT NULL COMMENT '表中文名称',
  `table_type` char(1) DEFAULT NULL COMMENT '数据表类型',
  `up_sysname` varchar(80) DEFAULT NULL COMMENT '供数系统名称',
  `table_load_mode` char(1) DEFAULT NULL COMMENT '数据装载方式',
  `load_from_file` varchar(100) DEFAULT NULL COMMENT '文件路径',
  `file_size` decimal(10,2) DEFAULT NULL COMMENT '文件大小',
  `start_time` varchar(24) DEFAULT NULL COMMENT '启动时间',
  `end_time` varchar(24) DEFAULT NULL COMMENT '结束时间',
  `cost_time` decimal(10,2) DEFAULT NULL COMMENT '运行时长',
  `read_rows` int(11) DEFAULT NULL COMMENT '读入条数',
  `load_rows` int(11) DEFAULT NULL COMMENT '装入条数',
  `reject_rows` int(11) DEFAULT NULL COMMENT '拒绝条数',
  `remark` varchar(254) DEFAULT NULL COMMENT '备注',
  `load_result` char(1) DEFAULT NULL COMMENT '导数结果',
  `batch_id` varchar(24) DEFAULT NULL COMMENT '批次标识',
  PRIMARY KEY (`batch_serial_no`,`table_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='02.数据装载结果表';

-- ----------------------------
-- Table structure for plugin_param_template
-- ----------------------------
DROP TABLE IF EXISTS `plugin_param_template`;
CREATE TABLE `plugin_param_template` (
  `param_id` varchar(32) NOT NULL COMMENT '参数标识',
  `plugin_id` varchar(32) DEFAULT NULL COMMENT '插件ID',
  `param_category` varchar(8) DEFAULT NULL COMMENT '参数类别  KV-键值对 GRID-列表参数',
  `param_code` varchar(32) DEFAULT NULL COMMENT '参数代码',
  `param_name` varchar(64) DEFAULT NULL COMMENT '参数名称',
  `param_html_tag` char(2) DEFAULT NULL COMMENT '参数HTML标签  01-文本框 02-数字框 11-下拉框',
  `param_html_option` varchar(254) DEFAULT NULL COMMENT '参数html选项  下拉框的选项值',
  `param_order_id` int(11) DEFAULT NULL COMMENT '参数排序',
  `parent_param_id` varchar(32) DEFAULT NULL COMMENT '父参数标识  如果参数类别为“GRID”,则有多个参数可挂在其中一个grid上，形成层级结构',
  PRIMARY KEY (`param_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='插件参数模板';

-- ----------------------------
-- Table structure for plugin_service_conf
-- ----------------------------
DROP TABLE IF EXISTS `plugin_service_conf`;
CREATE TABLE `plugin_service_conf` (
  `plugin_conf_id` varchar(24) NOT NULL COMMENT '配置标识符',
  `conf_sort` int(11) NOT NULL COMMENT '配置顺序',
  `service_id` varchar(80) DEFAULT NULL COMMENT '服务ID',
  `service_name` varchar(128) DEFAULT NULL COMMENT '服务名称',
  `service_interface` varchar(128) DEFAULT NULL COMMENT '服务接口类',
  `service_method` varchar(128) DEFAULT NULL COMMENT '服务实现方法',
  `return_type` varchar(128) DEFAULT NULL COMMENT '方法返回类型',
  `param_group_id` varchar(40) DEFAULT NULL COMMENT '参数组标识',
  `version` varchar(8) DEFAULT NULL COMMENT '服务版本',
  `service_group` varchar(16) DEFAULT NULL COMMENT '服务组别',
  `timeout` int(11) DEFAULT NULL COMMENT '调用超时时间',
  PRIMARY KEY (`plugin_conf_id`,`conf_sort`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='服务调用插件';

-- ----------------------------
-- Table structure for plugin_service_param
-- ----------------------------
DROP TABLE IF EXISTS `plugin_service_param`;
CREATE TABLE `plugin_service_param` (
  `param_group_id` varchar(40) NOT NULL COMMENT '参数组标识',
  `service_param_name` varchar(32) NOT NULL COMMENT '服务参数名称',
  `service_param_type` varchar(128) NOT NULL COMMENT '服务参数类型',
  `service_param_value` varchar(512) DEFAULT NULL COMMENT '服务参数值',
  PRIMARY KEY (`param_group_id`,`service_param_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='服务参数配置';

-- ----------------------------
-- Table structure for plugin_sysupd_conf
-- ----------------------------
DROP TABLE IF EXISTS `plugin_sysupd_conf`;
CREATE TABLE `plugin_sysupd_conf` (
  `plugin_conf_id` varchar(24) NOT NULL COMMENT '配置标识符',
  `conf_sort` int(11) NOT NULL COMMENT '配置顺序',
  `sysupd_action` char(2) DEFAULT NULL COMMENT '系统信息更新动作',
  PRIMARY KEY (`plugin_conf_id`,`conf_sort`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='01.系统信息更新配置表';

-- ----------------------------
-- Table structure for plugin_wt_condition
-- ----------------------------
DROP TABLE IF EXISTS `plugin_wt_condition`;
CREATE TABLE `plugin_wt_condition` (
  `plugin_conf_id` varchar(24) NOT NULL COMMENT '配置标识符',
  `conf_sort` int(11) NOT NULL COMMENT '配置顺序',
  `check_item_name` varchar(64) DEFAULT NULL COMMENT '检查项名称',
  `check_item_sql` varchar(2000) DEFAULT NULL COMMENT '检查项sql',
  `check_suc_condition` varchar(254) DEFAULT NULL COMMENT '检查成功条件',
  `valid_date` char(10) DEFAULT NULL COMMENT '生效日期',
  `invalid_date` char(10) DEFAULT NULL COMMENT '失效日期',
  `last_modify_date` char(10) DEFAULT NULL COMMENT '最后更新日期',
  PRIMARY KEY (`plugin_conf_id`,`conf_sort`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='02.等待条件表';

-- ----------------------------
-- Table structure for plugin_wt_conf
-- ----------------------------
DROP TABLE IF EXISTS `plugin_wt_conf`;
CREATE TABLE `plugin_wt_conf` (
  `plugin_conf_id` varchar(24) NOT NULL COMMENT '配置标识符',
  `check_inteval` int(11) DEFAULT NULL COMMENT '等待间隔',
  `max_wait_time` int(11) DEFAULT NULL COMMENT '最长等待时间',
  `wait_desc` varchar(80) DEFAULT NULL COMMENT '等待描述',
  PRIMARY KEY (`plugin_conf_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='01.等待指定条件配置表';

-- ----------------------------
-- Table structure for pub_sys_info
-- ----------------------------
DROP TABLE IF EXISTS `pub_sys_info`;
CREATE TABLE `pub_sys_info` (
  `sys_id` varchar(32) NOT NULL COMMENT '系统编号',
  `sys_name` varchar(100) DEFAULT NULL COMMENT '系统名称',
  `headoffice` varchar(20) DEFAULT NULL COMMENT '未知',
  `progress` char(2) DEFAULT NULL COMMENT '未知',
  `openday` varchar(10) DEFAULT NULL COMMENT '营业日期',
  `last_openday` varchar(10) DEFAULT NULL COMMENT '上一营业日期',
  `effectivedays` decimal(22,0) DEFAULT NULL COMMENT '未知',
  `sys_status` varchar(2) DEFAULT NULL COMMENT '系统状态',
  `loginmode` char(1) DEFAULT NULL COMMENT '未知',
  `slmode` char(1) DEFAULT NULL COMMENT '未知',
  PRIMARY KEY (`sys_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统营业日期';

-- ----------------------------
-- Table structure for sys_apps_info
-- ----------------------------
DROP TABLE IF EXISTS `sys_apps_info`;
CREATE TABLE `sys_apps_info` (
  `app_id` varchar(32) NOT NULL COMMENT '应用唯一标识',
  `app_code` varchar(24) DEFAULT NULL COMMENT '应用代码',
  `app_name` varchar(80) DEFAULT NULL COMMENT '应用名称',
  `arch_type` char(1) DEFAULT NULL COMMENT '架构类型',
  `app_version` varchar(16) DEFAULT NULL COMMENT '应用版本',
  `app_order` int(11) DEFAULT NULL COMMENT '应用排列顺序，也是应用构建顺序',
  `git_url` varchar(128) NOT NULL COMMENT 'GIT仓库路径',
  `jdk_version` varchar(8) NOT NULL COMMENT 'JDK版本',
  `app_desc` varchar(254) NOT NULL COMMENT '应用说明',
  `subs_code` varchar(16) NOT NULL COMMENT '所属系统',
  `create_user` varchar(32) DEFAULT NULL COMMENT '创建人',
  `create_time` varchar(24) DEFAULT NULL COMMENT '创建时间',
  `last_update_user` varchar(24) DEFAULT NULL COMMENT 'lmt_adjust_third',
  `last_update_time` varchar(24) DEFAULT NULL COMMENT 'lmt_adjust_third',
  PRIMARY KEY (`app_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统应用信息';

-- ----------------------------
-- Table structure for sys_deploy_info
-- ----------------------------
DROP TABLE IF EXISTS `sys_deploy_info`;
CREATE TABLE `sys_deploy_info` (
  `app_deploy_id` varchar(32) NOT NULL COMMENT '应用部署信息唯一标识',
  `app_id` varchar(32) DEFAULT NULL COMMENT '移动应用唯一标识',
  `app_name` varchar(80) DEFAULT NULL COMMENT '移动应用名称',
  `app_cnname` varchar(80) DEFAULT NULL COMMENT '应用中文名称',
  `app_version` varchar(16) DEFAULT NULL COMMENT '应用版本',
  `app_type` char(3) DEFAULT NULL COMMENT '应用类型',
  `app_order` int(11) DEFAULT NULL COMMENT '应用排列顺序，也是应用构建顺序',
  `ecs_id` varchar(32) DEFAULT NULL COMMENT 'ecs标识',
  `ecs_ip` varchar(24) DEFAULT NULL COMMENT '服务器IP',
  `subs_code` varchar(16) NOT NULL COMMENT '所属系统',
  `ecs_name` varchar(40) DEFAULT NULL COMMENT '服务器名称',
  `ecs_region` varchar(16) DEFAULT NULL COMMENT '所在位置',
  `app_deploy_path` varchar(80) DEFAULT NULL COMMENT '应用部署目录',
  `app_log_path` varchar(80) DEFAULT NULL COMMENT '默认值为应用部署目录+应用名称+logs',
  `app_port` varchar(8) DEFAULT NULL COMMENT '应用端口',
  `app_state` char(1) DEFAULT NULL COMMENT '1、运行中，2、部署中，3、已停止',
  `oper_time` varchar(24) DEFAULT NULL COMMENT '操作时间',
  `oper_userid` varchar(32) DEFAULT NULL COMMENT '操作人',
  `ecs_login_user` varchar(24) DEFAULT NULL COMMENT '服务器登陆用户',
  `ecs_login_pwd` varchar(128) DEFAULT NULL COMMENT '服务器登陆密码',
  PRIMARY KEY (`app_deploy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统部署信息';

-- ----------------------------
-- Table structure for sys_ecs_info
-- ----------------------------
DROP TABLE IF EXISTS `sys_ecs_info`;
CREATE TABLE `sys_ecs_info` (
  `ecs_id` varchar(32) NOT NULL COMMENT 'ecs标识',
  `subs_code` varchar(16) NOT NULL COMMENT '所属系统',
  PRIMARY KEY (`ecs_id`,`subs_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统服务器信息';

-- ----------------------------
-- Table structure for s_agent_info
-- ----------------------------
DROP TABLE IF EXISTS `s_agent_info`;
CREATE TABLE `s_agent_info` (
  `agent_id` varchar(32) NOT NULL COMMENT '代理节点标识',
  `agent_name` varchar(80) DEFAULT NULL COMMENT '代理节点名称',
  `agent_state` char(1) DEFAULT NULL COMMENT '代理节点状态',
  `agent_url` varchar(128) DEFAULT NULL COMMENT '代理节点地址',
  `start_time` varchar(24) DEFAULT NULL COMMENT '启动时间',
  `stop_time` varchar(24) DEFAULT NULL COMMENT '停止时间',
  PRIMARY KEY (`agent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='代理节点信息';

-- ----------------------------
-- Table structure for s_autocode
-- ----------------------------
DROP TABLE IF EXISTS `s_autocode`;
CREATE TABLE `s_autocode` (
  `atype` varchar(20) DEFAULT NULL COMMENT '类型',
  `owner` varchar(20) DEFAULT NULL COMMENT '拥有者',
  `initcycle` char(1) DEFAULT NULL COMMENT '未知',
  `cur_sernum` varchar(50) DEFAULT NULL COMMENT '当前序号值',
  `zero_flg` char(1) DEFAULT NULL COMMENT '未知',
  `cur_date` varchar(10) DEFAULT NULL COMMENT '当前日期'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='自动序号';

-- ----------------------------
-- Table structure for s_deptuser
-- ----------------------------
DROP TABLE IF EXISTS `s_deptuser`;
CREATE TABLE `s_deptuser` (
  `organno` varchar(16) NOT NULL COMMENT '机构码',
  `depno` varchar(16) DEFAULT NULL COMMENT '部门码',
  `actorno` varchar(8) NOT NULL COMMENT '用户码',
  `state` decimal(22,0) DEFAULT NULL COMMENT '状态',
  `orgid` varchar(16) DEFAULT NULL COMMENT '组织号',
  PRIMARY KEY (`organno`,`actorno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户部门关系表';

-- ----------------------------
-- Table structure for s_dic
-- ----------------------------
DROP TABLE IF EXISTS `s_dic`;
CREATE TABLE `s_dic` (
  `enname` varchar(20) NOT NULL COMMENT '选项值',
  `cnname` varchar(100) DEFAULT NULL COMMENT '选项名称',
  `opttype` varchar(30) NOT NULL COMMENT '选项类别',
  `memo` varchar(100) DEFAULT NULL COMMENT '选项描述',
  `flag` varchar(3) DEFAULT NULL COMMENT '标志',
  `levels` varchar(2) DEFAULT NULL COMMENT '级别',
  `orderid` decimal(38,0) DEFAULT NULL COMMENT '排序字段',
  `modify_date` varchar(10) DEFAULT NULL COMMENT '修改日期',
  PRIMARY KEY (`enname`,`opttype`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='通用字典';

-- ----------------------------
-- Table structure for s_duty
-- ----------------------------
DROP TABLE IF EXISTS `s_duty`;
CREATE TABLE `s_duty` (
  `duty_code` varchar(24) NOT NULL COMMENT '岗位代码',
  `duty_name` varchar(880) DEFAULT NULL COMMENT '岗位名称',
  `legal_org_code` varchar(24) DEFAULT NULL COMMENT '机构法人代码',
  `order_id` int(8) DEFAULT NULL COMMENT '排序字段',
  `duty_type` varchar(150) DEFAULT NULL COMMENT '类型',
  `memo` varchar(80) DEFAULT NULL COMMENT '备注',
  `org_id` varchar(12) DEFAULT NULL COMMENT '组织号',
  `duty_level` char(1) DEFAULT NULL COMMENT '岗位层级',
  `duty_parent_code` varchar(24) DEFAULT NULL COMMENT '上级岗位号',
  `create_time` varchar(24) DEFAULT NULL COMMENT '创建日期',
  `create_user` varchar(24) DEFAULT NULL COMMENT '创建人',
  `last_update_user` varchar(24) DEFAULT NULL COMMENT '最后修改人',
  `last_update_time` varchar(24) DEFAULT NULL COMMENT '最后修改日期',
  `status` char(1) DEFAULT NULL COMMENT '状态',
  `create_org_code` varchar(24) DEFAULT NULL COMMENT '创建人所属机构',
  `org_code` varchar(24) DEFAULT NULL COMMENT '机构代码',
  PRIMARY KEY (`duty_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统岗位表';

-- ----------------------------
-- Table structure for s_inst_license
-- ----------------------------
DROP TABLE IF EXISTS `s_inst_license`;
CREATE TABLE `s_inst_license` (
  `host_name` varchar(24) DEFAULT NULL COMMENT '主机名/ip地址',
  `web_port` varchar(5) DEFAULT NULL COMMENT 'web端口',
  `sys_inst_id` varchar(48) NOT NULL COMMENT '系统实例标识',
  `sys_code` varchar(24) DEFAULT NULL COMMENT '系统代码',
  `sys_name` varchar(80) DEFAULT NULL COMMENT '系统名称',
  `auth_target` varchar(80) DEFAULT NULL COMMENT '授权目标',
  `auth_begin_date` char(10) DEFAULT NULL COMMENT '授权起始日期',
  `auth_end_date` char(10) DEFAULT NULL COMMENT '授权截止日期',
  `remark` varchar(254) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`sys_inst_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='01.系统注册授权信息';

-- ----------------------------
-- Table structure for s_inst_srvs_conf
-- ----------------------------
DROP TABLE IF EXISTS `s_inst_srvs_conf`;
CREATE TABLE `s_inst_srvs_conf` (
  `srvs_inst_id` varchar(48) NOT NULL COMMENT '服务组件实例标识',
  `srvs_code` varchar(16) DEFAULT NULL COMMENT '服务组件代码',
  `sys_inst_id` varchar(48) DEFAULT NULL COMMENT '系统实例标识',
  `is_allow_service` char(1) DEFAULT NULL COMMENT '是否允许运行',
  `rsv_flag` char(1) DEFAULT NULL COMMENT '保留字段(标志)',
  PRIMARY KEY (`srvs_inst_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='02.服务实例配置信息';

-- ----------------------------
-- Table structure for s_org
-- ----------------------------
DROP TABLE IF EXISTS `s_org`;
CREATE TABLE `s_org` (
  `organno` varchar(20) DEFAULT NULL COMMENT '机构码',
  `suporganno` varchar(20) DEFAULT NULL COMMENT '上级机构码',
  `locate` varchar(100) DEFAULT NULL COMMENT '所属地区',
  `organname` varchar(40) DEFAULT NULL COMMENT '机构名称',
  `organshortform` varchar(40) DEFAULT NULL COMMENT '机构简称',
  `enname` varchar(40) DEFAULT NULL COMMENT '选项值',
  `orderno` decimal(38,0) DEFAULT NULL COMMENT '排序字段',
  `distno` varchar(12) DEFAULT NULL COMMENT '地区编号',
  `launchdate` varchar(10) DEFAULT NULL COMMENT '开办日期',
  `organlevel` decimal(38,0) DEFAULT NULL COMMENT '机构级别',
  `fincode` varchar(21) DEFAULT NULL COMMENT '金融代码',
  `state` decimal(38,0) DEFAULT NULL COMMENT '状态',
  `organchief` varchar(32) DEFAULT NULL COMMENT '机构负责人',
  `telnum` varchar(20) DEFAULT NULL COMMENT '联系电话',
  `address` varchar(200) DEFAULT NULL COMMENT '地址',
  `postcode` varchar(10) DEFAULT NULL COMMENT '邮编',
  `control` varchar(10) DEFAULT NULL COMMENT '控制字',
  `arti_organno` varchar(20) DEFAULT NULL COMMENT '所属法人机构码',
  `distname` varchar(100) DEFAULT NULL COMMENT '地区名称',
  `area_dev_cate_type` varchar(1) DEFAULT NULL COMMENT '地区发展分类',
  `is_marketing` varchar(1) DEFAULT NULL COMMENT '是否营销中心',
  `busiorganno` varchar(20) DEFAULT NULL COMMENT '业务机构编号'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='机构管理';

-- ----------------------------
-- Table structure for s_param_info
-- ----------------------------
DROP TABLE IF EXISTS `s_param_info`;
CREATE TABLE `s_param_info` (
  `param_code` varchar(32) NOT NULL COMMENT '参数代码',
  `param_name` varchar(64) DEFAULT NULL COMMENT '参数名称',
  `param_value` varchar(512) DEFAULT NULL COMMENT '参数值',
  `param_scope` char(1) DEFAULT NULL COMMENT '参数使用范围',
  `subs_code` varchar(16) DEFAULT NULL COMMENT '子系统代码',
  `plugin_id` int(11) DEFAULT NULL COMMENT '应用插件id',
  `remark` varchar(254) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`param_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='01.平台参数信息表';

-- ----------------------------
-- Table structure for s_resource
-- ----------------------------
DROP TABLE IF EXISTS `s_resource`;
CREATE TABLE `s_resource` (
  `resourceid` varchar(32) NOT NULL COMMENT '资源id',
  `cnname` varchar(50) DEFAULT NULL COMMENT '选项名称',
  `systempk` varchar(20) DEFAULT NULL COMMENT '所属子系统',
  `url` varchar(254) DEFAULT NULL COMMENT '资源url',
  `parentid` varchar(50) DEFAULT NULL COMMENT '上级资源id',
  `tablename` varchar(50) DEFAULT NULL COMMENT '表名',
  `procid` varchar(100) DEFAULT NULL COMMENT '流程模板',
  `orderid` varchar(20) DEFAULT NULL COMMENT '序号',
  `memo` varchar(50) DEFAULT NULL COMMENT '备注',
  `icon` varchar(100) DEFAULT NULL COMMENT '资源图标',
  `orgid` varchar(16) DEFAULT NULL COMMENT '组织号',
  PRIMARY KEY (`resourceid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='资源定义';

-- ----------------------------
-- Table structure for s_resourceaction
-- ----------------------------
DROP TABLE IF EXISTS `s_resourceaction`;
CREATE TABLE `s_resourceaction` (
  `resourceid` varchar(32) NOT NULL COMMENT '资源id',
  `actid` varchar(32) NOT NULL COMMENT '操作id',
  `descr` varchar(50) DEFAULT NULL COMMENT '描述',
  `flag` varchar(50) DEFAULT NULL COMMENT '标志',
  `confirmmsg` varchar(100) DEFAULT NULL COMMENT '提示确认信息',
  `orgid` varchar(16) DEFAULT NULL COMMENT '组织号',
  PRIMARY KEY (`resourceid`,`actid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='资源操作';

-- ----------------------------
-- Table structure for s_role
-- ----------------------------
DROP TABLE IF EXISTS `s_role`;
CREATE TABLE `s_role` (
  `roleno` varchar(4) NOT NULL COMMENT '角色码',
  `rolename` varchar(40) DEFAULT NULL COMMENT '角色名称',
  `orderno` decimal(22,0) DEFAULT NULL COMMENT '排序字段',
  `type` decimal(22,0) DEFAULT NULL COMMENT '类型',
  `memo` varchar(60) DEFAULT NULL COMMENT '备注',
  `orgid` varchar(16) DEFAULT NULL COMMENT '组织号',
  PRIMARY KEY (`roleno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色控制配置表';

-- ----------------------------
-- Table structure for s_roleright
-- ----------------------------
DROP TABLE IF EXISTS `s_roleright`;
CREATE TABLE `s_roleright` (
  `roleno` char(4) DEFAULT NULL COMMENT '角色编号',
  `resourceid` varchar(32) DEFAULT NULL COMMENT '资源id',
  `actid` varchar(32) DEFAULT NULL COMMENT '操作id',
  `state` decimal(22,0) DEFAULT NULL COMMENT '状态',
  `orgid` varchar(16) DEFAULT NULL COMMENT '组织号'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='资源权限';

-- ----------------------------
-- Table structure for s_roleuser
-- ----------------------------
DROP TABLE IF EXISTS `s_roleuser`;
CREATE TABLE `s_roleuser` (
  `roleno` char(4) NOT NULL COMMENT '角色码',
  `actorno` varchar(8) NOT NULL COMMENT '用户码',
  `state` decimal(22,0) DEFAULT NULL COMMENT '状态',
  `orgid` varchar(16) DEFAULT NULL COMMENT '组织号',
  PRIMARY KEY (`roleno`,`actorno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户角色关系表';

-- ----------------------------
-- Table structure for s_srvs_cron_conf
-- ----------------------------
DROP TABLE IF EXISTS `s_srvs_cron_conf`;
CREATE TABLE `s_srvs_cron_conf` (
  `job_code` varchar(40) NOT NULL COMMENT '定时作业编号',
  `job_desc` varchar(80) DEFAULT NULL COMMENT '定时作业描述',
  `job_class_type` varchar(16) DEFAULT NULL COMMENT '作业类类型',
  `job_class` varchar(128) DEFAULT NULL COMMENT '作业实现类',
  `job_method` varchar(60) DEFAULT NULL COMMENT '作业类方法',
  `service_version` varchar(8) DEFAULT NULL,
  `service_group` varchar(16) DEFAULT NULL,
  `service_timeout` int(11) DEFAULT NULL,
  `cron_expression` varchar(32) DEFAULT NULL COMMENT 'cron表达式',
  `again_time` int(11) DEFAULT NULL COMMENT '失败重试次数',
  `retry_second` int(11) DEFAULT NULL COMMENT '重试间隔秒数',
  `agent_id` varchar(32) DEFAULT NULL COMMENT '代理节点标识',
  `remark` varchar(254) DEFAULT NULL COMMENT '备注',
  `service_id` varchar(32) DEFAULT NULL,
  `subs_code` varchar(16) DEFAULT NULL COMMENT '所属系统',
  `job_state` varchar(1) DEFAULT NULL COMMENT '作业状态',
  `last_execute_time` varchar(24) DEFAULT NULL COMMENT '最近一次执行',
  `next_execute_time` varchar(24) DEFAULT NULL COMMENT '下次执行时间',
  `cost_time` decimal(10,2) DEFAULT NULL COMMENT '耗时（秒）',
  PRIMARY KEY (`job_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='定时调度配置';

-- ----------------------------
-- Table structure for s_srvs_cron_his
-- ----------------------------
DROP TABLE IF EXISTS `s_srvs_cron_his`;
CREATE TABLE `s_srvs_cron_his` (
  `record_id` char(32) NOT NULL COMMENT '记录id',
  `job_code` varchar(40) DEFAULT NULL COMMENT '定时作业编号',
  `job_desc` varchar(80) DEFAULT NULL COMMENT '定时作业描述',
  `state` int(11) DEFAULT NULL COMMENT '状态',
  `start_time` varchar(24) DEFAULT NULL COMMENT '启动时间',
  `end_time` varchar(24) DEFAULT NULL COMMENT '结束时间',
  `cost_time` decimal(10,2) DEFAULT NULL COMMENT '耗时(秒)',
  `agent_id` varchar(32) DEFAULT NULL COMMENT '代理节点标识',
  `result_desc` varchar(4000) DEFAULT NULL COMMENT '结果描述',
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='定时调度历史';

-- ----------------------------
-- Table structure for s_srvs_cron_inst
-- ----------------------------
DROP TABLE IF EXISTS `s_srvs_cron_inst`;
CREATE TABLE `s_srvs_cron_inst` (
  `job_code` varchar(40) NOT NULL COMMENT '定时作业编号',
  `job_desc` varchar(80) DEFAULT NULL COMMENT '定时作业描述',
  `state` int(11) DEFAULT NULL COMMENT '状态',
  `start_time` varchar(24) DEFAULT NULL COMMENT '启动时间',
  `end_time` varchar(24) DEFAULT NULL COMMENT '结束时间',
  `cost_time` decimal(10,2) DEFAULT NULL COMMENT '耗时(秒)',
  `agent_id` varchar(32) DEFAULT NULL COMMENT '代理节点标识',
  `result_desc` varchar(4000) DEFAULT NULL COMMENT '结果描述',
  PRIMARY KEY (`job_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='定时调度实例';

-- ----------------------------
-- Table structure for s_subs_datasource
-- ----------------------------
DROP TABLE IF EXISTS `s_subs_datasource`;
CREATE TABLE `s_subs_datasource` (
  `subs_code` varchar(16) NOT NULL COMMENT '子系统代码',
  `subs_ds_code` varchar(40) NOT NULL COMMENT '子系统数据源代码',
  `subs_ds_name` varchar(80) DEFAULT NULL COMMENT '子系统数据源名称',
  `ds_conn_str` varchar(128) DEFAULT NULL COMMENT '数据源-连接字符串',
  `ds_user_id` varchar(16) DEFAULT NULL COMMENT '数据源-用户名',
  `ds_user_pwd` varchar(64) DEFAULT NULL COMMENT '数据源-用户密码',
  `ds_schema_name` varchar(16) DEFAULT NULL COMMENT '数据源-模式',
  `ds_db_name` varchar(32) DEFAULT NULL COMMENT '数据源-数据库名',
  `subs_ds_type` char(2) DEFAULT NULL COMMENT '子系统数据源类型',
  PRIMARY KEY (`subs_code`,`subs_ds_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='03.平台子系统数据源信息表';

-- ----------------------------
-- Table structure for s_subs_info
-- ----------------------------
DROP TABLE IF EXISTS `s_subs_info`;
CREATE TABLE `s_subs_info` (
  `subs_code` varchar(16) NOT NULL COMMENT '子系统代码',
  `subs_name` varchar(80) DEFAULT NULL COMMENT '子系统名称',
  `subs_data_date` char(10) DEFAULT NULL COMMENT '子系统数据日期',
  `subs_last_data_date` char(10) DEFAULT NULL COMMENT '子系统上一数据日期',
  `subs_load_date` char(10) DEFAULT NULL COMMENT '子系统装数完成日期',
  `subs_bat_date` char(10) DEFAULT NULL COMMENT '子系统批次完成日期',
  `batch_id_prefix` varchar(16) DEFAULT NULL COMMENT '批次编号前缀',
  `rsv_date` char(10) DEFAULT NULL COMMENT '备用日期',
  `rsv_state` char(2) DEFAULT NULL COMMENT '备用状态',
  `orderid` int(11) DEFAULT NULL,
  PRIMARY KEY (`subs_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='01.平台子系统基础信息表';

-- ----------------------------
-- Table structure for s_user
-- ----------------------------
DROP TABLE IF EXISTS `s_user`;
CREATE TABLE `s_user` (
  `actorno` varchar(8) NOT NULL COMMENT '用户码',
  `enname` varchar(32) DEFAULT NULL COMMENT '选项值',
  `actorname` varchar(20) DEFAULT NULL COMMENT '用户名称',
  `nickname` varchar(40) DEFAULT NULL COMMENT '昵称',
  `state` char(1) DEFAULT NULL COMMENT '状态',
  `password` varchar(32) DEFAULT NULL COMMENT '密码',
  `startdate` char(10) DEFAULT NULL COMMENT '启用日期',
  `passwvalda` char(10) DEFAULT NULL COMMENT '密码',
  `firedate` char(10) DEFAULT NULL COMMENT '解雇日期',
  `birthday` char(10) DEFAULT NULL COMMENT '生日',
  `telnum` varchar(20) DEFAULT NULL COMMENT '联系电话',
  `idcardno` varchar(20) DEFAULT NULL COMMENT '身份证号码',
  `allowopersys` char(20) DEFAULT NULL COMMENT '允许操作的系统',
  `lastlogdat` char(10) DEFAULT NULL COMMENT '最后登陆日期',
  `creater` varchar(40) DEFAULT NULL COMMENT '创建人',
  `creattime` char(20) DEFAULT NULL COMMENT '创建时间',
  `usermail` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `wrongpinnum` decimal(22,0) DEFAULT NULL COMMENT '密码输入错误次数',
  `isadmin` char(1) DEFAULT NULL COMMENT '是否管理员',
  `memo` varchar(200) DEFAULT NULL COMMENT '备注',
  `ipmask` varchar(40) DEFAULT NULL COMMENT '用户ip掩码',
  `orderno` decimal(22,0) DEFAULT NULL COMMENT '排序字段',
  `question` varchar(200) DEFAULT NULL COMMENT '用户防伪问题',
  `answer` varchar(200) DEFAULT NULL COMMENT '用户防伪答案',
  `orgid` varchar(16) DEFAULT NULL COMMENT '组织号',
  `depno` varchar(16) DEFAULT NULL COMMENT '部门编号',
  `session_id` varchar(40) DEFAULT NULL COMMENT '当前登录会话id',
  `rank` char(2) DEFAULT NULL COMMENT '职级',
  `parentactorno` varchar(8) DEFAULT NULL COMMENT '主账号用户码',
  `device_no` varchar(18) DEFAULT NULL COMMENT '未知',
  `menu_config` longblob COMMENT '未知',
  `is_priority_show` char(1) DEFAULT NULL COMMENT '是否优先显示',
  `if_together_wf` char(1) DEFAULT NULL COMMENT '是否合并待办',
  `op_model` char(1) DEFAULT NULL COMMENT '操作模式',
  `mob_num` varchar(20) DEFAULT NULL COMMENT '手机号码',
  PRIMARY KEY (`actorno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户管理';
