DELETE b.*,n.* FROM bpm_base_info b,bpm_node_info n WHERE b.bpm_id = n.bpm_id AND b.subs_code="demo";
DELETE FROM bpa_inst_info WHERE subs_code = "demo";
DELETE FROM sys_apps_info WHERE subs_code = "demo";
DELETE FROM sys_deploy_info WHERE subs_code = "demo";
DELETE FROM sys_ecs_info WHERE subs_code = "demo";
DELETE FROM s_subs_info WHERE subs_code = "demo";
DELETE FROM s_resource WHERE systempk = "demo" OR resourceid="demo-system";