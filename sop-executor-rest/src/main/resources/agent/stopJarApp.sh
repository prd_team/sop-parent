#!/bin/bash
echo 开始停止应用$1
cd /agent
if [ $# -eq 0 ]; then
	echo "Usage: startJarApp.sh 应用名 端口"
	exit;
fi
PID="`pgrep -f java.*$1`"
echo "应用已启动实例 pid $PID"
if [ ! -z "$PID" ];then
	echo "结束进程 $PID"
	kill -9 "$PID"
fi

echo "停止应用成功 pid $PID"
