echo 开始部署应用$1
if [ $# -eq 0 ]; then
	echo "Usage: startWar.sh 应用名 端口"
	exit;
fi

echo --1.如果应用已经启动,则停止应用进程...
if [ "`ps -ef |grep $1|grep -v grep`" ]; then
 pid=`ps -ef |grep $1|grep -v grep |awk '{print $2}'`
fi

echo --2.如果存在应用目录,则移除应用目录到临时目录中
if [ -d $1 ]; then
	rm -rf $1
fi

echo --3.解压生成新的应用目录
tar -zxvf apache-tomcat-8.5.59.tar.gz
mv apache-tomcat-8.5.59 $1

echo --4.替换应用tomcat下的端口
sed -i "s/8080/$2/g" $1/conf/server.xml
port2=$[$2+200]
port3=$[$2+400]
sed -i "s/8009/$port2/g" $1/conf/server.xml
sed -i "s/8005/$port3/g" $1/conf/server.xml
 
echo --5.复制应用到对应的tomcat的目录
cp -R $1.war $1/webapps/ 
$1/bin/startup.sh
if [ $pid ]; then
 kill ${pid}
fi
