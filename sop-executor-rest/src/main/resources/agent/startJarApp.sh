#!/bin/bash
echo 开始部署应用$1
cd /agent
if [ $# -eq 0 ]; then
	echo "Usage: startJarApp.sh 应用名 端口"
	exit;
fi
PID="`pgrep -f java.*$1`"
echo "应用已启动实例 pid $PID"
echo --1.如果应用已经启动,则停止应用进程...
if [ ! -z "$PID" ];then
	echo "结束进程 $PID"
	kill -9 "$PID"
fi

if [ ! -z "$2" ];then
	echo --2.启动jar java -jar -Dfile.encoding=UTF-8 -Dspring.profiles.active=$2 $1.jar 
	nohup java -jar -Dfile.encoding=UTF-8 -Dspring.profiles.active=$2 "$1".jar > /dev/null &
else
	echo --2.启动jar java -jar -Dfile.encoding=UTF-8 $1.jar
	nohup java -jar -Dfile.encoding=UTF-8 "$1".jar > /dev/null &
fi

sleep 2
PID="`pgrep -f java.*$1`"
echo "启动成功 pid $PID"
