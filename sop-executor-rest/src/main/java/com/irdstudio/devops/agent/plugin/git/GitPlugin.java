package com.irdstudio.devops.agent.plugin.git;

import com.irdstudio.bfp.executor.core.plugin.AbstractPlugin;
import com.irdstudio.bfp.executor.core.plugin.common.SParamInfo;
import com.irdstudio.bfp.executor.rest.init.ExecutorResourceLoader;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.beetl.ext.fn.StringUtil;
import org.eclipse.jgit.api.errors.GitAPIException;

import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class GitPlugin extends AbstractPlugin {

    private String pluginName = null;
    private PluginGitConf pluginParam = null;
    private SParamInfo gitLocalPath=null;
    private String gitLocalPathFileName;
    /**
     * 更新到本地路径
     * @param localPath 本地仓库路径
     * @param remotePath 远程仓库地址
     * @param username 用户名
     * @param password 密码
     * @param branchName 分支
     * @return
     */
    public boolean repositoryCreate(String localPath,String remotePath,String username,String password,String branchName){
        JGitUtil jGitUtil = new JGitUtil(localPath,remotePath,username,password);
        try {
            jGitUtil.pull(branchName);
        } catch (Exception e) {
            try {
                jGitUtil.cloneBranch(branchName);
            } catch (Exception e1) {
                jGitUtil.close();
                File localRepoFile = new File(localPath);
                if (localRepoFile.exists()) {
                    FileUtils.deleteQuietly(localRepoFile);
                }
                try {
                    jGitUtil = new JGitUtil(localPath,remotePath,username,password);
                    jGitUtil.cloneBranch(branchName);
                } catch (Exception e2) {
                    logger.error("git 拉取仓库失败" + e2.getMessage(), e2);
                    e2.printStackTrace();
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    protected boolean doReadConfigureFromDB(Connection connection, String s) {
        pluginName = context.getSzPluginName();
        pluginParam = context.getSzPluginParam(PluginGitConf.class);
        //获取文件目录名（如 /xxx)
        int firstIndex = pluginParam.getGitRemotePath().lastIndexOf("/");
        int lastIndex = pluginParam.getGitRemotePath().lastIndexOf(".");
        gitLocalPathFileName  = pluginParam.getGitRemotePath().substring(firstIndex, lastIndex);

        //获取插件全局参数
        List<SParamInfo> paramInfos = new ArrayList();
        paramInfos.addAll(ExecutorResourceLoader.me().getPluginGlobalParam("1071"));
        for (int i = 0; i < paramInfos.size(); i++) {
            if (paramInfos.get(i).getParamCode()!=null && "localPath".equals(paramInfos.get(i).getParamCode()  )){
                gitLocalPath = paramInfos.get(i);
                break;
            }
        }
        if (pluginParam ==null || gitLocalPath ==null)
        {
            context.setSzLastErrorMsg("未读取到配置标识为：" + s + "的数据Git配置!");
            return false;
        }else {

            return true;
        }
    }

    @Override
    public boolean execute() throws Exception {
        this.logger.info("........................................................................");
        boolean flag = true;

        if (gitLocalPath!=null){
            flag = repositoryCreate(gitLocalPath.getParamValue()+gitLocalPathFileName, pluginParam.getGitRemotePath(), pluginParam.getGitUserName(), pluginParam.getGitPassword(), pluginParam.getGitBranchName());
            if (flag){
                logger.info("代码更新成功，本地更新路径:"+ gitLocalPath.getParamValue()+"\n远程路径为："+ pluginParam.getGitRemotePath());
            }
            else {
                logger.info("代码更新失败，本地更新路径:"+ gitLocalPath.getParamValue()+"\n远程路径为："+ pluginParam.getGitRemotePath());
            }
            return flag;
        }else {
            return false;
        }

    }
}
