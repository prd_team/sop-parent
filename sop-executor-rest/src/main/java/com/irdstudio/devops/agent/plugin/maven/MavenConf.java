package com.irdstudio.devops.agent.plugin.maven;

public class MavenConf {

    /**
     * maven参数
     */
    private String mavenParam;
    /**
     * maven参数
     */
    private String mavenSettings;

    public String getMavenSettings() {
        return mavenSettings;
    }

    public void setMavenSettings(String mavenSettings) {
        this.mavenSettings = mavenSettings;
    }

    public String getMavenParam() {
        return mavenParam;
    }

    public void setMavenParam(String mavenParam) {
        this.mavenParam = mavenParam;
    }
}