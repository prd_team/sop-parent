package com.irdstudio.devops.agent.boot.config;

import com.alibaba.druid.filter.Filter;
import com.alibaba.druid.filter.logging.Slf4jLogFilter;
import com.alibaba.druid.pool.DruidDataSource;
import com.irdstudio.bfp.executor.rest.init.ActiveConsoleExecutor;
import com.irdstudio.sdk.beans.core.threadpool.ApplicationThreadPool2;
import com.irdstudio.sdk.beans.core.util.PropertiesUtil;
import com.irdstudio.sdk.beans.core.util.RSAUtility;
import com.irdstudio.sdk.beans.core.util.SpringContextUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@ConditionalOnMissingBean(name="appConfig")
@Configuration("appExecutorConfig")
public class AppConfig {

    @Value("${dataSource.url}")
    private String url;

    @Value("${dataSource.username.rsa.decrypt}")
    private boolean usernameDecrypt;
    @Value("${dataSource.password.rsa.decrypt}")
    private boolean passwordDecrypt;

    @Value("${dataSource.username}")
    private String username;

    @Value("${dataSource.password}")
    private String password;

    @Value("${dataSource.driverClassName}")
    private String driverClassName;

    @Value("${dataSource.maxActive:30}")
    private int maxActive;

    @Value("${dataSource.initialSize:10}")
    private int initialSize;

    @Value("${dataSource.minIdle:10}")
    private int minIdle;

    @Value("${dataSource.validationQuery}")
    private String validationQuery;

    @Value("${dataSource.testWhileIdle}")
    private boolean testWhileIdle;

    @Value("${dataSource.timeBetweenEvictionRunsMillis}")
    private int timeBetweenEvictionRunsMillis;

    @Value("${dataSource.minEvictableIdleTimeMillis}")
    private int minEvictableIdleTimeMillis;

    @Value("${dataSource.testOnBorrow}")
    private boolean testOnBorrow;

    @Value("${dataSource.testOnReturn}")
    private boolean testOnReturn;

    @Value("${dataSource.poolPreparedStatements}")
    private boolean poolPreparedStatements;

    @Value("${dataSource.maxPoolPreparedStatementPerConnectionSize}")
    private int maxPoolPreparedStatementPerConnectionSize;

    @Value("${dataSource.filters}")
    private String filters;

    @Value("${dataSource.connectionProperties}")
    private String connectionProperties;

    @Value("${dataSource.removeAbandoned}")
    private String removeAbandoned;

    @Value("${dataSource.removeAbandonedTimeout}")
    private String removeAbandonedTimeout;

    @Value("${dataSource.logAbandoned}")
    private String logAbandoned;

    @Value("${e4a.sessionTimeOut:3600000}")
    private int sessionTimeOut;

    @Value("${e4a.allowUrl:/**/*}")
    private String allowUrl;

    @Value("${applicationThreadPool.maxTaskNum:300}")
    private int maxTaskNum;


    @Bean
    public DataSource druidDataSource() {
        DruidDataSource dataSource = new DruidDataSource();
        //设置数据库URL
        dataSource.setUrl(url);
        //设置账户密码
        try {
            if (usernameDecrypt) {
                String deUsername = RSAUtility.decryptByBase64(username, PropertiesUtil.getPropertyByKey("key", "rsa.privateKey"));
                dataSource.setUsername(deUsername);
            } else {
                dataSource.setUsername(username);
            }

            if (passwordDecrypt) {
                String dePassword = RSAUtility.decryptByBase64(password, PropertiesUtil.getPropertyByKey("key", "rsa.privateKey"));
                dataSource.setPassword(dePassword);
            } else {
                dataSource.setPassword(password);
            }
        } catch (Exception e) {
            LoggerFactory.getLogger(AppConfig.class).error(e.getMessage(), e);
        }
        //设置驱动
        dataSource.setDriverClassName(driverClassName);
        //初始化大小，最小，最大
        dataSource.setInitialSize(initialSize);
        dataSource.setMinIdle(minIdle);
        dataSource.setMaxActive(maxActive);
        dataSource.setValidationQuery(validationQuery);
        // 申请连接的时候检测，如果空闲时间大于timeBetweenEvictionRunsMillis，执行validationQuery检测连接是否有效
        dataSource.setTestWhileIdle(testWhileIdle);
        // 配置间隔多久才进行一次检测，检测需要关闭的空闲连接，单位是毫秒
        dataSource.setTimeBetweenEvictionRunsMillis(timeBetweenEvictionRunsMillis);
        // 配置一个连接在池中最小生存的时间，单位是毫秒
        dataSource.setMinEvictableIdleTimeMillis(minEvictableIdleTimeMillis);
        // 申请连接时执行validationQuery检测连接是否有效，做了这个配置会降低性能。
        dataSource.setTestOnBorrow(testOnBorrow);
        // 归还连接时执行validationQuery检测连接是否有效，做了这个配置会降低性能
        dataSource.setTestOnReturn(testOnReturn);
        // 是否缓存preparedStatement，也就是PSCache。PSCache对支持游标的数据库性能提升巨大，比如说oracle。在mysql5.5以下的版本中没有PSCache功能，建议关闭掉。5.5及以上版本有PSCache，建议开启。
        dataSource.setPoolPreparedStatements(poolPreparedStatements);
        dataSource.setMaxPoolPreparedStatementPerConnectionSize(maxPoolPreparedStatementPerConnectionSize);
        // 配置监控统计拦截的filters merge:合并为参数化sql wall:防御sql注入
        try {
            dataSource.setFilters(filters);
        } catch (SQLException e) {
            LoggerFactory.getLogger(AppConfig.class).error(e.getMessage(), e);
        }
        Slf4jLogFilter logFilter = new Slf4jLogFilter();
        logFilter.setStatementExecutableSqlLogEnable(false);
        List<Filter> filters = new ArrayList<>(1);
        filters.add(logFilter);
        dataSource.setProxyFilters(filters);
        return dataSource;
    }

    @Autowired
    @Bean
    public SpringContextUtils springContextUtils(ApplicationContext ctx) {
        SpringContextUtils springContextUtils = new SpringContextUtils();
        springContextUtils.setApplicationContext(ctx);
        return springContextUtils;
    }

    @Bean("applicationThreadPool")
    public ApplicationThreadPool2 applicationThreadPool() {
        ApplicationThreadPool2 threadPool = new ApplicationThreadPool2(maxTaskNum);
        return threadPool;
    }
}
