package com.irdstudio.devops.agent.plugin.svn;

import java.util.List;

public class PluginSVNConf {
    /*SVN路径*/
    private String svnPath;
    /*SVN用户名*/
    private String svnUserName;
    /*SVN用户密码*/
    private String svnPassword;
    /*SVN检出到目标路径*/
    private String localSvnPath;

    public String getSvnPath() {
        return svnPath;
    }

    public void setSvnPath(String svnPath) {
        this.svnPath = svnPath;
    }

    public String getSvnUserName() {
        return svnUserName;
    }

    public void setSvnUserName(String svnUserName) {
        this.svnUserName = svnUserName;
    }

    public String getSvnPassword() {
        return svnPassword;
    }

    public void setSvnPassword(String svnPassword) {
        this.svnPassword = svnPassword;
    }

    public String getLocalSvnPath() {
        return localSvnPath;
    }

    public void setLocalSvnPath(String localSvnPath) {
        this.localSvnPath = localSvnPath;
    }

}
