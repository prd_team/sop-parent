package com.irdstudio.devops.agent.dao;

import com.irdstudio.devops.agent.plugin.deploy.PluginSSHConf;
import com.irdstudio.sdk.beans.core.util.KeyUtil;
import com.irdstudio.sdk.beans.core.util.TimeUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;

/**
 * 数据表操作类-[表名: 流程节点信息(bpm_node_info)]
 * 通过JDBC实现基本的数据表操作(CRUD)
 */
public class SysDeployInfoDao {

	private static final Logger logger = LoggerFactory.getLogger(SysDeployInfoDao.class);
	/* 连接对象 */
	Connection conn = null;

	public SysDeployInfoDao(Connection conn){
		this.conn = conn;
	}

	/**
	 * 新增一行
	 * @return int
	 */
	public SysDeployInfo insertSysDeployInfo( PluginSSHConf pluginSSHConf,String subsId) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		EcsBaseInfo ecsBaseInfo = new EcsBaseInfo();
		SysAppsInfo sysAppsInfo = new SysAppsInfo();;
		SysDeployInfo sysDeployInfo = new SysDeployInfo();
		try {
			ps = conn.prepareStatement("select * from ecs_base_info where ecs_ip =? ");
			ps.setString(1,pluginSSHConf.getServerIp());
			rs = ps.executeQuery();
			while (rs.next()){
				ecsBaseInfo.setEcsIp(rs.getString("ecs_ip"));
				ecsBaseInfo.setEcsId(rs.getString("ecs_id"));
				ecsBaseInfo.setEcsName(rs.getString("ecs_name"));
				ecsBaseInfo.setEcsRegion(rs.getString("ecs_region"));
			}

			ps = conn.prepareStatement("select * from sys_apps_info where app_id =? ");
			ps.setString(1,pluginSSHConf.getAppId());
			rs = ps.executeQuery();
			while (rs.next()){
				sysAppsInfo.setAppId(rs.getString("app_id"));
				sysAppsInfo.setAppOrder(Integer.valueOf(rs.getString("app_order")));
				sysAppsInfo.setAppVersion(rs.getString("app_version"));
				sysAppsInfo.setArchType(rs.getString("arch_type"));
				sysAppsInfo.setAppCode(rs.getString("app_code"));
				sysAppsInfo.setAppName(rs.getString("app_name"));
			}

			ps = conn.prepareStatement("insert into sys_deploy_info ( "
					+ "app_deploy_id,app_id,app_name,app_cnname,app_version,app_type,app_order,ecs_id,ecs_ip,subs_id,ecs_name,ecs_region," +
					"app_deploy_path,app_log_path,app_port,app_state,oper_time,oper_userid,ecs_login_user,ecs_login_pwd"
					+ ") values ("+ "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?" +")"
			);
			String appDeployId = KeyUtil.createUUIDKey();
			sysDeployInfo.setAppDeployId(appDeployId);
			ps.setObject(1, appDeployId);
			ps.setObject(2, pluginSSHConf.getAppId());
			ps.setObject(3, sysAppsInfo.getAppCode());
			ps.setObject(4, sysAppsInfo.getAppName());
			ps.setObject(5, sysAppsInfo.getAppVersion());
			ps.setObject(6, sysAppsInfo.getArchType());
			ps.setObject(7, sysAppsInfo.getAppOrder());
			ps.setObject(8, ecsBaseInfo.getEcsId());
			ps.setObject(9, pluginSSHConf.getServerIp());
			ps.setObject(10, subsId);
			ps.setObject(11, ecsBaseInfo.getEcsName());
			ps.setObject(12, ecsBaseInfo.getEcsRegion());
			ps.setObject(13, pluginSSHConf.getAppArtifactLocation());
			ps.setObject(14, "/agent/"+pluginSSHConf.getAppName()+"/logs");
			ps.setObject(15, pluginSSHConf.getTomcatPort());
			ps.setObject(16, "D");
			ps.setObject(17, TimeUtil.getCurrentDateTime());
			ps.setObject(18, "admin");
			ps.setObject(19, pluginSSHConf.getServerUserName());
			ps.setObject(20, pluginSSHConf.getServerPwd());
			ps.executeUpdate();
			return sysDeployInfo;
		} catch (SQLException e) {
			throw new SQLException("insert sysDeployInfo is Wrong!" + e.getMessage());
		} finally {
			close(rs, null, ps);
		}
	}

	/**
	 * 根据主键更新一行记录
	 * @return int
	 */
	public int updateByPk(SysDeployInfo sysDeployInfo) throws SQLException {

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("update sys_deploy_info set "
					+ " app_state = ? "
					+ " where 1=1"
					+ " AND app_deploy_id = ? "
			);
			ps.setObject(1, sysDeployInfo.getAppState());
			ps.setObject(2, sysDeployInfo.getAppDeployId());
			return ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLException("update BpaStageInfo is Wrong!" + e.getMessage());
		} finally {
			close(rs, null, ps);
		}

	}






	/**
	 * 根据主键查询单条记录
	 * @param
	 * @return
	 * @throws SQLException
	 */
	public SysDeployInfo queryByAppIdAndAppPort(String appId,String appPort)
			throws SQLException {
		SysDeployInfo dc = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT * FROM sys_deploy_info WHERE app_id=? and app_port =?");
			ps.setString(1,appId);
			ps.setString(2,appPort);
			rs = ps.executeQuery();
			if (rs.next()) {
				dc = new SysDeployInfo();
				dc.setAppDeployId(rs.getString("app_deploy_id"));
				dc.setAppId(rs.getString("app_id"));
				dc.setAppName(rs.getString("app_name"));
				dc.setAppCnname(rs.getString("app_cnname"));
				dc.setAppVersion(rs.getString("app_version"));
				dc.setAppType(rs.getString("app_type"));
				dc.setAppOrder(Integer.valueOf(rs.getString("app_order")));
				dc.setEcsId(rs.getString("ecs_id"));
				dc.setEcsIp(rs.getString("ecs_ip"));
				dc.setSubsId(rs.getString("subs_id"));
				dc.setEcsName(rs.getString("ecs_name"));
				dc.setEcsRegion(rs.getString("ecs_region"));
				dc.setAppDeployPath(rs.getString("app_deploy_path"));
				dc.setAppLogPath(rs.getString("app_log_path"));
				dc.setAppPort(rs.getString("app_port"));
				dc.setAppState(rs.getString("app_state"));
				dc.setOperTime(rs.getString("oper_time"));
				dc.setOperUserid(rs.getString("oper_userid"));
				dc.setEcsLoginUser(rs.getString("ecs_login_user"));
				dc.setEcsLoginPwd(rs.getString("ecs_login_pwd"));
			}
		} catch (SQLException e) {
			throw new SQLException("queryBpmNodeInfoWithKeys is Wrong!"
					+ e.getMessage());
		} finally {
			close(rs, null, ps);
		}
		return dc;
	}

	


	/**
	 * 关闭资源
	 * @param theRs
	 * @param theStmt
	 * @param thePs
	 */
	protected void close(ResultSet theRs, Statement theStmt,
			PreparedStatement thePs) {
		try {
			if (theRs != null)
				theRs.close();
			if (theStmt != null)
				theStmt.close();
			if (thePs != null)
				thePs.close();
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}

}
