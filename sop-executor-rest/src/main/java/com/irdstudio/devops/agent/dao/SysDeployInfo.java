package com.irdstudio.devops.agent.dao;

import com.irdstudio.sdk.beans.core.vo.BaseInfo;

/**
 * <p>Description: 系统部署信息				<p>
 * @author zjj
 * @date 2020-10-27
 */
public class SysDeployInfo extends BaseInfo {

	private static final long serialVersionUID = 1L;	
	
	/** 应用部署标识 */
	private String appDeployId;
	/** 应用标识 */
	private String appId;
	/** 应用名称 */
	private String appName;
	/** 应用中文名称 */
	private String appCnname;
	/** 应用版本 */
	private String appVersion;
	/** 应用类型 */
	private String appType;
	/** 应用排列顺序 */
	private Integer appOrder;
	/** ecs标识 */
	private String ecsId;
	/** 服务器IP */
	private String ecsIp;
	/** 所属系统 */
	private String subsId;
	/** 服务器名称 */
	private String ecsName;
	/** 所在位置 */
	private String ecsRegion;
	/** 应用部署目录 */
	private String appDeployPath;
	/** 应用日志目录 */
	private String appLogPath;
	/** 应用端口 */
	private String appPort;
	/** 应用状态 */
	private String appState;
	/** 操作时间 */
	private String operTime;
	/** 操作人 */
	private String operUserid;
	/** 服务器登陆用户 */
	private String ecsLoginUser;
	/** 服务器登陆密码 */
	private String ecsLoginPwd;
	

	public void setAppDeployId(String appDeployId){
		this.appDeployId = appDeployId;
	}
	public String getAppDeployId(){
		return this.appDeployId;
	}		
	public void setAppId(String appId){
		this.appId = appId;
	}
	public String getAppId(){
		return this.appId;
	}		
	public void setAppName(String appName){
		this.appName = appName;
	}
	public String getAppName(){
		return this.appName;
	}		
	public void setAppCnname(String appCnname){
		this.appCnname = appCnname;
	}
	public String getAppCnname(){
		return this.appCnname;
	}		
	public void setAppVersion(String appVersion){
		this.appVersion = appVersion;
	}
	public String getAppVersion(){
		return this.appVersion;
	}		
	public void setAppType(String appType){
		this.appType = appType;
	}
	public String getAppType(){
		return this.appType;
	}		
	public void setAppOrder(Integer appOrder){
		this.appOrder = appOrder;
	}
	public Integer getAppOrder(){
		return this.appOrder;
	}		
	public void setEcsId(String ecsId){
		this.ecsId = ecsId;
	}
	public String getEcsId(){
		return this.ecsId;
	}		
	public void setEcsIp(String ecsIp){
		this.ecsIp = ecsIp;
	}
	public String getEcsIp(){
		return this.ecsIp;
	}		
	public void setSubsId(String subsId){
		this.subsId = subsId;
	}
	public String getSubsId(){
		return this.subsId;
	}		
	public void setEcsName(String ecsName){
		this.ecsName = ecsName;
	}
	public String getEcsName(){
		return this.ecsName;
	}		
	public void setEcsRegion(String ecsRegion){
		this.ecsRegion = ecsRegion;
	}
	public String getEcsRegion(){
		return this.ecsRegion;
	}		
	public void setAppDeployPath(String appDeployPath){
		this.appDeployPath = appDeployPath;
	}
	public String getAppDeployPath(){
		return this.appDeployPath;
	}		
	public void setAppLogPath(String appLogPath){
		this.appLogPath = appLogPath;
	}
	public String getAppLogPath(){
		return this.appLogPath;
	}		
	public void setAppPort(String appPort){
		this.appPort = appPort;
	}
	public String getAppPort(){
		return this.appPort;
	}		
	public void setAppState(String appState){
		this.appState = appState;
	}
	public String getAppState(){
		return this.appState;
	}		
	public void setOperTime(String operTime){
		this.operTime = operTime;
	}
	public String getOperTime(){
		return this.operTime;
	}		
	public void setOperUserid(String operUserid){
		this.operUserid = operUserid;
	}
	public String getOperUserid(){
		return this.operUserid;
	}

	public String getEcsLoginUser() {
		return ecsLoginUser;
	}

	public void setEcsLoginUser(String ecsLoginUser) {
		this.ecsLoginUser = ecsLoginUser;
	}

	public String getEcsLoginPwd() {
		return ecsLoginPwd;
	}

	public void setEcsLoginPwd(String ecsLoginPwd) {
		this.ecsLoginPwd = ecsLoginPwd;
	}
}
