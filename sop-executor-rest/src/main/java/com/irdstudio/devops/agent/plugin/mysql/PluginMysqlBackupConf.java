package com.irdstudio.devops.agent.plugin.mysql;
//Java代码实现MySQL数据库导出
public class PluginMysqlBackupConf {
    //MySQL数据库所在服务器地址IP
    private String baseIP;
    //进入数据库所需要的用户名
    private String userName;
    //进入数据库所需要的密码
    private String password;
    //数据库导出文件保存路径
    private String savePath;
    //数据库名称
    private String databaseName;

    public String getBaseIP() {
        return baseIP;
    }

    public void setBaseIP(String baseIP) {
        this.baseIP = baseIP;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSavePath() {
        return savePath;
    }

    public void setSavePath(String savePath) {
        this.savePath = savePath;
    }

    public String getDatabaseName() {
        return databaseName;
    }

    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }
}
