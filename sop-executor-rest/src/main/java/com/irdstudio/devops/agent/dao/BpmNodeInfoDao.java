package com.irdstudio.devops.agent.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;

/**
 * 数据表操作类-[表名: 流程节点信息(bpm_node_info)]
 * 通过JDBC实现基本的数据表操作(CRUD)
 */
public class BpmNodeInfoDao {

	private static final Logger logger = LoggerFactory.getLogger(BpmNodeInfoDao.class);
	/* 连接对象 */
	Connection conn = null;

	public BpmNodeInfoDao(Connection conn){
		this.conn = conn;
	}

	/**
	 * 根据主键查询单条记录
	 * @param
	 * @return
	 * @throws SQLException
	 */
	public BpmNodeInfo queryWithKeys(String bpmNodeId)
			throws SQLException {
		BpmNodeInfo dc = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT * FROM bpm_node_info WHERE bpm_node_id=?");
			ps.setString(1,bpmNodeId);
			rs = ps.executeQuery();
			if (rs.next()) {
				dc = new BpmNodeInfo();
				dc.setBpmNodeId(rs.getString("bpm_node_id"));
				dc.setBpmNodeCode(rs.getString("bpm_node_code"));
				dc.setBpmNodeName(rs.getString("bpm_node_name"));
				dc.setBpmNodeType(rs.getString("bpm_node_type"));
				dc.setBpmNodeParam(rs.getString("bpm_node_param"));
				dc.setBpmId(rs.getString("bpm_id"));
				dc.setX(rs.getInt("x"));
				dc.setY(rs.getInt("y"));
				dc.setH(rs.getInt("h"));
				dc.setW(rs.getInt("w"));
			}
		} catch (SQLException e) {
			throw new SQLException("queryBpmNodeInfoWithKeys is Wrong!"
					+ e.getMessage());
		} finally {
			close(rs, null, ps);
		}
		return dc;
	}

	/**
	 * 根据主键查询单条记录
	 * @param
	 * @return
	 * @throws SQLException
	 */
	public BpmNodeInfo queryByBpmIdAndBpmNodeCode(String bpmId,String BpmNodeCode)
			throws SQLException {
		BpmNodeInfo dc = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("SELECT * FROM bpm_node_info WHERE bpm_id =?  AND bpm_node_code = ?");
			ps.setString(1,bpmId);
			ps.setString(2,BpmNodeCode);
			rs = ps.executeQuery();
			if (rs.next()) {
				dc = new BpmNodeInfo();
				dc.setBpmNodeId(rs.getString("bpm_node_id"));
				dc.setBpmNodeCode(rs.getString("bpm_node_code"));
				dc.setBpmNodeName(rs.getString("bpm_node_name"));
				dc.setBpmNodeType(rs.getString("bpm_node_type"));
				dc.setBpmNodeParam(rs.getString("bpm_node_param"));
				dc.setBpmId(rs.getString("bpm_id"));
				dc.setX(rs.getInt("x"));
				dc.setY(rs.getInt("y"));
				dc.setH(rs.getInt("h"));
				dc.setW(rs.getInt("w"));
			}
		} catch (SQLException e) {
			throw new SQLException("queryBpmNodeInfoWithKeys is Wrong!"
					+ e.getMessage());
		} finally {
			close(rs, null, ps);
		}
		return dc;
	}


	/**
	 * 关闭资源
	 * @param theRs
	 * @param theStmt
	 * @param thePs
	 */
	protected void close(ResultSet theRs, Statement theStmt,
			PreparedStatement thePs) {
		try {
			if (theRs != null)
				theRs.close();
			if (theStmt != null)
				theStmt.close();
			if (thePs != null)
				thePs.close();
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}

}
