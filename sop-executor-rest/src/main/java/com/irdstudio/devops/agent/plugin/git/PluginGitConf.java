package com.irdstudio.devops.agent.plugin.git;

public class PluginGitConf {
    /* 远程git地址*/
    private String  gitRemotePath;
    /* git用户名*/
    private String  gitUserName;
    /* git密码*/
    private String  gitPassword;
    /*分支名称*/
    private String  gitBranchName;


    public String getGitRemotePath() {
        return gitRemotePath;
    }

    public void setGitRemotePath(String gitRemotePath) {
        this.gitRemotePath = gitRemotePath;
    }

    public String getGitUserName() {
        return gitUserName;
    }

    public void setGitUserName(String gitUserName) {
        this.gitUserName = gitUserName;
    }

    public String getGitPassword() {
        return gitPassword;
    }

    public void setGitPassword(String gitPassword) {
        this.gitPassword = gitPassword;
    }

    public String getGitBranchName() {
        return gitBranchName;
    }

    public void setGitBranchName(String gitBranchName) {
        this.gitBranchName = gitBranchName;
    }
}
