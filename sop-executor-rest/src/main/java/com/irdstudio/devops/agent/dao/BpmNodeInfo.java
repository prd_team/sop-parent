package com.irdstudio.devops.agent.dao;

public class BpmNodeInfo {
    private static final long serialVersionUID = 1L;

    /** 流程节点标识 */
    private String bpmNodeId;
    /** 流程节点代码 */
    private String bpmNodeCode;
    /** 流程节点名称 */
    private String bpmNodeName;
    /** 流程节点类型 */
    private String bpmNodeType;
    /** 流程节点参数 */
    private String bpmNodeParam;
    /** 流程标识 */
    private String bpmId;
    /** X坐标 */
    private Integer x;
    /** Y坐标 */
    private Integer y;
    /** H高 */
    private Integer h;
    /** W宽 */
    private Integer w;


    public void setBpmNodeId(String bpmNodeId){
        this.bpmNodeId = bpmNodeId;
    }
    public String getBpmNodeId(){
        return this.bpmNodeId;
    }
    public void setBpmNodeCode(String bpmNodeCode){
        this.bpmNodeCode = bpmNodeCode;
    }
    public String getBpmNodeCode(){
        return this.bpmNodeCode;
    }
    public void setBpmNodeName(String bpmNodeName){
        this.bpmNodeName = bpmNodeName;
    }
    public String getBpmNodeName(){
        return this.bpmNodeName;
    }
    public void setBpmNodeType(String bpmNodeType){
        this.bpmNodeType = bpmNodeType;
    }
    public String getBpmNodeType(){
        return this.bpmNodeType;
    }
    public String getBpmNodeParam() {
        return bpmNodeParam;
    }
    public void setBpmNodeParam(String bpmNodeParam) {
        this.bpmNodeParam = bpmNodeParam;
    }
    public void setBpmId(String bpmId){
        this.bpmId = bpmId;
    }
    public String getBpmId(){
        return this.bpmId;
    }
    public void setX(Integer x){
        this.x = x;
    }
    public Integer getX(){
        return this.x;
    }
    public void setY(Integer y){
        this.y = y;
    }
    public Integer getY(){
        return this.y;
    }
    public void setH(Integer h){
        this.h = h;
    }
    public Integer getH(){
        return this.h;
    }
    public void setW(Integer w){
        this.w = w;
    }
    public Integer getW(){
        return this.w;
    }
}
