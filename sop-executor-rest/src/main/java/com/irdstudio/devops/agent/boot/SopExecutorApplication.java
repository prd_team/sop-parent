package com.irdstudio.devops.agent.boot;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.irdstudio", "com.irdstudio.bfp.executor", "com.irdstudio.devops.agent"})
public class SopExecutorApplication {

    public static void main(String[] args) {
        SpringApplication.run(SopExecutorApplication.class, args);
    }
}


