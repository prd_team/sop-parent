package com.irdstudio.devops.agent.plugin.deploy;

public class PluginSSHConf {

    /**
     * Tomcat版本
     */
    private String tomcatVersion;
    /**
     * Tomcat端口
     */
    private String tomcatPort;
    /**
     * 服务器IP
     */
    private String serverIp;
    /**
     * 服务器用户名
     */
    private String serverUserName;
    /**
     * 服务器用户密码
     */
    private String serverPwd;

    /**
     * 应用编译结果目录
     */
    private String appArtifactLocation;
    /**
     * 环境标识 指明使用哪一个配置文件
     */
    private String envId;
    /**
     * spring boot 所在模块
     */
    private String module;

    private String appName;

    private String appId;

    public String getTomcatVersion() {
        return tomcatVersion;
    }

    public void setTomcatVersion(String tomcatVersion) {
        this.tomcatVersion = tomcatVersion;
    }

    public String getTomcatPort() {
        return tomcatPort;
    }

    public void setTomcatPort(String tomcatPort) {
        this.tomcatPort = tomcatPort;
    }

    public String getServerIp() {
        return serverIp;
    }

    public void setServerIp(String serverIp) {
        this.serverIp = serverIp;
    }

    public String getServerUserName() {
        return serverUserName;
    }

    public void setServerUserName(String serverUserName) {
        this.serverUserName = serverUserName;
    }

    public String getServerPwd() {
        return serverPwd;
    }

    public void setServerPwd(String serverPwd) {
        this.serverPwd = serverPwd;
    }

    public String getAppArtifactLocation() {
        return appArtifactLocation;
    }

    public void setAppArtifactLocation(String appArtifactLocation) {
        this.appArtifactLocation = appArtifactLocation;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getEnvId() {
        return envId;
    }

    public void setEnvId(String envId) {
        this.envId = envId;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }
}
