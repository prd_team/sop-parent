package com.irdstudio.devops.agent.plugin.svn;

import org.tmatesoft.svn.core.SVNDepth;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.internal.io.dav.DAVRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.fs.FSRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.svn.SVNRepositoryFactoryImpl;
import org.tmatesoft.svn.core.internal.wc.DefaultSVNOptions;
import org.tmatesoft.svn.core.wc.*;

import java.io.File;
import java.util.List;

public class SVNUtil {
    // 更新状态 true:没有程序在执行更新，反之则反
    public static Boolean DoUpdateStatus = true;

    // 声明SVN客户端管理类
    private static SVNClientManager ourClientManager;
    /**
     * SVN检出
     * @param svnPath SVN路径
     * @param svnUserName SVN用户名
     * @param svnPassword SVN用户密码
     * @param localSvnPath SVN检出到目标路径
     * @return
     */
    public static Boolean checkOut(String svnPath,String svnUserName,String svnPassword,String localSvnPath) {
        // 初始化库。 必须先执行此操作。具体操作封装在setupLibrary方法中。
        /*
         * For using over http:// and https://
         */
        DAVRepositoryFactory.setup();
        /*
         * For using over svn:// and svn+xxx://
         */
        SVNRepositoryFactoryImpl.setup();

        /*
         * For using over file:///
         */
        FSRepositoryFactory.setup();

        // 相关变量赋值
        SVNURL repositoryURL = null;
        try {
            repositoryURL = SVNURL.parseURIEncoded(svnPath);
        } catch (SVNException e) {
            e.printStackTrace();
            return false;
        }

        ISVNOptions options = SVNWCUtil.createDefaultOptions(true);

        // 实例化客户端管理类
        ourClientManager = SVNClientManager.newInstance((DefaultSVNOptions) options,svnUserName,svnPassword);

        // 要把版本库的内容check out到的目录
        File wcDir = new File(localSvnPath);

        // 通过客户端管理类获得updateClient类的实例。
        SVNUpdateClient updateClient = ourClientManager.getUpdateClient();

        updateClient.setIgnoreExternals(false);

        // 执行check out 操作，返回工作副本的版本号。
        long workingVersion = -1;
        try {
            if (!wcDir.exists()) {
                workingVersion = updateClient.doCheckout(repositoryURL, wcDir, SVNRevision.HEAD, SVNRevision.HEAD, SVNDepth.INFINITY, false);
            } else {
                ourClientManager.getWCClient().doCleanup(wcDir);
                workingVersion = updateClient.doCheckout(repositoryURL, wcDir, SVNRevision.HEAD, SVNRevision.HEAD, SVNDepth.INFINITY, false);
            }
        } catch (SVNException e) {
            e.printStackTrace();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        System.out.println("把版本：" + workingVersion + " check out 到目录：" + wcDir + "中。");
        return true;

    }

    /**
     * 解除svn Lock
     * @param svnUserName SVN用户名
     * @param svnPassword SVN用户密码
     * @param localSvnPath SVN检出到目标路径
     * @return
     */
    public static Boolean doCleanup(String svnUserName,String svnPassword,String localSvnPath) {
        ISVNOptions options = SVNWCUtil.createDefaultOptions(true);
        // 实例化客户端管理类
        ourClientManager = SVNClientManager.newInstance((DefaultSVNOptions) options,svnUserName,svnPassword);

        // 要把版本库的内容check out到的目录
        File wcDir = new File(localSvnPath);
        if (wcDir.exists()) {
            try {
                ourClientManager.getWCClient().doCleanup(wcDir);
            } catch (SVNException e) {
                e.printStackTrace();
                return false;
            }
        } else {
            return false;
        }
        return true;
    }

    /**
     * 更新svn
     * @param svnUserName SVN用户名
     * @param svnPassword SVN用户密码
     * @param localSvnPath SVN检出到目标路径
     * @return int(-1更新失败，1成功，0有程序在占用更新)
     */
    public static int doUpdate(String svnUserName,String svnPassword,String localSvnPath) {
        if (!SVNUtil.DoUpdateStatus) {
            System.out.println("更新程序已经在运行中，不能重复请求！");
            return 0;
        }
        SVNUtil.DoUpdateStatus = false;
        /*
         * For using over http:// and https://
         */
        try {
            DAVRepositoryFactory.setup();

            ISVNOptions options = SVNWCUtil.createDefaultOptions(true);
            // 实例化客户端管理类
            ourClientManager = SVNClientManager.newInstance((DefaultSVNOptions) options,svnUserName,svnPassword);
            // 要更新的文件
            File updateFile = new File(localSvnPath);
            // 获得updateClient的实例
            SVNUpdateClient updateClient = ourClientManager.getUpdateClient();
            updateClient.setIgnoreExternals(false);
            // 执行更新操作
            long versionNum = updateClient.doUpdate(updateFile, SVNRevision.HEAD, SVNDepth.INFINITY, false, false);
            System.out.println("工作副本更新后的版本：" + versionNum);
            DoUpdateStatus = true;
            return 1;
        } catch (SVNException e) {
            DoUpdateStatus = true;
            e.printStackTrace();
            return -1;
        }
    }

    /**
     * Svn提交 list.add("a.txt")也可直接添加单个文件; list.add("aaa")添加文件夹将添加夹子内所有的文件到svn,预添加文件必须先添加其所在的文件夹;
     * @param fileRelativePathList 文件相对路径
     * @param svnUserName SVN用户名
     * @param svnPassword SVN用户密码
     * @param localSvnPath SVN检出到目标路径
     * @param commitMessage 提交消息
     * @return
     */
    public static Boolean doCommit(List<String> fileRelativePathList,String svnUserName,String svnPassword,String localSvnPath,String commitMessage) {
        // 注意：执行此操作要先执行checkout操作。因为本地需要有工作副本此范例才能运行。
        // 初始化支持svn://协议的库
        SVNRepositoryFactoryImpl.setup();

        ISVNOptions options = SVNWCUtil.createDefaultOptions(true);
        // 实例化客户端管理类
        ourClientManager = SVNClientManager.newInstance((DefaultSVNOptions) options,svnUserName, svnPassword);
        // 要提交的文件夹子
        File commitFile = new File(localSvnPath);
        // 获取此文件的状态（是文件做了修改还是新添加的文件？）
        SVNStatus status = null;
        File addFile = null;
        String strPath = null;
        try {
            if (fileRelativePathList != null && fileRelativePathList.size() > 0) {
                for (int i = 0; i < fileRelativePathList.size(); i++) {
                    strPath = fileRelativePathList.get(i);
                    addFile = new File(localSvnPath + "/" + strPath);
                    status = ourClientManager.getStatusClient().doStatus(addFile, true);
                    // 如果此文件是新增加的则先把此文件添加到版本库，然后提交。
                    if (null == status || status.getContentsStatus() == SVNStatusType.STATUS_UNVERSIONED) {
                        // 把此文件增加到版本库中
                        ourClientManager.getWCClient().doAdd(addFile, false, false, false, SVNDepth.INFINITY, false, false);
                        System.out.println("add");
                    }
                }
                // 提交此文件
                ourClientManager.getCommitClient().doCommit(new File[] { commitFile }, true, commitMessage, null, null, true, false, SVNDepth.INFINITY);
                System.out.println("commit");
            }
            // 如果此文件不是新增加的，直接提交。
            else {
                ourClientManager.getCommitClient().doCommit(new File[] { commitFile }, true, commitMessage, null, null, true, false, SVNDepth.INFINITY);
                System.out.println("commit");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        // System.out.println(status.getContentsStatus());
        return true;
    }

    /**
     * Svn提交
     * @param fileRelativePath 文件相对路径
     * @param svnUserName SVN用户名
     * @param svnPassword SVN用户密码
     * @param localSvnPath SVN检出到目标路径
     * @param commitMessage 提交消息
     * @return
     */
    public static Boolean doCommit(String fileRelativePath,String svnUserName,String svnPassword,String localSvnPath,String commitMessage) {
        // 注意：执行此操作要先执行checkout操作。因为本地需要有工作副本此范例才能运行。
        // 初始化支持svn://协议的库
        SVNRepositoryFactoryImpl.setup();

        ISVNOptions options = SVNWCUtil.createDefaultOptions(true);
        // 实例化客户端管理类
        ourClientManager = SVNClientManager.newInstance((DefaultSVNOptions) options, svnUserName, svnPassword);
        // 要提交的文件夹子
        File commitFile = new File(localSvnPath);
        // 获取此文件的状态（是文件做了修改还是新添加的文件？）
        SVNStatus status = null;
        File addFile = null;
        try {
            if (fileRelativePath != null && fileRelativePath.trim().length() > 0) {
                addFile = new File(localSvnPath + "/" + fileRelativePath);
                status = ourClientManager.getStatusClient().doStatus(addFile, true);
                // 如果此文件是新增加的则先把此文件添加到版本库，然后提交。
                if (null == status || status.getContentsStatus() == SVNStatusType.STATUS_UNVERSIONED) {
                    // 把此文件增加到版本库中
                    ourClientManager.getWCClient().doAdd(addFile, false, false, false, SVNDepth.INFINITY, false, false);
                    System.out.println("add");
                }
                // 提交此文件
                ourClientManager.getCommitClient().doCommit(new File[] { commitFile }, true, commitMessage, null, null, true, false, SVNDepth.INFINITY);
                System.out.println("commit");
            }
            // 如果此文件不是新增加的，直接提交。
            else {
                ourClientManager.getCommitClient().doCommit(new File[] { commitFile }, true, commitMessage, null, null, true, false, SVNDepth.INFINITY);
                System.out.println("commit");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        // System.out.println(status.getContentsStatus());
        return true;
    }

    //检出（更新）与提交
    public static Boolean doCheckOutAndCommit(String fileRelativePath,String svnPath,String svnUserName,String svnPassword,String localSvnPath,String commitMessage){

        Boolean  flag = checkOut(svnPath,svnUserName,svnPassword,localSvnPath);
        if (flag){
            flag = doCommit(fileRelativePath,svnUserName,svnPassword,localSvnPath,commitMessage);
        }
        return flag;
    }
//    public static void main(String[] args) {
//        String svnPath; //SVN路径
//        String svnUserName; //SVN用户名
//        String svnPassword; //SVN用户密码
//        String localSvnPath; // SVN检出到目标路径
//        String dirPath;    // 文件所在文件夹的路径
//        String fileRelativePath;  //文件相对路径
//        List<String> fileRelativePathList;  //文件相对路径
//
//        svnPath =  "svn://47.115.62.59/maven/SQL";
//        svnUserName =  "lrt";
//        svnPassword =  "lrt123456";
//        localSvnPath =  "D:\\Tool\\SVN\\SVNTest";
//        fileRelativePath =  "1";
//        /**
//         * 检出
//         * 1.检出路径需为同一个URL的 working copy 文件夹
//         * 或 检出路径不存在时，会自动创建检出路径的 working copy 文件夹
//         * 2.检出路径不能为非 working copy 文件夹
//         */
//        System.out.println(checkOut(svnPath,svnUserName,svnPassword,localSvnPath));
//
//        /**
//         * 更新
//         * 1.更新路径需为 working copy 文件夹
//         */
////        System.out.println(doUpdate(svnUserName,svnPassword,localSvnPath));
//
//        /**
//         * 提交：
//         * 1.新增文件需传入新增文件的相对路径
//         * 2.新增文件夹及新增文件夹下新增文件，只需传入新增文件夹相对路径即可
//         * 3.修改已提交过的文件，可传入修改文件的最上层路径
//         */
//        String commitMessage ="提交测试";
////        System.out.println(doCommit(fileRelativePath,svnUserName,svnPassword,localSvnPath,commitMessage));
//    }
}
