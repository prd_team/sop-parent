package com.irdstudio.devops.agent.plugin.git;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.internal.storage.file.FileRepository;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;

import java.io.File;
import java.io.IOException;

public class JGitUtil {
    private String  localPath, localGitPath, remotePath;
    private Repository localRepository;
    private String username;
    private String password;
    private Git git;

    public JGitUtil(String localPath, String remotePath, String username, String password) {
        this.localPath = localPath;
        this.remotePath = remotePath;
        this.username = username;
        this.password = password;
        this.localGitPath = this.localPath+"/.git";
        try {
            localRepository = new FileRepository(localGitPath);
        }catch (IOException e){
            e.printStackTrace();
        }
        git = new Git(localRepository);
    }

    /**
     * 创建本地仓库
     *
     * @throws IOException
     */
    public void create() throws IOException {
        Repository repository = new FileRepository(localGitPath);
        repository.create();
        System.out.println("create success");
    }

    /**
     * clone克隆远程分支到本地
     *
     * @param branchName
     * @throws GitAPIException
     */
    public void cloneBranch(String branchName) throws GitAPIException {
        Git.cloneRepository()
                .setURI(remotePath)
                .setBranch(branchName)
                .setDirectory(new File(localPath))
                .setCredentialsProvider(new UsernamePasswordCredentialsProvider(username,password))
                .call();
        System.out.println("clone success");

    }

    /**
     * pull远程代码
     *
     * @param branchName 远程分支名称
     * @throws Exception
     */
    public void pull(String branchName) throws Exception {
        git.pull()
                .setRemoteBranchName(branchName)
                .setCredentialsProvider(new UsernamePasswordCredentialsProvider(username,password))
                .call();
        System.out.println("pull success");
    }

    /**
     * 将单个文件加入Git
     *
     * @param fileName 添加文件名
     * @throws Exception
     */
    public void add(String fileName) throws Exception {
        File myFile = new File(localPath + fileName);
        myFile.createNewFile();
        git.add().addFilepattern(fileName).call();
        System.out.println("add success");
    }

    /**
     * 将增加的所有文件加入Git
     *
     * @throws Exception
     */
    public void addAll() throws Exception {
        git.add().addFilepattern(".").call();
        System.out.println("add success");
    }


    /**
     * 提交文件
     *
     * @param message 提交备注
     * @throws Exception
     */
    public void commit(String message) throws Exception {
        git.commit().setMessage(message).call();
        System.out.println("commit success");

    }

    /**
     * 同步远程仓库
     *
     * @throws Exception
     */
    public void push() throws Exception {
        git.push().setCredentialsProvider(new UsernamePasswordCredentialsProvider(this.username, this.password)).call();
        System.out.println("push success");
    }

    public void close() {
        if (git != null) {
            git.close();
        }
    }
}
