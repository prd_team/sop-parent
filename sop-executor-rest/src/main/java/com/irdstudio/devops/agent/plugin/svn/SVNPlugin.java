package com.irdstudio.devops.agent.plugin.svn;

import com.irdstudio.bfp.executor.core.plugin.AbstractPlugin;
import java.sql.Connection;
import java.sql.SQLException;

import static com.irdstudio.devops.agent.plugin.svn.SVNUtil.checkOut;

public class SVNPlugin extends AbstractPlugin {

    private String pluginName = null;
    private PluginSVNConf pluginParam=null;

    @Override
    protected boolean doReadConfigureFromDB(Connection connection, String s) throws SQLException {
        pluginName = context.getSzPluginName();
        pluginParam = context.getSzPluginParam(PluginSVNConf.class);
        if (pluginParam ==null)
        {
            context.setSzLastErrorMsg("未读取到配置标识为：" + s + "的数据SVN配置!");
            return false;
        }else {
            return true;
        }
    }

    @Override
    public boolean execute() throws Exception {
        if (checkOut(pluginParam.getSvnPath(), pluginParam.getSvnUserName(), pluginParam.getSvnPassword(), pluginParam.getLocalSvnPath())){
            logger.info("检出更新成功,本地路径为："+ pluginParam.getLocalSvnPath()+"\n远程路径为："+ pluginParam.getSvnPath());
            return  true;
        }else {
            logger.info("检出更新失败，本地路径为："+ pluginParam.getLocalSvnPath()+"\n远程路径为："+ pluginParam.getSvnPath());
            return  false;
        }
    }

}
