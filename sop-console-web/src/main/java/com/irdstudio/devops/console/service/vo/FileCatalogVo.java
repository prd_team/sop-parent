package com.irdstudio.devops.console.service.vo;

import com.irdstudio.sdk.beans.core.vo.BaseInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lwc
 * @Description TODO
 * @createTime 2020年12月15日
 */
public class FileCatalogVo extends BaseInfo {
    private static final long serialVersionUID = 1L;
    private ArrayList<String> files;

    public ArrayList<String> getFiles() {
        return files;
    }
    public void setFiles(ArrayList<String> files) {
        this.files = files;
    }
}
