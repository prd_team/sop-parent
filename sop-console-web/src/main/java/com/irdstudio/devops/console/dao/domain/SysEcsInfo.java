package com.irdstudio.devops.console.dao.domain;

import com.irdstudio.sdk.beans.core.vo.BaseInfo;
/**
 * Description: 系统服务器信息			
 * @author zjj
 * @date 2020-10-27
 */
public class SysEcsInfo extends BaseInfo{

	private static final long serialVersionUID = 1L;	
	
	/** ecs标识 */
	private String ecsId;
	/** 所属系统 */
	private String subsId;


	private String ecsIp;

	private String ecsName;
	

	public void setEcsId(String ecsId){
		this.ecsId = ecsId;
	}
	public String getEcsId(){
		return this.ecsId;
	}		
	public void setSubsId(String subsId){
		this.subsId = subsId;
	}
	public String getSubsId(){
		return this.subsId;
	}

	public String getEcsIp() {
		return ecsIp;
	}

	public void setEcsIp(String ecsIp) {
		this.ecsIp = ecsIp;
	}

	public String getEcsName() {
		return ecsName;
	}

	public void setEcsName(String ecsName) {
		this.ecsName = ecsName;
	}
}
