package com.irdstudio.devops.console.service.facade;

import java.util.List;

import com.irdstudio.bfp.console.dao.domain.SubsCodeBase;
import com.irdstudio.devops.console.dao.domain.SysDeployInfo;
import com.irdstudio.devops.console.service.vo.SysDeployInfoVO;

/**
 * <p>Description:系统部署信息				<p>
 * @author zjj
 * @date 2020-10-27
 */
public interface SysDeployInfoService {
	
	public List<SysDeployInfoVO> queryAllOwner(SysDeployInfoVO sysDeployInfoVo);
	
	public List<SysDeployInfoVO> queryAllCurrOrg(SysDeployInfoVO sysDeployInfoVo);
	
	public List<SysDeployInfoVO> queryAllCurrDownOrg(SysDeployInfoVO sysDeployInfoVo);
	
	public int insertSysDeployInfo(SysDeployInfoVO inSysDeployInfoVo);
	public void deploymentApplication(SysDeployInfoVO inSysDeployInfoVo);

	public int deleteByPk(SysDeployInfoVO sysDeployInfoVo);
	public String readLogFile(String bpaSerialNo);
	
	public int updateByPk(SysDeployInfoVO sysDeployInfoVo);

	public int updateBySubsCode(SubsCodeBase subsCodeBase);

	public int updateAppName(SysDeployInfoVO sysDeployInfoVo);

	public SysDeployInfoVO queryByPk(SysDeployInfoVO sysDeployInfoVo);

}
