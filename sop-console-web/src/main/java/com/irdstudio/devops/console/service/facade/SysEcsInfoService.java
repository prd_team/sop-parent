package com.irdstudio.devops.console.service.facade;

import java.util.List;

import com.irdstudio.bfp.console.dao.domain.SubsCodeBase;
import com.irdstudio.devops.console.dao.domain.EcsBaseInfo;
import com.irdstudio.devops.console.service.vo.EcsBaseInfoVO;
import com.irdstudio.devops.console.service.vo.SysEcsInfoVO;

/**
 * <p>Description:系统服务器信息				<p>
 * @author zjj
 * @date 2020-10-27
 */
public interface SysEcsInfoService {
	
	public List<EcsBaseInfoVO> queryAllOwner(SysEcsInfoVO sysEcsInfoVo);
	
	public List<SysEcsInfoVO> queryAllCurrOrg(SysEcsInfoVO sysEcsInfoVo);
	
	public List<SysEcsInfoVO> queryAllCurrDownOrg(SysEcsInfoVO sysEcsInfoVo);
	
	public int insertSysEcsInfo(SysEcsInfoVO inSysEcsInfoVo);
	
	public int deleteByPk(SysEcsInfoVO sysEcsInfoVo);
	
	public int updateByPk(SysEcsInfoVO sysEcsInfoVo);

	public int updateBySubsCode(SubsCodeBase subsCodeBase);

	public SysEcsInfoVO queryByPk(SysEcsInfoVO sysEcsInfoVo);

}
