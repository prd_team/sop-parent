package com.irdstudio.devops.console.api.rest;

import java.util.List;

import com.irdstudio.bfp.console.dao.domain.SubsCodeBase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.irdstudio.devops.console.service.facade.SysAppsInfoService;
import com.irdstudio.devops.console.service.vo.SysAppsInfoVO;
import com.irdstudio.sdk.beans.core.vo.ResponseData;
import com.irdstudio.sdk.beans.web.controller.AbstractController;

@RestController
@RequestMapping("/api")
public class SysAppsInfoController extends AbstractController  {
	
	@Autowired
	@Qualifier("sysAppsInfoServiceImpl")
	private SysAppsInfoService sysAppsInfoService;

	
	/**
	 * 列表数据查询
	 * @param page
	 * @param size
	 * @return
	 */
	@RequestMapping(value="/sys/apps/infos", method=RequestMethod.POST)
	public @ResponseBody ResponseData<List<SysAppsInfoVO>> querySysAppsInfoAll(
			SysAppsInfoVO vo) {		
		List<SysAppsInfoVO> outputVo = sysAppsInfoService.queryAllOwner(vo);
		return getResponseData(outputVo);
		
	}
	
	/**
	 * 根据主键查询详情
	 * @return
	 */
	@RequestMapping(value="/sys/apps/info/{appId}", method=RequestMethod.GET)
	public @ResponseBody ResponseData<SysAppsInfoVO> queryByPk(@PathVariable("appId") String appId) {		
		SysAppsInfoVO inVo = new SysAppsInfoVO();
				inVo.setAppId(appId);
		SysAppsInfoVO outputVo = sysAppsInfoService.queryByPk(inVo);
		return getResponseData(outputVo);
		
	}
	
	/**
	 * 根据主键删除信息
	 * @param sysAppsInfo
	 * @return
	 */
	@RequestMapping(value="/sys/apps/info", method=RequestMethod.DELETE)
	public @ResponseBody ResponseData<Integer> deleteByPk(@RequestBody SysAppsInfoVO inSysAppsInfoVo) {		
		int outputVo = sysAppsInfoService.deleteByPk(inSysAppsInfoVo);
		return getResponseData(outputVo);
		
	}

	/**
	 * 删除应用及相关表数据
	 * @param
	 * @return
	 */
	@RequestMapping(value="/sys/apps/info/delete", method=RequestMethod.DELETE)
	public @ResponseBody ResponseData<Integer> deleteAppByPKAndAppCode(@RequestBody SysAppsInfoVO inSysAppsInfoVo) {
		int outputVo = sysAppsInfoService.deleteAppByPKAndAppCode(inSysAppsInfoVo);
		return getResponseData(outputVo);

	}

	/**
	 * 根据主键更新信息
	 * @param inSysAppsInfoVo
	 * @return
	 */
	@RequestMapping(value="/sys/apps/info", method=RequestMethod.PUT)
	public @ResponseBody ResponseData<Integer> updateByPk(@RequestBody SysAppsInfoVO inSysAppsInfoVo) {		
		int outputVo = sysAppsInfoService.updateByPk(inSysAppsInfoVo);
		return getResponseData(outputVo);
		
	}
	/**
	 * 更新subsCode
	 * @return
	 */
	@RequestMapping(value="/sys/apps/info/change/subs/code", method=RequestMethod.PUT)
	public @ResponseBody ResponseData<Integer> updateBySubsCode(@RequestBody SubsCodeBase subsCodeBase) {
		int outputVo = sysAppsInfoService.updateBySubsCode(subsCodeBase);
		return getResponseData(outputVo);
	}
	
	/**
	 * 新增数据
	 * @param inSysAppsInfoVo
	 * @return
	 */
	@RequestMapping(value="/sys/apps/info", method=RequestMethod.POST)
	public @ResponseBody ResponseData<Integer> insertSysAppsInfo(@RequestBody SysAppsInfoVO inSysAppsInfoVo) {
		int outputVo = sysAppsInfoService.insertSysAppsInfo(inSysAppsInfoVo);
		return getResponseData(outputVo);
		
	}
}
