package com.irdstudio.devops.console.service.facade;

import java.util.List;

import com.irdstudio.devops.console.service.vo.EcsBaseInfoVO;

/**
 * <p>Description:服务器信息				<p>
 * @author ligm
 * @date 2019-03-21
 */
public interface EcsBaseInfoService {
	
	public List<EcsBaseInfoVO> queryAllOwner(EcsBaseInfoVO ecsBaseInfoVo);
	
	public List<EcsBaseInfoVO> queryAllCurrOrg(EcsBaseInfoVO ecsBaseInfoVo);
	
	public List<EcsBaseInfoVO> queryAllCurrDownOrg(EcsBaseInfoVO ecsBaseInfoVo);
	
	public int insertEcsBaseInfo(EcsBaseInfoVO inEcsBaseInfoVo);
	
	public int deleteByPk(EcsBaseInfoVO ecsBaseInfoVo);
	
	public int updateByPk(EcsBaseInfoVO ecsBaseInfoVo);
	
	public EcsBaseInfoVO queryByPk(EcsBaseInfoVO ecsBaseInfoVo);

}
