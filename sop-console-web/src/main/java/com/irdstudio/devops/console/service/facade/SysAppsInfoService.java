package com.irdstudio.devops.console.service.facade;

import java.util.List;

import com.irdstudio.bfp.console.dao.domain.SubsCodeBase;
import com.irdstudio.devops.console.service.vo.SysAppsInfoVO;
import org.apache.ibatis.annotations.Param;

/**
 * <p>Description:系统应用信息				<p>
 * @author zjj
 * @date 2020-10-27
 */
public interface SysAppsInfoService {
	
	public List<SysAppsInfoVO> queryAllOwner(SysAppsInfoVO sysAppsInfoVo);
	
	public List<SysAppsInfoVO> queryAllCurrOrg(SysAppsInfoVO sysAppsInfoVo);
	
	public List<SysAppsInfoVO> queryAllCurrDownOrg(SysAppsInfoVO sysAppsInfoVo);
	
	public int insertSysAppsInfo(SysAppsInfoVO inSysAppsInfoVo);
	
	public int deleteByPk(SysAppsInfoVO sysAppsInfoVo);
	
	public int updateByPk(SysAppsInfoVO sysAppsInfoVo);

	public int updateBySubsCode(SubsCodeBase subsCodeBase);

	public SysAppsInfoVO queryByPk(SysAppsInfoVO sysAppsInfoVo);

	int deleteAppByPKAndAppCode(SysAppsInfoVO inSysAppsInfoVo);
}
