package com.irdstudio.devops.console.dao.domain;

import com.irdstudio.sdk.beans.core.vo.BaseInfo;
/**
 * Description: 服务器操作日志			
 * @author ligm
 * @date 2018-12-31
 */
public class EcsAuditLog extends BaseInfo{

	private static final long serialVersionUID = 1L;	
	
	/** 记录唯一编号 */
	private String recordKeyid;
	/** 操作时间 */
	private String operTime;
	/** ecs标识 */
	private String ecsId;
	/** 服务器操作 */
	private String ecsOperAction;
	/** 服务器响应信息 */
	private String ecsMsg;
	/** 操作人 */
	private String operUserid;
	

	public void setRecordKeyid(String recordKeyid){
		this.recordKeyid = recordKeyid;
	}
	public String getRecordKeyid(){
		return this.recordKeyid;
	}		
	public void setOperTime(String operTime){
		this.operTime = operTime;
	}
	public String getOperTime(){
		return this.operTime;
	}		
	public void setEcsId(String ecsId){
		this.ecsId = ecsId;
	}
	public String getEcsId(){
		return this.ecsId;
	}		
	public void setEcsOperAction(String ecsOperAction){
		this.ecsOperAction = ecsOperAction;
	}
	public String getEcsOperAction(){
		return this.ecsOperAction;
	}		
	public void setEcsMsg(String ecsMsg){
		this.ecsMsg = ecsMsg;
	}
	public String getEcsMsg(){
		return this.ecsMsg;
	}		
	public void setOperUserid(String operUserid){
		this.operUserid = operUserid;
	}
	public String getOperUserid(){
		return this.operUserid;
	}		

}
