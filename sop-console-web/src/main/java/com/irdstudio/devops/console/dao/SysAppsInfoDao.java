package com.irdstudio.devops.console.dao;

import java.util.List;

import com.irdstudio.bfp.console.dao.domain.SubsCodeBase;
import com.irdstudio.devops.console.dao.domain.SysAppsInfo;
import com.irdstudio.devops.console.service.vo.SysAppsInfoVO;
import org.apache.ibatis.annotations.Param;

/**
 * <p>DAO interface:系统应用信息				<p>
 * @author zjj
 * @date 2020-10-27
 */
public interface SysAppsInfoDao {
	
	public int insertSysAppsInfo(SysAppsInfo sysAppsInfo);
	
	public int deleteByPk(SysAppsInfo sysAppsInfo);
	
	public int updateByPk(SysAppsInfo sysAppsInfo);

	public SysAppsInfo queryByPk(SysAppsInfo sysAppsInfo);

	public int updateBySubsCode(SubsCodeBase subsCodeBase);

	public List<SysAppsInfo> queryAllOwnerByPage(SysAppsInfoVO sysAppsInfo);
	
	public List<SysAppsInfo> queryAllCurrOrgByPage(SysAppsInfoVO sysAppsInfo);
	
	public List<SysAppsInfo> queryAllCurrDownOrgByPage(SysAppsInfoVO sysAppsInfo);

}
