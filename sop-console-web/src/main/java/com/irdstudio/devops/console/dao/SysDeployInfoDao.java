package com.irdstudio.devops.console.dao;

import java.util.List;

import com.irdstudio.bfp.console.dao.domain.SubsCodeBase;
import com.irdstudio.devops.console.dao.domain.SysDeployInfo;
import com.irdstudio.devops.console.service.vo.SysDeployInfoVO;
/**
 * <p>DAO interface:系统部署信息				<p>
 * @author zjj
 * @date 2020-10-27
 */
public interface SysDeployInfoDao {
	
	public int insertSysDeployInfo(SysDeployInfo sysDeployInfo);
	
	public int deleteByPk(SysDeployInfo sysDeployInfo);
	
	public int updateByPk(SysDeployInfo sysDeployInfo);

	public int updateBySubsCode(SubsCodeBase subsCodeBase);

	public int updateAppName(SysDeployInfo sysDeployInfo);

	public SysDeployInfo queryByPk(SysDeployInfo sysDeployInfo);
	
	public List<SysDeployInfo> queryAllOwnerByPage(SysDeployInfoVO sysDeployInfo);
	
	public List<SysDeployInfo> queryAllCurrOrgByPage(SysDeployInfoVO sysDeployInfo);
	
	public List<SysDeployInfo> queryAllCurrDownOrgByPage(SysDeployInfoVO sysDeployInfo);

}
