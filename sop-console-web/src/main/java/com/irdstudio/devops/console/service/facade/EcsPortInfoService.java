package com.irdstudio.devops.console.service.facade;

import java.util.List;

import com.irdstudio.devops.console.service.vo.EcsPortInfoVO;

/**
 * <p>Description:服务器开放端口				<p>
 * @author ligm
 * @date 2018-12-31
 */
public interface EcsPortInfoService {
	
	public List<EcsPortInfoVO> queryAllOwner(EcsPortInfoVO ecsPortInfoVo);
	
	public List<EcsPortInfoVO> queryAllCurrOrg(EcsPortInfoVO ecsPortInfoVo);
	
	public List<EcsPortInfoVO> queryAllCurrDownOrg(EcsPortInfoVO ecsPortInfoVo);
	
	public int insertEcsPortInfo(EcsPortInfoVO inEcsPortInfoVo);
	
	public int deleteByPk(EcsPortInfoVO ecsPortInfoVo);
	
	public int updateByPk(EcsPortInfoVO ecsPortInfoVo);
	
	public EcsPortInfoVO queryByPk(EcsPortInfoVO ecsPortInfoVo);

}
