package com.irdstudio.devops.console.api.rest;

import java.util.List;

import com.irdstudio.bfp.console.dao.domain.SubsCodeBase;
import com.irdstudio.devops.console.service.vo.EcsBaseInfoVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.irdstudio.devops.console.service.facade.SysEcsInfoService;
import com.irdstudio.devops.console.service.vo.SysEcsInfoVO;
import com.irdstudio.sdk.beans.core.vo.ResponseData;
import com.irdstudio.sdk.beans.web.controller.AbstractController;

@RestController
@RequestMapping("/api")
public class SysEcsInfoController extends AbstractController  {
	
	@Autowired
	@Qualifier("sysEcsInfoServiceImpl")
	private SysEcsInfoService sysEcsInfoService;

	
	/**
	 * 列表数据查询
	 * @param page
	 * @param size
	 * @return
	 */
	@RequestMapping(value="/sys/ecs/infos", method=RequestMethod.POST)
	public @ResponseBody ResponseData<List<EcsBaseInfoVO>> querySysEcsInfoAll(
			SysEcsInfoVO vo) {
		List<EcsBaseInfoVO> outputVo = sysEcsInfoService.queryAllOwner(vo);
		return getResponseData(outputVo);
		
	}
	
	/**
	 * 根据主键查询详情
	 * @return
	 */
	@RequestMapping(value="/sys/ecs/info/{ecsId}/{subsCode}", method=RequestMethod.GET)
	public @ResponseBody ResponseData<SysEcsInfoVO> queryByPk(@PathVariable("ecsId") String ecsId,@PathVariable("subsCode") String subsCode) {		
		SysEcsInfoVO inVo = new SysEcsInfoVO();
				inVo.setEcsId(ecsId);
				inVo.setSubsId(subsCode);
		SysEcsInfoVO outputVo = sysEcsInfoService.queryByPk(inVo);
		return getResponseData(outputVo);
		
	}
	
	/**
	 * 根据主键删除信息
	 * @param sysEcsInfo
	 * @return
	 */
	@RequestMapping(value="/sys/ecs/info", method=RequestMethod.DELETE)
	public @ResponseBody ResponseData<Integer> deleteByPk(@RequestBody SysEcsInfoVO inSysEcsInfoVo) {		
		int outputVo = sysEcsInfoService.deleteByPk(inSysEcsInfoVo);
		return getResponseData(outputVo);
		
	}
	
	/**
	 * 根据主键更新信息
	 * @param inSysEcsInfoVo
	 * @return
	 */
	@RequestMapping(value="/sys/ecs/info", method=RequestMethod.PUT)
	public @ResponseBody ResponseData<Integer> updateByPk(@RequestBody SysEcsInfoVO inSysEcsInfoVo) {		
		int outputVo = sysEcsInfoService.updateByPk(inSysEcsInfoVo);
		return getResponseData(outputVo);
		
	}

	/**
	 * 更新subsCode
	 * @return
	 */
	@RequestMapping(value="/sys/ecs/info/change/subs/code", method=RequestMethod.PUT)
	public @ResponseBody ResponseData<Integer> updateBySubsCode(@RequestBody SubsCodeBase subsCodeBase) {
		int outputVo = sysEcsInfoService.updateBySubsCode(subsCodeBase);
		return getResponseData(outputVo);
	}
	
	/**
	 * 新增数据
	 * @param inSysEcsInfoVo
	 * @return
	 */
	@RequestMapping(value="/sys/ecs/info", method=RequestMethod.POST)
	public @ResponseBody ResponseData<Integer> insertSysEcsInfo(@RequestBody SysEcsInfoVO inSysEcsInfoVo) {
		int outputVo = sysEcsInfoService.insertSysEcsInfo(inSysEcsInfoVo);
		return getResponseData(outputVo);
		
	}
}
