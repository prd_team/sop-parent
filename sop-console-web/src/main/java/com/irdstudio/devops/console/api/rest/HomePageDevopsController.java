package com.irdstudio.devops.console.api.rest;

import com.irdstudio.devops.console.dao.domain.HomePageInfo;
import com.irdstudio.devops.console.dao.domain.OngoingProcessInfo;
import com.irdstudio.devops.console.service.facade.HomePageInfoService;
import com.irdstudio.devops.console.service.vo.HomePageInfoVO;
import com.irdstudio.devops.console.service.vo.OngoingProcessInfoVO;
import com.irdstudio.sdk.beans.core.vo.ResponseData;
import com.irdstudio.sdk.beans.web.controller.AbstractController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author lwc
 * @Description TODO
 * @createTime 2020年11月25日
 */
@RestController
@RequestMapping("/api")
public class HomePageDevopsController extends AbstractController {
    @Autowired
    @Qualifier("homePageInfoServiceImpl")
    private HomePageInfoService homePageInfoService;

    @RequestMapping(value = "/devops/home/page",method = RequestMethod.POST)
    public @ResponseBody ResponseData<List<HomePageInfoVO>> queryHomePage(HomePageInfo param){
        List<HomePageInfoVO> homePageInfos = homePageInfoService.homePageQueryAll(param);
        return getResponseData(homePageInfos);
    }

    @RequestMapping(value = "/devops/home/page/{bpaState}",method = RequestMethod.POST)
    public @ResponseBody ResponseData<List<OngoingProcessInfoVO>> queryAllOngoingProcess(@PathVariable("bpaState") String bpaState){
        List<OngoingProcessInfoVO> ongoingProcessInfoVOS = homePageInfoService.ongoingProcessQueryAll(bpaState);
        return getResponseData(ongoingProcessInfoVOS);
    }
}
