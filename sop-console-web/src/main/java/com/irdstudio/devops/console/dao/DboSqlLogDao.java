package com.irdstudio.devops.console.dao;

import java.util.List;

import com.irdstudio.devops.console.dao.domain.DboSqlLog;
import com.irdstudio.devops.console.service.vo.DboSqlLogVO;
/**
 * <p>DAO interface:数据运维日志				<p>
 * @author ligm
 * @date 2020-11-04
 */
public interface DboSqlLogDao {
	
	public int insertDboSqlLog(DboSqlLog dboSqlLog);
	
	public int deleteByPk(DboSqlLog dboSqlLog);
	
	public int updateByPk(DboSqlLog dboSqlLog);
	
	public DboSqlLog queryByPk(DboSqlLog dboSqlLog);
	
	public List<DboSqlLog> queryAllOwnerByPage(DboSqlLogVO dboSqlLog);
	
	public List<DboSqlLog> queryAllCurrOrgByPage(DboSqlLogVO dboSqlLog);
	
	public List<DboSqlLog> queryAllCurrDownOrgByPage(DboSqlLogVO dboSqlLog);

}
