package com.irdstudio.devops.console.service.vo;

import com.irdstudio.sdk.beans.core.vo.BaseInfo;
/**
 * <p>Description: 服务器开放端口				<p>
 * @author ligm
 * @date 2018-12-31
 */
public class EcsPortInfoVO extends BaseInfo {

	private static final long serialVersionUID = 1L;	
	
	/** ecs标识 */
	private String ecsId;
	/** 服务器开放端口 */
	private int ecsPort;
	/** 备注 */
	private String remarks;
	

	public void setEcsId(String ecsId){
		this.ecsId = ecsId;
	}
	public String getEcsId(){
		return this.ecsId;
	}		
	public void setEcsPort(int ecsPort){
		this.ecsPort = ecsPort;
	}
	public int getEcsPort(){
		return this.ecsPort;
	}		
	public void setRemarks(String remarks){
		this.remarks = remarks;
	}
	public String getRemarks(){
		return this.remarks;
	}		

}
