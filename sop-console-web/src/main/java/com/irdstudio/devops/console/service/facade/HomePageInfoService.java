package com.irdstudio.devops.console.service.facade;

import com.irdstudio.devops.console.dao.domain.HomePageInfo;
import com.irdstudio.devops.console.dao.domain.OngoingProcessInfo;
import com.irdstudio.devops.console.service.vo.HomePageInfoVO;
import com.irdstudio.devops.console.service.vo.OngoingProcessInfoVO;

import java.util.List;

/**
 * @author lwc
 * @Description TODO
 * @createTime 2020年11月25日
 */
public interface HomePageInfoService {
    /**
     * 查询主页业务系统数据
     * @return
     */
    List<HomePageInfoVO> homePageQueryAll(HomePageInfo param);

    /**
     * 查询主页正在执行的运维流程
     * @param bpaState
     * @return
     */
    List<OngoingProcessInfoVO> ongoingProcessQueryAll(String bpaState);
}
