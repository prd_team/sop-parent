package com.irdstudio.devops.console.dao.domain;

import com.irdstudio.sdk.beans.core.vo.BaseInfo;
/**
 * Description: 服务器信息			
 * @author ligm
 * @date 2019-03-21
 */
public class EcsBaseInfo extends BaseInfo{

	private static final long serialVersionUID = 1L;	
	
	/** ecs标识 */
	private String ecsId;
	/** 服务器IP */
	private String ecsIp;
	/** 服务器名称 */
	private String ecsName;
	/** 描述 */
	private String ecsDesc;
	/** 所在位置 */
	private String ecsRegion;
	/** 操作系统 */
	private String ecsOs;
	/** CPU(核) */
	private int ecsVcpu;
	/** 内存(G) */
	private int ecsMemory;
	/** 服务器磁盘(G) */
	private int ecsDisk;
	/** 服务器用途 */
	private String ecsPurpose;
	/** 服务器登录方式 */
	private String ecsLoginMethod;
	/** 服务器登录用户 */
	private String ecsLoginUser;
	/** 服务器登录密码 */
	private String ecsLoginPwd;
	/** 状态 */
	private String ecsState;
	/** 服务器响应信息 */
	private String ecsMsg;
	

	public void setEcsId(String ecsId){
		this.ecsId = ecsId;
	}
	public String getEcsId(){
		return this.ecsId;
	}		
	public void setEcsIp(String ecsIp){
		this.ecsIp = ecsIp;
	}
	public String getEcsIp(){
		return this.ecsIp;
	}		
	public void setEcsName(String ecsName){
		this.ecsName = ecsName;
	}
	public String getEcsName(){
		return this.ecsName;
	}		
	public void setEcsDesc(String ecsDesc){
		this.ecsDesc = ecsDesc;
	}
	public String getEcsDesc(){
		return this.ecsDesc;
	}		
	public void setEcsRegion(String ecsRegion){
		this.ecsRegion = ecsRegion;
	}
	public String getEcsRegion(){
		return this.ecsRegion;
	}		
	public void setEcsOs(String ecsOs){
		this.ecsOs = ecsOs;
	}
	public String getEcsOs(){
		return this.ecsOs;
	}		
	public void setEcsVcpu(int ecsVcpu){
		this.ecsVcpu = ecsVcpu;
	}
	public int getEcsVcpu(){
		return this.ecsVcpu;
	}		
	public void setEcsMemory(int ecsMemory){
		this.ecsMemory = ecsMemory;
	}
	public int getEcsMemory(){
		return this.ecsMemory;
	}		
	public void setEcsDisk(int ecsDisk){
		this.ecsDisk = ecsDisk;
	}
	public int getEcsDisk(){
		return this.ecsDisk;
	}		
	public void setEcsPurpose(String ecsPurpose){
		this.ecsPurpose = ecsPurpose;
	}
	public String getEcsPurpose(){
		return this.ecsPurpose;
	}		
	public void setEcsLoginMethod(String ecsLoginMethod){
		this.ecsLoginMethod = ecsLoginMethod;
	}
	public String getEcsLoginMethod(){
		return this.ecsLoginMethod;
	}		
	public void setEcsLoginUser(String ecsLoginUser){
		this.ecsLoginUser = ecsLoginUser;
	}
	public String getEcsLoginUser(){
		return this.ecsLoginUser;
	}		
	public void setEcsLoginPwd(String ecsLoginPwd){
		this.ecsLoginPwd = ecsLoginPwd;
	}
	public String getEcsLoginPwd(){
		return this.ecsLoginPwd;
	}		
	public void setEcsState(String ecsState){
		this.ecsState = ecsState;
	}
	public String getEcsState(){
		return this.ecsState;
	}		
	public void setEcsMsg(String ecsMsg){
		this.ecsMsg = ecsMsg;
	}
	public String getEcsMsg(){
		return this.ecsMsg;
	}		

}
