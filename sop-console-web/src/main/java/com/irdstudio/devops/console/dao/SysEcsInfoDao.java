package com.irdstudio.devops.console.dao;

import java.util.List;

import com.irdstudio.bfp.console.dao.domain.SubsCodeBase;
import com.irdstudio.devops.console.dao.domain.EcsBaseInfo;
import com.irdstudio.devops.console.dao.domain.SysEcsInfo;
import com.irdstudio.devops.console.service.vo.EcsBaseInfoVO;
import com.irdstudio.devops.console.service.vo.SysEcsInfoVO;
/**
 * <p>DAO interface:系统服务器信息				<p>
 * @author zjj
 * @date 2020-10-27
 */
public interface SysEcsInfoDao {
	
	public int insertSysEcsInfo(SysEcsInfo sysEcsInfo);
	
	public int deleteByPk(SysEcsInfo sysEcsInfo);
	
	public int updateByPk(SysEcsInfo sysEcsInfo);

	public int updateBySubsCode(SubsCodeBase subsCodeBase);

	public SysEcsInfo queryByPk(SysEcsInfo sysEcsInfo);
	
	public List<EcsBaseInfo> queryAllOwnerByPage(SysEcsInfoVO sysEcsInfo);
	
	public List<SysEcsInfo> queryAllCurrOrgByPage(SysEcsInfoVO sysEcsInfo);
	
	public List<SysEcsInfo> queryAllCurrDownOrgByPage(SysEcsInfoVO sysEcsInfo);

}
