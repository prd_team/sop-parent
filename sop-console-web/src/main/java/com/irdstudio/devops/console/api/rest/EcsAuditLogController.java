package com.irdstudio.devops.console.api.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.irdstudio.devops.console.service.facade.EcsAuditLogService;
import com.irdstudio.devops.console.service.vo.EcsAuditLogVO;
import com.irdstudio.sdk.beans.core.vo.ResponseData;
import com.irdstudio.sdk.beans.web.controller.AbstractController;

@RestController
@RequestMapping("/api")
public class EcsAuditLogController extends AbstractController  {
	
	@Autowired
	@Qualifier("ecsAuditLogServiceImpl")
	private EcsAuditLogService ecsAuditLogService;

	
	/**
	 * 列表数据查询
	 * @param page
	 * @param size
	 * @return
	 */
	@RequestMapping(value="/ecs/audit/logs", method=RequestMethod.POST)
	public @ResponseBody ResponseData<List<EcsAuditLogVO>> queryEcsAuditLogAll(
			EcsAuditLogVO vo) {		
		List<EcsAuditLogVO> outputVo = ecsAuditLogService.queryAllOwner(vo);
		return getResponseData(outputVo);
		
	}
	
	/**
	 * 根据主键查询详情
	 * @return
	 */
	@RequestMapping(value="/ecs/audit/log/{recordKeyid}", method=RequestMethod.GET)
	public @ResponseBody ResponseData<EcsAuditLogVO> queryByPk(@PathVariable("recordKeyid") String recordKeyid) {		
		EcsAuditLogVO inVo = new EcsAuditLogVO();
				inVo.setRecordKeyid(recordKeyid);
		EcsAuditLogVO outputVo = ecsAuditLogService.queryByPk(inVo);
		return getResponseData(outputVo);
		
	}
	
	/**
	 * 根据主键删除信息
	 * @param ecsAuditLog
	 * @return
	 */
	@RequestMapping(value="/ecs/audit/log", method=RequestMethod.DELETE)
	public @ResponseBody ResponseData<Integer> deleteByPk(@RequestBody EcsAuditLogVO inEcsAuditLogVo) {		
		int outputVo = ecsAuditLogService.deleteByPk(inEcsAuditLogVo);
		return getResponseData(outputVo);
		
	}
	
	/**
	 * 根据主键更新信息
	 * @param inEcsAuditLogVo
	 * @return
	 */
	@RequestMapping(value="/ecs/audit/log", method=RequestMethod.PUT)
	public @ResponseBody ResponseData<Integer> updateByPk(@RequestBody EcsAuditLogVO inEcsAuditLogVo) {		
		int outputVo = ecsAuditLogService.updateByPk(inEcsAuditLogVo);
		return getResponseData(outputVo);
		
	}
	
	/**
	 * 新增数据
	 * @param inEcsAuditLogVo
	 * @return
	 */
	@RequestMapping(value="/ecs/audit/log", method=RequestMethod.POST)
	public @ResponseBody ResponseData<Integer> insertEcsAuditLog(@RequestBody EcsAuditLogVO inEcsAuditLogVo) {
		int outputVo = ecsAuditLogService.insertEcsAuditLog(inEcsAuditLogVo);
		return getResponseData(outputVo);
		
	}
}
