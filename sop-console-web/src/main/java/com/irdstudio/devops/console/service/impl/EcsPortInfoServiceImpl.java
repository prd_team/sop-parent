package com.irdstudio.devops.console.service.impl;

import java.util.List;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.irdstudio.devops.console.dao.EcsPortInfoDao;
import com.irdstudio.devops.console.dao.domain.EcsPortInfo;
import com.irdstudio.devops.console.service.facade.EcsPortInfoService;
import com.irdstudio.devops.console.service.vo.EcsPortInfoVO;
import com.irdstudio.sdk.beans.core.base.FrameworkService;
/**
 * <p>ServiceImpl: 服务器开放端口				<p>
 * @author ligm
 * @date 2018-12-31
 */
@Service("ecsPortInfoServiceImpl")
public class EcsPortInfoServiceImpl implements EcsPortInfoService, FrameworkService {
	
	private static Logger logger = LoggerFactory.getLogger(EcsPortInfoServiceImpl.class);

	@Autowired
	private EcsPortInfoDao ecsPortInfoDao;
	
	/**
	 * 新增操作
	 */
	public int insertEcsPortInfo(EcsPortInfoVO inEcsPortInfoVo) {
		logger.debug("当前新增数据为:"+ inEcsPortInfoVo.toString());
		int num = 0;
		try {
			EcsPortInfo ecsPortInfo = new EcsPortInfo();
			beanCopy(inEcsPortInfoVo, ecsPortInfo);
			num = ecsPortInfoDao.insertEcsPortInfo(ecsPortInfo);
		} catch (Exception e) {
			logger.error("新增数据发生异常!", e);
			num = -1;
		}
		logger.debug("当前新增数据条数为:"+ num);
		return num;
	}

	/**
	 * 删除操作
	 */
	public int deleteByPk(EcsPortInfoVO inEcsPortInfoVo) {
		logger.debug("当前删除数据条件为:"+ inEcsPortInfoVo);
		int num = 0;
		try {
			EcsPortInfo ecsPortInfo = new EcsPortInfo();
			beanCopy(inEcsPortInfoVo, ecsPortInfo);
			num = ecsPortInfoDao.deleteByPk(ecsPortInfo);
		} catch (Exception e) {
			logger.error("删除数据发生异常!", e);
			num = -1;
		}
		logger.debug("根据条件:"+ inEcsPortInfoVo +"删除的数据条数为"+ num);
		return num;
	}


	/**
	 * 更新操作
	 */
	public int updateByPk(EcsPortInfoVO inEcsPortInfoVo) {
		logger.debug("当前修改数据为:"+ inEcsPortInfoVo.toString());
		int num = 0;
		try {
			EcsPortInfo ecsPortInfo = new EcsPortInfo();
			beanCopy(inEcsPortInfoVo, ecsPortInfo);
			num = ecsPortInfoDao.updateByPk(ecsPortInfo);
		} catch (Exception e) {
			logger.error("修改数据发生异常!", e);
			num = -1;
		}
		logger.debug("根据条件:"+ inEcsPortInfoVo +"修改的数据条数为"+ num);
		return num;
	}
	
	/**
	 * 查询操作
	 */
	public EcsPortInfoVO queryByPk(EcsPortInfoVO inEcsPortInfoVo) {
		
		logger.debug("当前查询参数信息为:"+ inEcsPortInfoVo);
		try {
			EcsPortInfo queryEcsPortInfo = new EcsPortInfo();
			beanCopy(inEcsPortInfoVo, queryEcsPortInfo);
			EcsPortInfo queryRslEcsPortInfo = ecsPortInfoDao.queryByPk(queryEcsPortInfo);
			if (Objects.nonNull(queryRslEcsPortInfo)) {
				EcsPortInfoVO outEcsPortInfoVo = beanCopy(queryRslEcsPortInfo, new EcsPortInfoVO());
				logger.debug("当前查询结果为:"+ outEcsPortInfoVo.toString());
				return outEcsPortInfoVo;
			} else {
				logger.debug("当前查询结果为空!");
			}
		} catch (Exception e) {
			logger.error("查询数据发生异常!", e);
		}
		return null;
	}


	/**
	 * 查询用户权限数据
	 */
	@SuppressWarnings("unchecked")
	public List<EcsPortInfoVO> queryAllOwner(EcsPortInfoVO ecsPortInfoVo) {

		logger.debug("当前查询本人所属数据信息的参数信息为:");
		List<EcsPortInfoVO> list = null;
		try {
			List<EcsPortInfo> ecsPortInfos = ecsPortInfoDao.queryAllOwnerByPage(ecsPortInfoVo);
			logger.debug("当前查询本人所属数据信息的结果集数量为:"+ ecsPortInfos.size());
			pageSet(ecsPortInfos, ecsPortInfoVo);
			list = (List<EcsPortInfoVO>) beansCopy(ecsPortInfos, EcsPortInfoVO.class);
		} catch (Exception e) {
			logger.error("数据转换出现异常!", e);
		}
		
		return list;
	
	}


	/**
	 * 查询当前机构权限数据
	 */
	@SuppressWarnings("unchecked")
	public List<EcsPortInfoVO> queryAllCurrOrg(EcsPortInfoVO ecsPortInfoVo) {

		logger.debug("当前查询本人所属机构数据信息的参数信息为:");
		List<EcsPortInfo> ecsPortInfos = ecsPortInfoDao.queryAllCurrOrgByPage(ecsPortInfoVo);
		logger.debug("当前查询本人所属机构数据信息的结果集数量为:"+ecsPortInfos.size());
		List<EcsPortInfoVO> list = null;
		try {
			pageSet(ecsPortInfos, ecsPortInfoVo);
			list = (List<EcsPortInfoVO>) beansCopy(ecsPortInfos, EcsPortInfoVO.class);
		} catch (Exception e) {
			logger.error("数据转换出现异常!", e);
		}
		
		return list;
	
	}


	/**
	 * 查询当前机构及下属机构权限数据
	 */
	@SuppressWarnings("unchecked")
	public List<EcsPortInfoVO> queryAllCurrDownOrg(EcsPortInfoVO ecsPortInfoVo) {

		logger.debug("当前查询本人所属机构及以下数据信息的参数信息为:");
		List<EcsPortInfo> ecsPortInfos = ecsPortInfoDao.queryAllCurrDownOrgByPage(ecsPortInfoVo);
		logger.debug("当前查询本人所属机构及以下数据信息的结果集数量为:"+ ecsPortInfos.size());
		List<EcsPortInfoVO> list = null;
		try {
			pageSet(ecsPortInfos, ecsPortInfoVo);
			list = (List<EcsPortInfoVO>) beansCopy(ecsPortInfos, EcsPortInfoVO.class);
		} catch (Exception e) {
			logger.error("数据转换出现异常!", e);
		}
		
		return list;
	
	}
}
