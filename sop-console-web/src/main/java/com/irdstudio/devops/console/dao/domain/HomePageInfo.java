package com.irdstudio.devops.console.dao.domain;

import com.irdstudio.sdk.beans.core.vo.BaseInfo;

/**
 * @author lwc
 * @Description TODO
 * @createTime 2020年11月25日
 */
public class HomePageInfo extends BaseInfo {
    private static final long serialVersionUID = 1L;

    /**系统编号 */
    private String subsId;
    /**系统代码 */
    private String subsCode;
    /**系统名称 */
    private String subsName;
    /**应用数量 */
    private String appNumber;
    /**服务器数量 */
    private String escNumber;
    /**自动运维流程 */
    private String automaticProcess;
    /**手工运维流程 */
    private String manualProcesses;
    /**执行中 */
    private String hpInService;
    /**异常 */
    private String hpError;
    /**结束 */
    private String hpFinish;

    public String getSubsCode() {
        return subsCode;
    }

    public void setSubsCode(String subsCode) {
        this.subsCode = subsCode;
    }

    public String getSubsName() {
        return subsName;
    }

    public void setSubsName(String subsName) {
        this.subsName = subsName;
    }

    public String getAppNumber() {
        return appNumber;
    }

    public void setAppNumber(String appNumber) {
        this.appNumber = appNumber;
    }

    public String getEscNumber() {
        return escNumber;
    }

    public void setEscNumber(String escNumber) {
        this.escNumber = escNumber;
    }

    public String getAutomaticProcess() {
        return automaticProcess;
    }

    public void setAutomaticProcess(String automaticProcess) {
        this.automaticProcess = automaticProcess;
    }

    public String getManualProcesses() {
        return manualProcesses;
    }

    public void setManualProcesses(String manualProcesses) {
        this.manualProcesses = manualProcesses;
    }

    public String getHpInService() {
        return hpInService;
    }

    public void setHpInService(String hpInService) {
        this.hpInService = hpInService;
    }

    public String getHpError() {
        return hpError;
    }

    public void setHpError(String hpError) {
        this.hpError = hpError;
    }

    public String getHpFinish() {
        return hpFinish;
    }

    public void setHpFinish(String hpFinish) {
        this.hpFinish = hpFinish;
    }

    public String getSubsId() {
        return subsId;
    }

    public void setSubsId(String subsId) {
        this.subsId = subsId;
    }
}
