package com.irdstudio.devops.console.service.impl;

import java.util.List;
import java.util.Objects;

import com.irdstudio.bfp.console.dao.domain.SubsCodeBase;
import com.irdstudio.bfp.executor.core.utils.TimeUtil;
import com.irdstudio.bfp.console.dao.BpmBaseInfoDao;
import com.irdstudio.bfp.console.dao.BpmLinkedInfoDao;
import com.irdstudio.bfp.console.dao.BpmNodeInfoDao;
import com.irdstudio.bfp.console.dao.domain.BpmBaseInfo;
import com.irdstudio.bfp.console.dao.domain.BpmLinkedInfo;
import com.irdstudio.bfp.console.dao.domain.BpmNodeInfo;
import com.irdstudio.bfp.console.service.vo.BpmBaseInfoVO;
import com.irdstudio.devops.console.dao.SysAppsInfoDao;
import com.irdstudio.sdk.beans.core.util.KeyUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.irdstudio.devops.console.service.facade.SysAppsInfoService;
import com.irdstudio.devops.console.dao.domain.SysAppsInfo;
import com.irdstudio.devops.console.service.vo.SysAppsInfoVO;
import com.irdstudio.sdk.beans.core.base.FrameworkService;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>ServiceImpl: 系统应用信息				<p>
 * @author zjj
 * @date 2020-10-27
 */
@Service("sysAppsInfoServiceImpl")
public class SysAppsInfoServiceImpl implements SysAppsInfoService, FrameworkService {
	
	private static Logger logger = LoggerFactory.getLogger(SysAppsInfoServiceImpl.class);

	@Autowired
	private SysAppsInfoDao sysAppsInfoDao;
	@Autowired
	private BpmBaseInfoDao bpmBaseInfoDao;
	@Autowired
	private BpmNodeInfoDao bpmNodeInfoDao;
	@Autowired
	private BpmLinkedInfoDao bpmLinkedInfoDao;
	
	/**
	 * 新增操作
	 */
	@Override
	public int insertSysAppsInfo(SysAppsInfoVO inSysAppsInfoVo) {
		logger.debug("当前新增数据为:"+ inSysAppsInfoVo.toString());
		int num = 0;
		try {
			//1.查询是否存在相同的appCode(应用代码)
			String appCode = inSysAppsInfoVo.getAppCode();
			SysAppsInfoVO sysAppsInfoVO = new SysAppsInfoVO();
			sysAppsInfoVO.setAppCode(appCode);
			List<SysAppsInfo> sysAppsInfoList = sysAppsInfoDao.queryAllOwnerByPage(sysAppsInfoVO);

			//2.查询是否存在相同的bpmCode(流程编号)
			String bpmCode = inSysAppsInfoVo.getAppCode();
			BpmBaseInfoVO bpmBaseInfoVO = new BpmBaseInfoVO();
			bpmBaseInfoVO.setBpmCode(bpmCode);
			List<BpmBaseInfo> bpmBaseInfoList = bpmBaseInfoDao.queryAllOwnerByPage(bpmBaseInfoVO);
			if (sysAppsInfoList.size()>0||bpmBaseInfoList.size()>0){
				//3.该应用代码或流程编号已存在记录,返回
				num = -2;
			}else {
				//4.应用代码不存在，新增系统应用信息
				SysAppsInfo sysAppsInfo = new SysAppsInfo();
				beanCopy(inSysAppsInfoVo, sysAppsInfo);
				sysAppsInfo.setAppId(KeyUtil.createUUIDKey());
				sysAppsInfo.setCreateTime(TimeUtil.getCurrentDateTime());
				sysAppsInfo.setCreateUser("admin");
				num = sysAppsInfoDao.insertSysAppsInfo(sysAppsInfo);


				bpmBaseInfoVO.setBpmId(sysAppsInfo.getAppId());
				com.irdstudio.bfp.console.dao.domain.BpmBaseInfo bpmBaseInfo = new BpmBaseInfo();
				beanCopy(bpmBaseInfoVO, bpmBaseInfo);
				bpmBaseInfo.setBpmName(sysAppsInfo.getAppCode());
				bpmBaseInfo.setSubsId(inSysAppsInfoVo.getSubsId());
				bpmBaseInfo.setBpmState("0"); //0-未发布  1-已发布
				bpmBaseInfo.setBpmVersion("0.0.0");
				bpmBaseInfo.setCreateUser("admin");
				bpmBaseInfo.setBpmType("1");
				bpmBaseInfo.setCategoryTag("continuous");
				bpmBaseInfo.setLatestState("N");
				bpmBaseInfo.setEffectState("Y");
				bpmBaseInfo.setIsRunAgain("Y");
				bpmBaseInfo.setEquallyTaskAmount(10);
				bpmBaseInfo.setCreateTime(com.irdstudio.sdk.ssm.util.TimeUtil.getCurrentDateTime());
				num = bpmBaseInfoDao.insertBpmBaseInfo(bpmBaseInfo);

				if (num>0){
					// 初始化一个起始节点、git插件节点、maven插件节点、结束节点，并链接
					BpmNodeInfo startNode = new BpmNodeInfo();
					startNode.setBpmId(bpmBaseInfo.getBpmId());
					startNode.setBpmNodeId(KeyUtil.createUUIDKey());
					startNode.setBpmNodeType("N01");
					startNode.setBpmNodeName("开始节点");
					startNode.setH(35);
					startNode.setW(200);
					startNode.setX(215);
					startNode.setY(25);
					bpmNodeInfoDao.insertBpmNodeInfo(startNode);

					BpmNodeInfo gitNode = new BpmNodeInfo();
					gitNode.setBpmId(bpmBaseInfo.getBpmId());
					gitNode.setBpmNodeId(KeyUtil.createUUIDKey());
					gitNode.setBpmNodeType("N07");
					gitNode.setBpmNodeName("git更新插件");
					gitNode.setBpmNodeCode("1095");
					gitNode.setBpmNodeParam("{\"gitRemotePath\":\""+sysAppsInfo.getGitUrl()+"\"}");
					gitNode.setH(35);
					gitNode.setW(200);
					gitNode.setX(215);
					gitNode.setY(startNode.getY() + 100);
					bpmNodeInfoDao.insertBpmNodeInfo(gitNode);

					BpmLinkedInfo bpmLinkedInfo = new BpmLinkedInfo();
					bpmLinkedInfo.setLinkedId(KeyUtil.createUUIDKey());
					bpmLinkedInfo.setSourceNodeId(startNode.getBpmNodeId());
					bpmLinkedInfo.setTargetNodeId(gitNode.getBpmNodeId());
					bpmLinkedInfo.setX1(314);
					bpmLinkedInfo.setY1(66);
					bpmLinkedInfo.setX2(314);
					bpmLinkedInfo.setY2(118);
					bpmLinkedInfo.setLinkedDesc("start-->git");
					bpmLinkedInfoDao.insertBpmLinkedInfo(bpmLinkedInfo);


					BpmNodeInfo mavenNode = new BpmNodeInfo();
					mavenNode.setBpmId(bpmBaseInfo.getBpmId());
					mavenNode.setBpmNodeId(KeyUtil.createUUIDKey());
					mavenNode.setBpmNodeType("N07");
					mavenNode.setBpmNodeName("代码构建插件(MAVEN)");
					mavenNode.setBpmNodeCode("1071");
					mavenNode.setH(35);
					mavenNode.setW(200);
					mavenNode.setX(215);
					mavenNode.setY(gitNode.getY() + 100);
					bpmNodeInfoDao.insertBpmNodeInfo(mavenNode);

					bpmLinkedInfo.setLinkedId(KeyUtil.createUUIDKey());
					bpmLinkedInfo.setSourceNodeId(gitNode.getBpmNodeId());
					bpmLinkedInfo.setTargetNodeId(mavenNode.getBpmNodeId());
					bpmLinkedInfo.setLinkedDesc("git-->maven");
					bpmLinkedInfo.setX1(314);
					bpmLinkedInfo.setY1(166);
					bpmLinkedInfo.setX2(314);
					bpmLinkedInfo.setY2(218);
					bpmLinkedInfoDao.insertBpmLinkedInfo(bpmLinkedInfo);


					BpmNodeInfo sshNode = new BpmNodeInfo();
					sshNode.setBpmId(bpmBaseInfo.getBpmId());
					sshNode.setBpmNodeId(KeyUtil.createUUIDKey());
					sshNode.setBpmNodeType("N07");
					sshNode.setBpmNodeName("代码部署插件(SSH)");
					sshNode.setBpmNodeCode("1072");
					sshNode.setH(35);
					sshNode.setW(200);
					sshNode.setX(215);
					sshNode.setY(mavenNode.getY() + 100);
					bpmNodeInfoDao.insertBpmNodeInfo(sshNode);

					bpmLinkedInfo.setLinkedId(KeyUtil.createUUIDKey());
					bpmLinkedInfo.setSourceNodeId(mavenNode.getBpmNodeId());
					bpmLinkedInfo.setTargetNodeId(sshNode.getBpmNodeId());
					bpmLinkedInfo.setLinkedDesc("maven-->ssh");
					bpmLinkedInfo.setX1(314);
					bpmLinkedInfo.setY1(266);
					bpmLinkedInfo.setX2(314);
					bpmLinkedInfo.setY2(318);
					bpmLinkedInfoDao.insertBpmLinkedInfo(bpmLinkedInfo);


					BpmNodeInfo endNode = new BpmNodeInfo();
					endNode.setBpmId(bpmBaseInfo.getBpmId());
					endNode.setBpmNodeId(KeyUtil.createUUIDKey());
					endNode.setBpmNodeType("N99");
					endNode.setBpmNodeName("结束节点");
					endNode.setH(35);
					endNode.setW(200);
					endNode.setX(215);
					endNode.setY(sshNode.getY() + 100);
					bpmNodeInfoDao.insertBpmNodeInfo(endNode);

					bpmLinkedInfo.setLinkedId(KeyUtil.createUUIDKey());
					bpmLinkedInfo.setSourceNodeId(sshNode.getBpmNodeId());
					bpmLinkedInfo.setTargetNodeId(endNode.getBpmNodeId());
					bpmLinkedInfo.setLinkedDesc("ssh-->end");
					bpmLinkedInfo.setX1(314);
					bpmLinkedInfo.setY1(366);
					bpmLinkedInfo.setX2(314);
					bpmLinkedInfo.setY2(418);
					bpmLinkedInfoDao.insertBpmLinkedInfo(bpmLinkedInfo);


				}
			}
		} catch (Exception e) {
			logger.error("新增数据发生异常!", e);
			num = -1;
		}
		logger.debug("当前新增数据条数为:"+ num);
		return num;
	}

	/**
	 * 删除操作
	 */
	@Override
	public int deleteByPk(SysAppsInfoVO inSysAppsInfoVo) {
		logger.debug("当前删除数据条件为:"+ inSysAppsInfoVo);
		int num = 0;
		try {
			SysAppsInfo sysAppsInfo = new SysAppsInfo();
			beanCopy(inSysAppsInfoVo, sysAppsInfo);
			num = sysAppsInfoDao.deleteByPk(sysAppsInfo);
		} catch (Exception e) {
			logger.error("删除数据发生异常!", e);
			num = -1;
		}
		logger.debug("根据条件:"+ inSysAppsInfoVo +"删除的数据条数为"+ num);
		return num;
	}

	/**
	 * 删除操作2
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public int deleteAppByPKAndAppCode(SysAppsInfoVO inSysAppsInfoVo) {
		logger.debug("当前删除数据条件为:"+ inSysAppsInfoVo);
		int appNum = 0;
		int linkedNum = 0;
		int nodeNum = 0;
		int baseNum = 0;
		try {
			SysAppsInfo sysAppsInfo = new SysAppsInfo();

			BpmBaseInfo bpmBaseInfo = new BpmBaseInfo();
			bpmBaseInfo.setBpmCode(inSysAppsInfoVo.getAppCode());
			linkedNum = bpmLinkedInfoDao.deleteByBpmCode(bpmBaseInfo);
			nodeNum = bpmNodeInfoDao.deleteByBpmCode(bpmBaseInfo);
			baseNum = bpmBaseInfoDao.deleteByBpmCode(bpmBaseInfo);

			beanCopy(inSysAppsInfoVo, sysAppsInfo);
			appNum = sysAppsInfoDao.deleteByPk(sysAppsInfo);
		} catch (Exception e) {
			logger.error("删除数据发生异常!", e);
			appNum = -1;
		}
		logger.debug("根据条件:"+ inSysAppsInfoVo +"删除的数据条数分别为：linkedNum:{},nodeNum:{},baseNum:{},appNum:{}",linkedNum,nodeNum,baseNum, appNum);
		return appNum;
	}

	/**
	 * 更新操作
	 */
	@Override
	public int updateByPk(SysAppsInfoVO inSysAppsInfoVo) {
		logger.debug("当前修改数据为:"+ inSysAppsInfoVo.toString());
		int num = 0;
		try {
			SysAppsInfo sysAppsInfo = new SysAppsInfo();
			beanCopy(inSysAppsInfoVo, sysAppsInfo);
			sysAppsInfo.setLastUpdateUser("admin");
			sysAppsInfo.setLastUpdateTime(TimeUtil.getCurrentDateTime());
			num = sysAppsInfoDao.updateByPk(sysAppsInfo);
		} catch (Exception e) {
			logger.error("修改数据发生异常!", e);
			num = -1;
		}
		logger.debug("根据条件:"+ inSysAppsInfoVo +"修改的数据条数为"+ num);
		return num;
	}

	/**
	 *更新subsCode
	 * @return
	 */
	@Override
	public int updateBySubsCode(SubsCodeBase subsCodeBase) {
		logger.debug("当前修改数据为:"+ subsCodeBase.toString());
		int num = 0;
		try {
			num = sysAppsInfoDao.updateBySubsCode(subsCodeBase);
		} catch (Exception e) {
			logger.error("修改数据发生异常!", e);
			num = -1;
		}
		logger.debug("根据条件:"+ subsCodeBase.getOldSubsCode() +"修改的数据条数为"+ num);
		return 0;
	}

	/**
	 * 查询操作
	 */
	@Override
	public SysAppsInfoVO queryByPk(SysAppsInfoVO inSysAppsInfoVo) {
		
		logger.debug("当前查询参数信息为:"+ inSysAppsInfoVo);
		try {
			SysAppsInfo querySysAppsInfo = new SysAppsInfo();
			beanCopy(inSysAppsInfoVo, querySysAppsInfo);
			SysAppsInfo queryRslSysAppsInfo = sysAppsInfoDao.queryByPk(querySysAppsInfo);
			if (Objects.nonNull(queryRslSysAppsInfo)) {
				SysAppsInfoVO outSysAppsInfoVo = beanCopy(queryRslSysAppsInfo, new SysAppsInfoVO());
				logger.debug("当前查询结果为:"+ outSysAppsInfoVo.toString());
				return outSysAppsInfoVo;
			} else {
				logger.debug("当前查询结果为空!");
			}
		} catch (Exception e) {
			logger.error("查询数据发生异常!", e);
		}
		return null;
	}


	/**
	 * 查询用户权限数据
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<SysAppsInfoVO> queryAllOwner(SysAppsInfoVO sysAppsInfoVo) {

		logger.debug("当前查询本人所属数据信息的参数信息为:");
		List<SysAppsInfoVO> list = null;
		try {
			List<SysAppsInfo> sysAppsInfos = sysAppsInfoDao.queryAllOwnerByPage(sysAppsInfoVo);
			logger.debug("当前查询本人所属数据信息的结果集数量为:"+ sysAppsInfos.size());
			pageSet(sysAppsInfos, sysAppsInfoVo);
			list = (List<SysAppsInfoVO>) beansCopy(sysAppsInfos, SysAppsInfoVO.class);
		} catch (Exception e) {
			logger.error("数据转换出现异常!", e);
		}
		
		return list;
	
	}


	/**
	 * 查询当前机构权限数据
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<SysAppsInfoVO> queryAllCurrOrg(SysAppsInfoVO sysAppsInfoVo) {

		logger.debug("当前查询本人所属机构数据信息的参数信息为:");
		List<SysAppsInfo> sysAppsInfos = sysAppsInfoDao.queryAllCurrOrgByPage(sysAppsInfoVo);
		logger.debug("当前查询本人所属机构数据信息的结果集数量为:"+sysAppsInfos.size());
		List<SysAppsInfoVO> list = null;
		try {
			pageSet(sysAppsInfos, sysAppsInfoVo);
			list = (List<SysAppsInfoVO>) beansCopy(sysAppsInfos, SysAppsInfoVO.class);
		} catch (Exception e) {
			logger.error("数据转换出现异常!", e);
		}
		
		return list;
	
	}


	/**
	 * 查询当前机构及下属机构权限数据
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<SysAppsInfoVO> queryAllCurrDownOrg(SysAppsInfoVO sysAppsInfoVo) {

		logger.debug("当前查询本人所属机构及以下数据信息的参数信息为:");
		List<SysAppsInfo> sysAppsInfos = sysAppsInfoDao.queryAllCurrDownOrgByPage(sysAppsInfoVo);
		logger.debug("当前查询本人所属机构及以下数据信息的结果集数量为:"+ sysAppsInfos.size());
		List<SysAppsInfoVO> list = null;
		try {
			pageSet(sysAppsInfos, sysAppsInfoVo);
			list = (List<SysAppsInfoVO>) beansCopy(sysAppsInfos, SysAppsInfoVO.class);
		} catch (Exception e) {
			logger.error("数据转换出现异常!", e);
		}
		
		return list;
	
	}
}
