package com.irdstudio.devops.console.common.sdicemun;

/**
 * 是否字典
 * 
 * @author fangmingfei1991@163.com
 * @since 2019年4月9日 下午8:14:52
 * @version 1.0
 */
public enum StdPubYnEnum {
	Y("是"), // 是
	N("否");// 否
	private String cnname;

	private StdPubYnEnum(String cnname) {
		this.cnname = cnname;
	}

	public String getCnname() {
		return cnname;
	}
}
