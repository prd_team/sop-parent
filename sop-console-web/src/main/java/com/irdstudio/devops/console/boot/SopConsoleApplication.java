package com.irdstudio.devops.console.boot;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.irdstudio.sdk", "com.irdstudio.bfp.console","com.irdstudio.bfp.executor", "com.irdstudio.devops.console"})
@MapperScan({"com.irdstudio.sdk", "com.irdstudio.bfp.console","com.irdstudio.bfp.executor", "com.irdstudio.devops.console"})
public class SopConsoleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SopConsoleApplication.class, args);
    }
}


