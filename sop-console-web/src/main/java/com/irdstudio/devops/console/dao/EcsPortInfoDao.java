package com.irdstudio.devops.console.dao;

import java.util.List;

import com.irdstudio.devops.console.dao.domain.EcsPortInfo;
import com.irdstudio.devops.console.service.vo.EcsPortInfoVO;
/**
 * <p>DAO interface:服务器开放端口				<p>
 * @author ligm
 * @date 2018-12-31
 */
public interface EcsPortInfoDao {
	
	public int insertEcsPortInfo(EcsPortInfo ecsPortInfo);
	
	public int deleteByPk(EcsPortInfo ecsPortInfo);
	
	public int updateByPk(EcsPortInfo ecsPortInfo);
	
	public EcsPortInfo queryByPk(EcsPortInfo ecsPortInfo);
	
	public List<EcsPortInfo> queryAllOwnerByPage(EcsPortInfoVO ecsPortInfo);
	
	public List<EcsPortInfo> queryAllCurrOrgByPage(EcsPortInfoVO ecsPortInfo);
	
	public List<EcsPortInfo> queryAllCurrDownOrgByPage(EcsPortInfoVO ecsPortInfo);

}
