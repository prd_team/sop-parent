package com.irdstudio.devops.console.service.impl;

import java.util.List;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.irdstudio.devops.console.dao.EcsAuditLogDao;
import com.irdstudio.devops.console.dao.domain.EcsAuditLog;
import com.irdstudio.devops.console.service.facade.EcsAuditLogService;
import com.irdstudio.devops.console.service.vo.EcsAuditLogVO;
import com.irdstudio.sdk.beans.core.base.FrameworkService;
/**
 * <p>ServiceImpl: 服务器操作日志				<p>
 * @author ligm
 * @date 2018-12-31
 */
@Service("ecsAuditLogServiceImpl")
public class EcsAuditLogServiceImpl implements EcsAuditLogService, FrameworkService {
	
	private static Logger logger = LoggerFactory.getLogger(EcsAuditLogServiceImpl.class);

	@Autowired
	private EcsAuditLogDao ecsAuditLogDao;
	
	/**
	 * 新增操作
	 */
	public int insertEcsAuditLog(EcsAuditLogVO inEcsAuditLogVo) {
		logger.debug("当前新增数据为:"+ inEcsAuditLogVo.toString());
		int num = 0;
		try {
			EcsAuditLog ecsAuditLog = new EcsAuditLog();
			beanCopy(inEcsAuditLogVo, ecsAuditLog);
			num = ecsAuditLogDao.insertEcsAuditLog(ecsAuditLog);
		} catch (Exception e) {
			logger.error("新增数据发生异常!", e);
			num = -1;
		}
		logger.debug("当前新增数据条数为:"+ num);
		return num;
	}

	/**
	 * 删除操作
	 */
	public int deleteByPk(EcsAuditLogVO inEcsAuditLogVo) {
		logger.debug("当前删除数据条件为:"+ inEcsAuditLogVo);
		int num = 0;
		try {
			EcsAuditLog ecsAuditLog = new EcsAuditLog();
			beanCopy(inEcsAuditLogVo, ecsAuditLog);
			num = ecsAuditLogDao.deleteByPk(ecsAuditLog);
		} catch (Exception e) {
			logger.error("删除数据发生异常!", e);
			num = -1;
		}
		logger.debug("根据条件:"+ inEcsAuditLogVo +"删除的数据条数为"+ num);
		return num;
	}


	/**
	 * 更新操作
	 */
	public int updateByPk(EcsAuditLogVO inEcsAuditLogVo) {
		logger.debug("当前修改数据为:"+ inEcsAuditLogVo.toString());
		int num = 0;
		try {
			EcsAuditLog ecsAuditLog = new EcsAuditLog();
			beanCopy(inEcsAuditLogVo, ecsAuditLog);
			num = ecsAuditLogDao.updateByPk(ecsAuditLog);
		} catch (Exception e) {
			logger.error("修改数据发生异常!", e);
			num = -1;
		}
		logger.debug("根据条件:"+ inEcsAuditLogVo +"修改的数据条数为"+ num);
		return num;
	}
	
	/**
	 * 查询操作
	 */
	public EcsAuditLogVO queryByPk(EcsAuditLogVO inEcsAuditLogVo) {
		
		logger.debug("当前查询参数信息为:"+ inEcsAuditLogVo);
		try {
			EcsAuditLog queryEcsAuditLog = new EcsAuditLog();
			beanCopy(inEcsAuditLogVo, queryEcsAuditLog);
			EcsAuditLog queryRslEcsAuditLog = ecsAuditLogDao.queryByPk(queryEcsAuditLog);
			if (Objects.nonNull(queryRslEcsAuditLog)) {
				EcsAuditLogVO outEcsAuditLogVo = beanCopy(queryRslEcsAuditLog, new EcsAuditLogVO());
				logger.debug("当前查询结果为:"+ outEcsAuditLogVo.toString());
				return outEcsAuditLogVo;
			} else {
				logger.debug("当前查询结果为空!");
			}
		} catch (Exception e) {
			logger.error("查询数据发生异常!", e);
		}
		return null;
	}


	/**
	 * 查询用户权限数据
	 */
	@SuppressWarnings("unchecked")
	public List<EcsAuditLogVO> queryAllOwner(EcsAuditLogVO ecsAuditLogVo) {

		logger.debug("当前查询本人所属数据信息的参数信息为:");
		List<EcsAuditLogVO> list = null;
		try {
			List<EcsAuditLog> ecsAuditLogs = ecsAuditLogDao.queryAllOwnerByPage(ecsAuditLogVo);
			logger.debug("当前查询本人所属数据信息的结果集数量为:"+ ecsAuditLogs.size());
			pageSet(ecsAuditLogs, ecsAuditLogVo);
			list = (List<EcsAuditLogVO>) beansCopy(ecsAuditLogs, EcsAuditLogVO.class);
		} catch (Exception e) {
			logger.error("数据转换出现异常!", e);
		}
		
		return list;
	
	}


	/**
	 * 查询当前机构权限数据
	 */
	@SuppressWarnings("unchecked")
	public List<EcsAuditLogVO> queryAllCurrOrg(EcsAuditLogVO ecsAuditLogVo) {

		logger.debug("当前查询本人所属机构数据信息的参数信息为:");
		List<EcsAuditLog> ecsAuditLogs = ecsAuditLogDao.queryAllCurrOrgByPage(ecsAuditLogVo);
		logger.debug("当前查询本人所属机构数据信息的结果集数量为:"+ecsAuditLogs.size());
		List<EcsAuditLogVO> list = null;
		try {
			pageSet(ecsAuditLogs, ecsAuditLogVo);
			list = (List<EcsAuditLogVO>) beansCopy(ecsAuditLogs, EcsAuditLogVO.class);
		} catch (Exception e) {
			logger.error("数据转换出现异常!", e);
		}
		
		return list;
	
	}


	/**
	 * 查询当前机构及下属机构权限数据
	 */
	@SuppressWarnings("unchecked")
	public List<EcsAuditLogVO> queryAllCurrDownOrg(EcsAuditLogVO ecsAuditLogVo) {

		logger.debug("当前查询本人所属机构及以下数据信息的参数信息为:");
		List<EcsAuditLog> ecsAuditLogs = ecsAuditLogDao.queryAllCurrDownOrgByPage(ecsAuditLogVo);
		logger.debug("当前查询本人所属机构及以下数据信息的结果集数量为:"+ ecsAuditLogs.size());
		List<EcsAuditLogVO> list = null;
		try {
			pageSet(ecsAuditLogs, ecsAuditLogVo);
			list = (List<EcsAuditLogVO>) beansCopy(ecsAuditLogs, EcsAuditLogVO.class);
		} catch (Exception e) {
			logger.error("数据转换出现异常!", e);
		}
		
		return list;
	
	}
}
