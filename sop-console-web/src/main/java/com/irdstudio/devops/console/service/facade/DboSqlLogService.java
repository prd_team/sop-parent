package com.irdstudio.devops.console.service.facade;

import java.util.List;

import com.irdstudio.bfp.console.service.vo.SSubsDatasourceVO;
import com.irdstudio.devops.console.service.vo.DboSqlLogVO;

/**
 * <p>Description:数据运维日志				<p>
 * @author ligm
 * @date 2020-11-04
 */
public interface DboSqlLogService {
	
	public List<DboSqlLogVO> queryAllOwner(DboSqlLogVO dboSqlLogVo);
	
	public List<DboSqlLogVO> queryAllCurrOrg(DboSqlLogVO dboSqlLogVo);
	
	public List<DboSqlLogVO> queryAllCurrDownOrg(DboSqlLogVO dboSqlLogVo);
	
	public int insertDboSqlLog(DboSqlLogVO inDboSqlLogVo);
	
	public int deleteByPk(DboSqlLogVO dboSqlLogVo);
	
	public int updateByPk(DboSqlLogVO dboSqlLogVo);
	
	public DboSqlLogVO queryByPk(DboSqlLogVO dboSqlLogVo);

	public String sqlConnection(SSubsDatasourceVO inSSubsDatasourceVo);

	public List<String> getTableName(SSubsDatasourceVO inSSubsDatasourceVo);

	public List<String> doExcetueSql(SSubsDatasourceVO sSubsDatasourceVo,String remark, String sql);

}
