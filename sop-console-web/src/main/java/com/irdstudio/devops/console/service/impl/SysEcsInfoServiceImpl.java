package com.irdstudio.devops.console.service.impl;

import java.util.List;
import java.util.Objects;

import com.irdstudio.bfp.console.dao.domain.SubsCodeBase;
import com.irdstudio.devops.console.dao.SysEcsInfoDao;
import com.irdstudio.devops.console.dao.domain.EcsBaseInfo;
import com.irdstudio.devops.console.service.vo.EcsBaseInfoVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.irdstudio.devops.console.service.facade.SysEcsInfoService;
import com.irdstudio.devops.console.dao.domain.SysEcsInfo;
import com.irdstudio.devops.console.service.vo.SysEcsInfoVO;
import com.irdstudio.sdk.beans.core.base.FrameworkService;
/**
 * <p>ServiceImpl: 系统服务器信息				<p>
 * @author zjj
 * @date 2020-10-27
 */
@Service("sysEcsInfoServiceImpl")
public class SysEcsInfoServiceImpl implements SysEcsInfoService, FrameworkService {
	
	private static Logger logger = LoggerFactory.getLogger(SysEcsInfoServiceImpl.class);

	@Autowired
	private SysEcsInfoDao sysEcsInfoDao;
	
	/**
	 * 新增操作
	 */
	public int insertSysEcsInfo(SysEcsInfoVO inSysEcsInfoVo) {
		logger.debug("当前新增数据为:"+ inSysEcsInfoVo.toString());
		int num = 0;
		try {
			SysEcsInfo sysEcsInfo = new SysEcsInfo();
			beanCopy(inSysEcsInfoVo, sysEcsInfo);
			num = sysEcsInfoDao.insertSysEcsInfo(sysEcsInfo);
		} catch (Exception e) {
			logger.error("新增数据发生异常!", e);
			num = -1;
		}
		logger.debug("当前新增数据条数为:"+ num);
		return num;
	}

	/**
	 * 删除操作
	 */
	public int deleteByPk(SysEcsInfoVO inSysEcsInfoVo) {
		logger.debug("当前删除数据条件为:"+ inSysEcsInfoVo);
		int num = 0;
		try {
			SysEcsInfo sysEcsInfo = new SysEcsInfo();
			beanCopy(inSysEcsInfoVo, sysEcsInfo);
			num = sysEcsInfoDao.deleteByPk(sysEcsInfo);
		} catch (Exception e) {
			logger.error("删除数据发生异常!", e);
			num = -1;
		}
		logger.debug("根据条件:"+ inSysEcsInfoVo +"删除的数据条数为"+ num);
		return num;
	}


	/**
	 * 更新操作
	 */
	public int updateByPk(SysEcsInfoVO inSysEcsInfoVo) {
		logger.debug("当前修改数据为:"+ inSysEcsInfoVo.toString());
		int num = 0;
		try {
			SysEcsInfo sysEcsInfo = new SysEcsInfo();
			beanCopy(inSysEcsInfoVo, sysEcsInfo);
			num = sysEcsInfoDao.updateByPk(sysEcsInfo);
		} catch (Exception e) {
			logger.error("修改数据发生异常!", e);
			num = -1;
		}
		logger.debug("根据条件:"+ inSysEcsInfoVo +"修改的数据条数为"+ num);
		return num;
	}

	/**
	 *更新subsCode
	 * @return
	 */
	@Override
	public int updateBySubsCode(SubsCodeBase subsCodeBase) {
		logger.debug("当前修改数据为:"+ subsCodeBase.toString());
		int num = 0;
		try {
			num = sysEcsInfoDao.updateBySubsCode(subsCodeBase);
		} catch (Exception e) {
			logger.error("修改数据发生异常!", e);
			num = -1;
		}
		logger.debug("根据条件:"+ subsCodeBase.getOldSubsCode() +"修改的数据条数为"+ num);
		return 0;
	}

	/**
	 * 查询操作
	 */
	public SysEcsInfoVO queryByPk(SysEcsInfoVO inSysEcsInfoVo) {
		
		logger.debug("当前查询参数信息为:"+ inSysEcsInfoVo);
		try {
			SysEcsInfo querySysEcsInfo = new SysEcsInfo();
			beanCopy(inSysEcsInfoVo, querySysEcsInfo);
			SysEcsInfo queryRslSysEcsInfo = sysEcsInfoDao.queryByPk(querySysEcsInfo);
			if (Objects.nonNull(queryRslSysEcsInfo)) {
				SysEcsInfoVO outSysEcsInfoVo = beanCopy(queryRslSysEcsInfo, new SysEcsInfoVO());
				logger.debug("当前查询结果为:"+ outSysEcsInfoVo.toString());
				return outSysEcsInfoVo;
			} else {
				logger.debug("当前查询结果为空!");
			}
		} catch (Exception e) {
			logger.error("查询数据发生异常!", e);
		}
		return null;
	}


	/**
	 * 查询用户权限数据
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<EcsBaseInfoVO> queryAllOwner(SysEcsInfoVO sysEcsInfoVo) {

		logger.debug("当前查询本人所属数据信息的参数信息为:");
		List<EcsBaseInfoVO> list = null;
		try {
			List<EcsBaseInfo> ecsBaseInfos = sysEcsInfoDao.queryAllOwnerByPage(sysEcsInfoVo);
			logger.debug("当前查询本人所属数据信息的结果集数量为:"+ ecsBaseInfos.size());
			pageSet(ecsBaseInfos, sysEcsInfoVo);
			list = (List<EcsBaseInfoVO>) beansCopy(ecsBaseInfos, EcsBaseInfoVO.class);
		} catch (Exception e) {
			logger.error("数据转换出现异常!", e);
		}
		
		return list;
	
	}


	/**
	 * 查询当前机构权限数据
	 */
	@SuppressWarnings("unchecked")
	public List<SysEcsInfoVO> queryAllCurrOrg(SysEcsInfoVO sysEcsInfoVo) {

		logger.debug("当前查询本人所属机构数据信息的参数信息为:");
		List<SysEcsInfo> sysEcsInfos = sysEcsInfoDao.queryAllCurrOrgByPage(sysEcsInfoVo);
		logger.debug("当前查询本人所属机构数据信息的结果集数量为:"+sysEcsInfos.size());
		List<SysEcsInfoVO> list = null;
		try {
			pageSet(sysEcsInfos, sysEcsInfoVo);
			list = (List<SysEcsInfoVO>) beansCopy(sysEcsInfos, SysEcsInfoVO.class);
		} catch (Exception e) {
			logger.error("数据转换出现异常!", e);
		}
		
		return list;
	
	}


	/**
	 * 查询当前机构及下属机构权限数据
	 */
	@SuppressWarnings("unchecked")
	public List<SysEcsInfoVO> queryAllCurrDownOrg(SysEcsInfoVO sysEcsInfoVo) {

		logger.debug("当前查询本人所属机构及以下数据信息的参数信息为:");
		List<SysEcsInfo> sysEcsInfos = sysEcsInfoDao.queryAllCurrDownOrgByPage(sysEcsInfoVo);
		logger.debug("当前查询本人所属机构及以下数据信息的结果集数量为:"+ sysEcsInfos.size());
		List<SysEcsInfoVO> list = null;
		try {
			pageSet(sysEcsInfos, sysEcsInfoVo);
			list = (List<SysEcsInfoVO>) beansCopy(sysEcsInfos, SysEcsInfoVO.class);
		} catch (Exception e) {
			logger.error("数据转换出现异常!", e);
		}
		
		return list;
	
	}
}
