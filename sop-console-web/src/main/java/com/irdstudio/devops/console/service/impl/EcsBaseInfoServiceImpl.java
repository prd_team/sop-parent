package com.irdstudio.devops.console.service.impl;

import java.util.List;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.irdstudio.devops.console.dao.EcsBaseInfoDao;
import com.irdstudio.devops.console.dao.domain.EcsBaseInfo;
import com.irdstudio.devops.console.service.facade.EcsBaseInfoService;
import com.irdstudio.devops.console.service.vo.EcsBaseInfoVO;
import com.irdstudio.sdk.beans.core.base.FrameworkService;

/**
 * <p>ServiceImpl: 服务器信息				<p>
 * @author ligm
 * @date 2019-03-21
 */
@Service("ecsBaseInfoServiceImpl")
public class EcsBaseInfoServiceImpl implements EcsBaseInfoService, FrameworkService {
	
	private static Logger logger = LoggerFactory.getLogger(EcsBaseInfoServiceImpl.class);

	@Autowired
	private EcsBaseInfoDao ecsBaseInfoDao;
	
	/**
	 * 新增操作
	 */
	public int insertEcsBaseInfo(EcsBaseInfoVO inEcsBaseInfoVo) {
		logger.debug("当前新增数据为:"+ inEcsBaseInfoVo.toString());
		int num = 0;
		try {
			EcsBaseInfo ecsBaseInfo = new EcsBaseInfo();
			beanCopy(inEcsBaseInfoVo, ecsBaseInfo);
			num = ecsBaseInfoDao.insertEcsBaseInfo(ecsBaseInfo);
		} catch (Exception e) {
			logger.error("新增数据发生异常!", e);
			num = -1;
		}
		logger.debug("当前新增数据条数为:"+ num);
		return num;
	}

	/**
	 * 删除操作
	 */
	public int deleteByPk(EcsBaseInfoVO inEcsBaseInfoVo) {
		logger.debug("当前删除数据条件为:"+ inEcsBaseInfoVo);
		int num = 0;
		try {
			EcsBaseInfo ecsBaseInfo = new EcsBaseInfo();
			beanCopy(inEcsBaseInfoVo, ecsBaseInfo);
			num = ecsBaseInfoDao.deleteByPk(ecsBaseInfo);
		} catch (Exception e) {
			logger.error("删除数据发生异常!", e);
			num = -1;
		}
		logger.debug("根据条件:"+ inEcsBaseInfoVo +"删除的数据条数为"+ num);
		return num;
	}


	/**
	 * 更新操作
	 */
	public int updateByPk(EcsBaseInfoVO inEcsBaseInfoVo) {
		logger.debug("当前修改数据为:"+ inEcsBaseInfoVo.toString());
		int num = 0;
		try {
			EcsBaseInfo ecsBaseInfo = new EcsBaseInfo();
			beanCopy(inEcsBaseInfoVo, ecsBaseInfo);
			num = ecsBaseInfoDao.updateByPk(ecsBaseInfo);
		} catch (Exception e) {
			logger.error("修改数据发生异常!", e);
			num = -1;
		}
		logger.debug("根据条件:"+ inEcsBaseInfoVo +"修改的数据条数为"+ num);
		return num;
	}
	
	/**
	 * 查询操作
	 */
	public EcsBaseInfoVO queryByPk(EcsBaseInfoVO inEcsBaseInfoVo) {
		
		logger.debug("当前查询参数信息为:"+ inEcsBaseInfoVo);
		try {
			EcsBaseInfo queryEcsBaseInfo = new EcsBaseInfo();
			beanCopy(inEcsBaseInfoVo, queryEcsBaseInfo);
			EcsBaseInfo queryRslEcsBaseInfo = ecsBaseInfoDao.queryByPk(queryEcsBaseInfo);
			if (Objects.nonNull(queryRslEcsBaseInfo)) {
				EcsBaseInfoVO outEcsBaseInfoVo = beanCopy(queryRslEcsBaseInfo, new EcsBaseInfoVO());
				logger.debug("当前查询结果为:"+ outEcsBaseInfoVo.toString());
				return outEcsBaseInfoVo;
			} else {
				logger.debug("当前查询结果为空!");
			}
		} catch (Exception e) {
			logger.error("查询数据发生异常!", e);
		}
		return null;
	}


	/**
	 * 查询用户权限数据
	 */
	@SuppressWarnings("unchecked")
	public List<EcsBaseInfoVO> queryAllOwner(EcsBaseInfoVO ecsBaseInfoVo) {

		logger.debug("当前查询本人所属数据信息的参数信息为:");
		List<EcsBaseInfoVO> list = null;
		try {
			List<EcsBaseInfo> ecsBaseInfos = ecsBaseInfoDao.queryAllOwnerByPage(ecsBaseInfoVo);
			logger.debug("当前查询本人所属数据信息的结果集数量为:"+ ecsBaseInfos.size());
			pageSet(ecsBaseInfos, ecsBaseInfoVo);
			list = (List<EcsBaseInfoVO>) beansCopy(ecsBaseInfos, EcsBaseInfoVO.class);
		} catch (Exception e) {
			logger.error("数据转换出现异常!", e);
		}
		
		return list;
	
	}


	/**
	 * 查询当前机构权限数据
	 */
	@SuppressWarnings("unchecked")
	public List<EcsBaseInfoVO> queryAllCurrOrg(EcsBaseInfoVO ecsBaseInfoVo) {

		logger.debug("当前查询本人所属机构数据信息的参数信息为:");
		List<EcsBaseInfo> ecsBaseInfos = ecsBaseInfoDao.queryAllCurrOrgByPage(ecsBaseInfoVo);
		logger.debug("当前查询本人所属机构数据信息的结果集数量为:"+ecsBaseInfos.size());
		List<EcsBaseInfoVO> list = null;
		try {
			pageSet(ecsBaseInfos, ecsBaseInfoVo);
			list = (List<EcsBaseInfoVO>) beansCopy(ecsBaseInfos, EcsBaseInfoVO.class);
		} catch (Exception e) {
			logger.error("数据转换出现异常!", e);
		}
		
		return list;
	
	}


	/**
	 * 查询当前机构及下属机构权限数据
	 */
	@SuppressWarnings("unchecked")
	public List<EcsBaseInfoVO> queryAllCurrDownOrg(EcsBaseInfoVO ecsBaseInfoVo) {

		logger.debug("当前查询本人所属机构及以下数据信息的参数信息为:");
		List<EcsBaseInfo> ecsBaseInfos = ecsBaseInfoDao.queryAllCurrDownOrgByPage(ecsBaseInfoVo);
		logger.debug("当前查询本人所属机构及以下数据信息的结果集数量为:"+ ecsBaseInfos.size());
		List<EcsBaseInfoVO> list = null;
		try {
			pageSet(ecsBaseInfos, ecsBaseInfoVo);
			list = (List<EcsBaseInfoVO>) beansCopy(ecsBaseInfos, EcsBaseInfoVO.class);
		} catch (Exception e) {
			logger.error("数据转换出现异常!", e);
		}
		
		return list;
	
	}
}
