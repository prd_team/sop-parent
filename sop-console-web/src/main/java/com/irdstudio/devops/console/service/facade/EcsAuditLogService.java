package com.irdstudio.devops.console.service.facade;

import java.util.List;

import com.irdstudio.devops.console.service.vo.EcsAuditLogVO;

/**
 * <p>Description:服务器操作日志				<p>
 * @author ligm
 * @date 2018-12-31
 */
public interface EcsAuditLogService {
	
	public List<EcsAuditLogVO> queryAllOwner(EcsAuditLogVO ecsAuditLogVo);
	
	public List<EcsAuditLogVO> queryAllCurrOrg(EcsAuditLogVO ecsAuditLogVo);
	
	public List<EcsAuditLogVO> queryAllCurrDownOrg(EcsAuditLogVO ecsAuditLogVo);
	
	public int insertEcsAuditLog(EcsAuditLogVO inEcsAuditLogVo);
	
	public int deleteByPk(EcsAuditLogVO ecsAuditLogVo);
	
	public int updateByPk(EcsAuditLogVO ecsAuditLogVo);
	
	public EcsAuditLogVO queryByPk(EcsAuditLogVO ecsAuditLogVo);

}
