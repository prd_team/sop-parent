package com.irdstudio.devops.console.common.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 文件处理工具类
 * 
 * @author fangmingfei1991@163.com
 * @since 2019年4月11日 下午10:59:45
 * @version 1.0
 */
public class FilePubUtil {
	private static Logger logger = LoggerFactory.getLogger(FilePubUtil.class);

	/**
	 * 删除文件，文件不存在时返回true
	 * 
	 * @param file
	 *            文件
	 * @return 删除成功返回true
	 * @author fangmingfei1991@163.com
	 * @since 2019年4月11日 下午11:02:10
	 * @date 2019年4月11日 下午11:02:10
	 * @version 0.1
	 */
	public static final boolean deleteFile(File file) {
		logger.debug("删除文件....[{}]。", file.getPath());
		if (!file.exists()) {
			return true;
		}
		return file.delete();
	}

	/**
	 * 删除目录
	 * 
	 * @param dir
	 *            目录
	 * @return
	 * @author fangmingfei1991@163.com
	 * @since 2019年4月11日 下午11:03:50
	 * @date 2019年4月11日 下午11:03:50
	 * @version 0.1
	 */
	public static final boolean deleteDir(File dir) {
		if(!getDirFiles(dir).isEmpty()) {
			logger.debug("目录[{}]不为空，不能删除。",dir.getPath());
			return false;
		}
		return dir.delete();
	}

	/**
	 * 获取文件目录下的所有文件
	 * 
	 * @param dir
	 * @return 文件列表
	 * @author fangmingfei1991@163.com
	 * @since 2019年4月11日 下午11:06:03
	 * @date 2019年4月11日 下午11:06:03
	 * @version 0.1
	 */
	public static final List<File> getDirFiles(File dir) {
		List<File> fileList = new ArrayList<File>();
		if (dir.exists() && dir.isDirectory()) {
			File[] files = dir.listFiles();
			for (File file : files) {
				logger.debug("file:{}",file.getPath());
				fileList.add(file);
			}
		}
		return fileList;
	}

	private FilePubUtil() {
		
	}

	public static void main(String[] args) {
		String path = "F:\\投标保函\\file";
		FilePubUtil.deleteDir(new File(path));
	}
}
