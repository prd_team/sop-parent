package com.irdstudio.devops.console.dao;

import com.irdstudio.devops.console.dao.domain.HomePageInfo;
import com.irdstudio.devops.console.dao.domain.OngoingProcessInfo;

import java.util.List;

/**
 * @author lwc
 * @Description TODO
 * @createTime 2020年11月25日
 */
public interface HomePageInfoDao {
    /**
     * 查询主页业务系统数据
     * @return
     */
    List<HomePageInfo> homePageQueryAllByPage(HomePageInfo param);

    /**
     * 查询主页正在执行的运维流程
     * @param bpaState
     * @return
     */
    List<OngoingProcessInfo> ongoingProcessQueryAllByPage(OngoingProcessInfo param);
}
