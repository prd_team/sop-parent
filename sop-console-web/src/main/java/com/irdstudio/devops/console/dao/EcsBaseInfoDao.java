package com.irdstudio.devops.console.dao;

import java.util.List;

import com.irdstudio.devops.console.dao.domain.EcsBaseInfo;
import com.irdstudio.devops.console.service.vo.EcsBaseInfoVO;
/**
 * <p>DAO interface:服务器信息				<p>
 * @author ligm
 * @date 2019-03-21
 */
public interface EcsBaseInfoDao {
	
	public int insertEcsBaseInfo(EcsBaseInfo ecsBaseInfo);
	
	public int deleteByPk(EcsBaseInfo ecsBaseInfo);
	
	public int updateByPk(EcsBaseInfo ecsBaseInfo);
	
	public EcsBaseInfo queryByPk(EcsBaseInfo ecsBaseInfo);
	
	public List<EcsBaseInfo> queryAllOwnerByPage(EcsBaseInfoVO ecsBaseInfo);
	
	public List<EcsBaseInfo> queryAllCurrOrgByPage(EcsBaseInfoVO ecsBaseInfo);
	
	public List<EcsBaseInfo> queryAllCurrDownOrgByPage(EcsBaseInfoVO ecsBaseInfo);

}
