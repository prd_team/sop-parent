package com.irdstudio.devops.console.dao;

import java.util.List;

import com.irdstudio.devops.console.dao.domain.EcsAuditLog;
import com.irdstudio.devops.console.service.vo.EcsAuditLogVO;
/**
 * <p>DAO interface:服务器操作日志				<p>
 * @author ligm
 * @date 2018-12-31
 */
public interface EcsAuditLogDao {
	
	public int insertEcsAuditLog(EcsAuditLog ecsAuditLog);
	
	public int deleteByPk(EcsAuditLog ecsAuditLog);
	
	public int updateByPk(EcsAuditLog ecsAuditLog);
	
	public EcsAuditLog queryByPk(EcsAuditLog ecsAuditLog);
	
	public List<EcsAuditLog> queryAllOwnerByPage(EcsAuditLogVO ecsAuditLog);
	
	public List<EcsAuditLog> queryAllCurrOrgByPage(EcsAuditLogVO ecsAuditLog);
	
	public List<EcsAuditLog> queryAllCurrDownOrgByPage(EcsAuditLogVO ecsAuditLog);

}
