package com.irdstudio.devops.console.dao.domain;

import com.irdstudio.sdk.beans.core.vo.BaseInfo;
/**
 * Description: 系统应用信息			
 * @author zjj
 * @date 2020-10-27
 */
public class SysAppsInfo extends BaseInfo{

	private static final long serialVersionUID = 1L;	
	
	/** 应用标识 */
	private String appId;
	/** 应用代码 */
	private String appCode;
	/** 应用名称 */
	private String appName;
	/** 架构类型 */
	private String archType;
	/** 应用版本 */
	private String appVersion;
	/** 应用排列顺序 */
	private Integer appOrder;
	/** GIT仓库路径 */
	private String gitUrl;
	/** JDK版本 */
	private String jdkVersion;
	/** 应用说明 */
	private String appDesc;
	/** 所属系统 */
	private String subsId;
	/** 创建人 */
	private String createUser;
	/** 创建时间 */
	private String createTime;
	/** 最后修改人 */
	private String lastUpdateUser;
	/** 最后更新时间 */
	private String lastUpdateTime;
	

	public void setAppId(String appId){
		this.appId = appId;
	}
	public String getAppId(){
		return this.appId;
	}		
	public void setAppCode(String appCode){
		this.appCode = appCode;
	}
	public String getAppCode(){
		return this.appCode;
	}		
	public void setAppName(String appName){
		this.appName = appName;
	}
	public String getAppName(){
		return this.appName;
	}		
	public void setArchType(String archType){
		this.archType = archType;
	}
	public String getArchType(){
		return this.archType;
	}		
	public void setAppVersion(String appVersion){
		this.appVersion = appVersion;
	}
	public String getAppVersion(){
		return this.appVersion;
	}		
	public void setAppOrder(Integer appOrder){
		this.appOrder = appOrder;
	}
	public Integer getAppOrder(){
		return this.appOrder;
	}		
	public void setGitUrl(String gitUrl){
		this.gitUrl = gitUrl;
	}
	public String getGitUrl(){
		return this.gitUrl;
	}		
	public void setJdkVersion(String jdkVersion){
		this.jdkVersion = jdkVersion;
	}
	public String getJdkVersion(){
		return this.jdkVersion;
	}		
	public void setAppDesc(String appDesc){
		this.appDesc = appDesc;
	}
	public String getAppDesc(){
		return this.appDesc;
	}		
	public void setSubsId(String subsId){
		this.subsId = subsId;
	}
	public String getSubsId(){
		return this.subsId;
	}		
	public void setCreateUser(String createUser){
		this.createUser = createUser;
	}
	public String getCreateUser(){
		return this.createUser;
	}		
	public void setCreateTime(String createTime){
		this.createTime = createTime;
	}
	public String getCreateTime(){
		return this.createTime;
	}		
	public void setLastUpdateUser(String lastUpdateUser){
		this.lastUpdateUser = lastUpdateUser;
	}
	public String getLastUpdateUser(){
		return this.lastUpdateUser;
	}		
	public void setLastUpdateTime(String lastUpdateTime){
		this.lastUpdateTime = lastUpdateTime;
	}
	public String getLastUpdateTime(){
		return this.lastUpdateTime;
	}		

}
