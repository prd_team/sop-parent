package com.irdstudio.devops.console.api.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.irdstudio.devops.console.service.facade.EcsPortInfoService;
import com.irdstudio.devops.console.service.vo.EcsPortInfoVO;
import com.irdstudio.sdk.beans.core.vo.ResponseData;
import com.irdstudio.sdk.beans.web.controller.AbstractController;

@RestController
@RequestMapping("/api")
public class EcsPortInfoController extends AbstractController  {
	
	@Autowired
	@Qualifier("ecsPortInfoServiceImpl")
	private EcsPortInfoService ecsPortInfoService;

	
	/**
	 * 列表数据查询
	 * @param page
	 * @param size
	 * @return
	 */
	@RequestMapping(value="/ecs/port/infos", method=RequestMethod.POST)
	public @ResponseBody ResponseData<List<EcsPortInfoVO>> queryEcsPortInfoAll(
			EcsPortInfoVO vo) {		
		List<EcsPortInfoVO> outputVo = ecsPortInfoService.queryAllOwner(vo);
		return getResponseData(outputVo);
		
	}
	
	/**
	 * 根据主键查询详情
	 * @return
	 */
	@RequestMapping(value="/ecs/port/info/{ecsId}", method=RequestMethod.GET)
	public @ResponseBody ResponseData<EcsPortInfoVO> queryByPk(@PathVariable("ecsId") String ecsId) {		
		EcsPortInfoVO inVo = new EcsPortInfoVO();
				inVo.setEcsId(ecsId);
		EcsPortInfoVO outputVo = ecsPortInfoService.queryByPk(inVo);
		return getResponseData(outputVo);
		
	}
	
	/**
	 * 根据主键删除信息
	 * @param ecsPortInfo
	 * @return
	 */
	@RequestMapping(value="/ecs/port/info", method=RequestMethod.DELETE)
	public @ResponseBody ResponseData<Integer> deleteByPk(@RequestBody EcsPortInfoVO inEcsPortInfoVo) {		
		int outputVo = ecsPortInfoService.deleteByPk(inEcsPortInfoVo);
		return getResponseData(outputVo);
		
	}
	
	/**
	 * 根据主键更新信息
	 * @param inEcsPortInfoVo
	 * @return
	 */
	@RequestMapping(value="/ecs/port/info", method=RequestMethod.PUT)
	public @ResponseBody ResponseData<Integer> updateByPk(@RequestBody EcsPortInfoVO inEcsPortInfoVo) {		
		int outputVo = ecsPortInfoService.updateByPk(inEcsPortInfoVo);
		return getResponseData(outputVo);
		
	}
	
	/**
	 * 新增数据
	 * @param inEcsPortInfoVo
	 * @return
	 */
	@RequestMapping(value="/ecs/port/info", method=RequestMethod.POST)
	public @ResponseBody ResponseData<Integer> insertEcsPortInfo(@RequestBody EcsPortInfoVO inEcsPortInfoVo) {
		int outputVo = ecsPortInfoService.insertEcsPortInfo(inEcsPortInfoVo);
		return getResponseData(outputVo);
		
	}
}
