package com.irdstudio.devops.console.service.impl;

import com.irdstudio.devops.console.dao.HomePageInfoDao;
import com.irdstudio.devops.console.dao.domain.HomePageInfo;
import com.irdstudio.devops.console.dao.domain.OngoingProcessInfo;
import com.irdstudio.devops.console.service.facade.HomePageInfoService;
import com.irdstudio.devops.console.service.vo.HomePageInfoVO;
import com.irdstudio.devops.console.service.vo.OngoingProcessInfoVO;
import com.irdstudio.sdk.beans.core.base.FrameworkService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author lwc
 * @Description TODO
 * @createTime 2020年11月25日
 */
@Service("homePageInfoServiceImpl")
public class HomePageInfoServiceImpl implements HomePageInfoService, FrameworkService {

    private static Logger logger = LoggerFactory.getLogger(HomePageInfoServiceImpl.class);
    @Autowired
    private HomePageInfoDao homePageInfoDao;
    @Override
    public List<HomePageInfoVO> homePageQueryAll(HomePageInfo param) {
        List<HomePageInfo> homePageInfos = homePageInfoDao.homePageQueryAllByPage(param);
        logger.debug("当前查询首页业务系统数据结果集数量为:"+homePageInfos.size());
        List<HomePageInfoVO> list = null;
        try {
            pageSet(homePageInfos, param);
            list = (List<HomePageInfoVO>) beansCopy(homePageInfos, HomePageInfoVO.class);
        } catch (Exception e) {
            logger.error("数据转换出现异常!", e);
        }
        return list;
    }

    @Override
    public List<OngoingProcessInfoVO> ongoingProcessQueryAll(String bpaState) {
        logger.debug("当前查询正在执行的运维流程参数bpaState:"+bpaState);

        OngoingProcessInfo param = new OngoingProcessInfo();
        param.setBpaState(bpaState);
        List<OngoingProcessInfo> ongoingProcessInfos = homePageInfoDao.ongoingProcessQueryAllByPage(param);
        logger.debug("当前查询正在执行的运维流程数据结果集为:"+ongoingProcessInfos.size());
        List<OngoingProcessInfoVO> list =null;
        try {
            OngoingProcessInfoVO ongoingProcessInfoVO = new OngoingProcessInfoVO();
            pageSet(ongoingProcessInfos,ongoingProcessInfoVO);
            list = (List<OngoingProcessInfoVO>)beansCopy(ongoingProcessInfos,OngoingProcessInfoVO.class);
        }catch (Exception e){
            logger.error("数据转换出现异常!", e);
        }
        return list;
    }
}
