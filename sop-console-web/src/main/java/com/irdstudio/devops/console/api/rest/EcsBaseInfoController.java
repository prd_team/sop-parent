package com.irdstudio.devops.console.api.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.irdstudio.devops.console.service.facade.EcsBaseInfoService;
import com.irdstudio.devops.console.service.vo.EcsBaseInfoVO;
import com.irdstudio.sdk.beans.core.vo.ResponseData;
import com.irdstudio.sdk.beans.web.controller.AbstractController;

@RestController
@RequestMapping("/api")
public class EcsBaseInfoController extends AbstractController  {
	
	@Autowired
	@Qualifier("ecsBaseInfoServiceImpl")
	private EcsBaseInfoService ecsBaseInfoService;

	
	/**
	 * 列表数据查询
	 * @param page
	 * @param size
	 * @return
	 */
	@RequestMapping(value="/ecs/base/infos", method=RequestMethod.POST)
	public @ResponseBody ResponseData<List<EcsBaseInfoVO>> queryEcsBaseInfoAll(
			EcsBaseInfoVO vo) {		
		List<EcsBaseInfoVO> outputVo = ecsBaseInfoService.queryAllOwner(vo);
		return getResponseData(outputVo);
		
	}
	
	/**
	 * 根据主键查询详情
	 * @return
	 */
	@RequestMapping(value="/ecs/base/info/{ecsId}", method=RequestMethod.GET)
	public @ResponseBody ResponseData<EcsBaseInfoVO> queryByPk(@PathVariable("ecsId") String ecsId) {		
		EcsBaseInfoVO inVo = new EcsBaseInfoVO();
				inVo.setEcsId(ecsId);
		EcsBaseInfoVO outputVo = ecsBaseInfoService.queryByPk(inVo);
		return getResponseData(outputVo);
		
	}
	
	/**
	 * 根据主键删除信息
	 * @param ecsBaseInfo
	 * @return
	 */
	@RequestMapping(value="/ecs/base/info", method=RequestMethod.DELETE)
	public @ResponseBody ResponseData<Integer> deleteByPk(@RequestBody EcsBaseInfoVO inEcsBaseInfoVo) {		
		int outputVo = ecsBaseInfoService.deleteByPk(inEcsBaseInfoVo);
		return getResponseData(outputVo);
		
	}
	
	/**
	 * 根据主键更新信息
	 * @param inEcsBaseInfoVo
	 * @return
	 */
	@RequestMapping(value="/ecs/base/info", method=RequestMethod.PUT)
	public @ResponseBody ResponseData<Integer> updateByPk(@RequestBody EcsBaseInfoVO inEcsBaseInfoVo) {		
		int outputVo = ecsBaseInfoService.updateByPk(inEcsBaseInfoVo);
		return getResponseData(outputVo);
		
	}
	
	/**
	 * 新增数据
	 * @param inEcsBaseInfoVo
	 * @return
	 */
	@RequestMapping(value="/ecs/base/info", method=RequestMethod.POST)
	public @ResponseBody ResponseData<Integer> insertEcsBaseInfo(@RequestBody EcsBaseInfoVO inEcsBaseInfoVo) {
		int outputVo = ecsBaseInfoService.insertEcsBaseInfo(inEcsBaseInfoVo);
		return getResponseData(outputVo);
		
	}
}
