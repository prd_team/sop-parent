package com.irdstudio.devops.console.dao.domain;


import com.irdstudio.sdk.beans.core.vo.BaseInfo;

import java.math.BigDecimal;

/**
 * Description: 数据运维日志			
 * @author ligm
 * @date 2020-11-04
 */
public class DboSqlLog extends BaseInfo {

	private static final long serialVersionUID = 1L;	
	
	/** 数据记录标识 */
	private String recordKeyid;
	/** 数据运维语句 */
	private String dboSqlContent;
	/** 数据运维执行结果 */
	private String dboResult;
	/** 数据运维影响记录数 */
	private Integer dboAffectedNum;
	/** 数据运维错误信息 */
	private String dboErrMsg;
	/** 交易开始时间 */
	private String startTime;
	/** 交易结束时间 */
	private String endTime;
	/** 执行时长（秒） */
	private BigDecimal costTime;
	/** 操作人 */
	private String operUserid;
	/** 数据源代码 */
	private String dsCode;
	/** 数据运维备注 */
	private String dboRemarks;
	

	public void setRecordKeyid(String recordKeyid){
		this.recordKeyid = recordKeyid;
	}
	public String getRecordKeyid(){
		return this.recordKeyid;
	}		
	public void setDboSqlContent(String dboSqlContent){
		this.dboSqlContent = dboSqlContent;
	}
	public String getDboSqlContent(){
		return this.dboSqlContent;
	}		
	public void setDboResult(String dboResult){
		this.dboResult = dboResult;
	}
	public String getDboResult(){
		return this.dboResult;
	}		
	public void setDboAffectedNum(Integer dboAffectedNum){
		this.dboAffectedNum = dboAffectedNum;
	}
	public Integer getDboAffectedNum(){
		return this.dboAffectedNum;
	}		
	public void setDboErrMsg(String dboErrMsg){
		this.dboErrMsg = dboErrMsg;
	}
	public String getDboErrMsg(){
		return this.dboErrMsg;
	}		
	public void setStartTime(String startTime){
		this.startTime = startTime;
	}
	public String getStartTime(){
		return this.startTime;
	}		
	public void setEndTime(String endTime){
		this.endTime = endTime;
	}
	public String getEndTime(){
		return this.endTime;
	}		
	public void setCostTime(BigDecimal costTime){
		this.costTime = costTime;
	}
	public BigDecimal getCostTime(){
		return this.costTime;
	}		
	public void setOperUserid(String operUserid){
		this.operUserid = operUserid;
	}
	public String getOperUserid(){
		return this.operUserid;
	}		
	public void setDsCode(String dsCode){
		this.dsCode = dsCode;
	}
	public String getDsCode(){
		return this.dsCode;
	}		
	public void setDboRemarks(String dboRemarks){
		this.dboRemarks = dboRemarks;
	}
	public String getDboRemarks(){
		return this.dboRemarks;
	}		

}
