package com.irdstudio.devops.console.common.constant;
/**
 * 
  * TODO 字典项常量类位置
  * @author fangmingfei1991@163.com
  * @since 2019年3月27日 上午9:21:55
  * @version 1.0
 */
public final class SDicConstant {
	
	/** =========== 版本更新方式  ============  */
	/** 在原先版本的基础上，最后一位版本号加一  */
	public static final String VERSION_CHANGETYPE_SMALL="small";
	/** 在原先版本的基础上，中间一位版本号加一  */
	public static final String VERSION_CHANGETYPE_MIDDLE="middle";
	/** 在原先版本的基础上，前面一位版本号加一  */
	public static final String VERSION_CHANGETYPE_BIG="big";
	/** =========== 版本更新方式  ============  */
	
	/** =========== 是否  ============  */
	/** 是 */
	public static final String STD_PUB_Y = "Y";
	/** 否 */
	public static final String STD_PUB_N = "N";
	/** =========== 是否  ============  */
	
	
	/** 防止被new */
	private SDicConstant() {
		
	}

}
