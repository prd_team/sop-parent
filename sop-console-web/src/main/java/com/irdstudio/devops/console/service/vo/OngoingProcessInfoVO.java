package com.irdstudio.devops.console.service.vo;

import com.irdstudio.sdk.beans.core.vo.BaseInfo;

/**
 * @author lwc
 * @Description TODO
 * @createTime 2020年11月25日
 */
public class OngoingProcessInfoVO extends BaseInfo {
    private static final long serialVersionUID = 1L;

    /** 流程编号*/
    private String bpaId;
    /** 流程名称*/
    private String bpaName;
    /** 所属系统*/
    private String subsCode;
    /** 流程状态*/
    private String bpaState;
    /** 流程启动时间*/
    private String startTime;
    /** 执行节点*/
    private String agentId;

    public String getBpaId() {
        return bpaId;
    }

    public void setBpaId(String bpaId) {
        this.bpaId = bpaId;
    }

    public String getBpaName() {
        return bpaName;
    }

    public void setBpaName(String bpaName) {
        this.bpaName = bpaName;
    }

    public String getSubsCode() {
        return subsCode;
    }

    public void setSubsCode(String subsCode) {
        this.subsCode = subsCode;
    }

    public String getBpaState() {
        return bpaState;
    }

    public void setBpaState(String bpaState) {
        this.bpaState = bpaState;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    @Override
    public String toString() {
        return "OngoingProcessInfoVO{" +
                "bpaId='" + bpaId + '\'' +
                ", bpaName='" + bpaName + '\'' +
                ", subsCode='" + subsCode + '\'' +
                ", bpaState='" + bpaState + '\'' +
                ", startTime='" + startTime + '\'' +
                ", agentId='" + agentId + '\'' +
                '}';
    }
}
