package com.irdstudio.devops.console.dao.domain;

import com.irdstudio.sdk.beans.core.vo.BaseInfo;

public class DboBackupConf extends BaseInfo {
    private static final long serialVersionUID = 1L;
    /** 数据库IP */
    private String baseIP;
    /** 数据库名称 */
    private String databaseName;
    /** 用户名 */
    private String username;
    /** 用户密码 */
    private String password;
    /** 备份路径 */
    private String savePath;
    /** 定时任务编号 */
    private String jobCode;
    /** 定时任务编号 */
    private String subsId;

    public String getBaseIP() { return baseIP; }
    public void setBaseIP(String baseIP) { this.baseIP = baseIP; }
    public String getDatabaseName() { return databaseName; }
    public void setDatabaseName(String databaseName) { this.databaseName = databaseName; }
    public String getUsername() { return username; }
    public void setUsername(String username) { this.username = username; }
    public String getPassword() { return password; }
    public void setPassword(String password) { this.password = password; }
    public String getSavePath() { return savePath; }
    public void setSavePath(String savePath) { this.savePath = savePath; }
    public String getJobCode() { return jobCode; }
    public void setJobCode(String jobCode) { this.jobCode = jobCode; }
    public String getSubsId() { return subsId; }
    public void setSubsId(String subsId) { this.subsId = subsId; }
}
