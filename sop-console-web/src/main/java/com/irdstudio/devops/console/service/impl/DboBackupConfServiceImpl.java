package com.irdstudio.devops.console.service.impl;

import com.irdstudio.bfp.console.dao.domain.SubsCodeBase;
import com.irdstudio.devops.console.dao.DboBackupConfDao;
import com.irdstudio.devops.console.dao.domain.DboBackupConf;
import com.irdstudio.devops.console.service.facade.DboBackupConfService;
import com.irdstudio.devops.console.service.vo.DboBackupConfVO;
import com.irdstudio.sdk.beans.core.base.FrameworkService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service("dboBackupConfServiceImpl")
public class DboBackupConfServiceImpl implements DboBackupConfService, FrameworkService {

    private static Logger logger = LoggerFactory.getLogger(DboBackupConfServiceImpl.class);

    @Autowired
    private DboBackupConfDao dboBackupConfDao;

    /**
     * 新增操作
     */
    public int insertDboBackupConf(DboBackupConfVO inDboBackupConfVo) {
        logger.debug("当前新增数据为:"+ inDboBackupConfVo.toString());
        int num = 0;
        try {
            DboBackupConf dboBackupConf  = new DboBackupConf();
            beanCopy(inDboBackupConfVo, dboBackupConf);
            num = dboBackupConfDao.insertDboBackupConf(dboBackupConf);
        } catch (Exception e) {
            logger.error("新增数据发生异常!", e);
            num = -1;
        }
        logger.debug("当前新增数据条数为:"+ num);
        return num;
    }

    /**
     * 删除操作
     */
    public int deleteByPk(DboBackupConfVO inDboBackupConfVo) {
        logger.debug("当前删除数据条件为:"+ inDboBackupConfVo);
        int num = 0;
        try {
            DboBackupConf dboBackupConf = new DboBackupConf();
            beanCopy(inDboBackupConfVo, dboBackupConf);
            num = dboBackupConfDao.deleteByPk(dboBackupConf);
        } catch (Exception e) {
            logger.error("删除数据发生异常!", e);
            num = -1;
        }
        logger.debug("根据条件:"+ inDboBackupConfVo +"删除的数据条数为"+ num);
        return num;
    }


    /**
     * 更新操作
     */
    public int updateByPk(DboBackupConfVO inDboBackupConfVo) {
        logger.debug("当前修改数据为:"+ inDboBackupConfVo.toString());
        int num = 0;
        try {
            DboBackupConf dboBackupConf = new DboBackupConf();
            beanCopy(inDboBackupConfVo, dboBackupConf);
            num = dboBackupConfDao.updateByPk(dboBackupConf);
        } catch (Exception e) {
            logger.error("修改数据发生异常!", e);
            num = -1;
        }
        logger.debug("根据条件:"+ inDboBackupConfVo +"修改的数据条数为"+ num);
        return num;
    }

    /**
     *更新subsCode
     * @return
     */
    @Override
    public int updateBySubsCode(SubsCodeBase subsCodeBase) {
        logger.debug("当前修改数据为:"+ subsCodeBase.toString());
        int num = 0;
        try {
            num = dboBackupConfDao.updateBySubsCode(subsCodeBase);
        } catch (Exception e) {
            logger.error("修改数据发生异常!", e);
            num = -1;
        }
        logger.debug("根据条件:"+ subsCodeBase.getOldSubsCode() +"修改的数据条数为"+ num);
        return 0;
    }

    /**
     * 查询操作
     */
    public DboBackupConfVO queryByPk(DboBackupConfVO inDboBackupConfVo) {

        logger.debug("当前查询参数信息为:"+ inDboBackupConfVo);
        try {
            DboBackupConf queryDboBackupConf  = new DboBackupConf();
            beanCopy(inDboBackupConfVo, queryDboBackupConf);
            DboBackupConf queryRslDboBackupConf = dboBackupConfDao.queryByPk(queryDboBackupConf);
            if (Objects.nonNull(queryRslDboBackupConf)) {
                DboBackupConfVO outDboBackupConfVo = beanCopy(queryRslDboBackupConf, new DboBackupConfVO());
                logger.debug("当前查询结果为:"+ outDboBackupConfVo.toString());
                return outDboBackupConfVo;
            } else {
                logger.debug("当前查询结果为空!");
            }
        } catch (Exception e) {
            logger.error("查询数据发生异常!", e);
        }
        return null;
    }


    /**
     * 查询用户权限数据
     */
    @SuppressWarnings("unchecked")
    public List<DboBackupConfVO> queryAllOwner(DboBackupConfVO dboBackupConfVo) {

        logger.debug("当前查询本人所属数据信息的参数信息为:");
        List<DboBackupConfVO> list = null;
        try {
            List<DboBackupConf> dboBackupConfs = dboBackupConfDao.queryAllOwnerByPage(dboBackupConfVo);
            logger.debug("当前查询本人所属数据信息的结果集数量为:"+ dboBackupConfs.size());
            pageSet(dboBackupConfs, dboBackupConfVo);
            list = (List<DboBackupConfVO>) beansCopy(dboBackupConfs, DboBackupConfVO.class);
        } catch (Exception e) {
            logger.error("数据转换出现异常!", e);
        }

        return list;

    }


    /**
     * 查询当前机构权限数据
     */
    @SuppressWarnings("unchecked")
    public List<DboBackupConfVO> queryAllCurrOrg(DboBackupConfVO dboBackupConfVO) {

        logger.debug("当前查询本人所属机构数据信息的参数信息为:");
        List<DboBackupConf> dboBackupConfs = dboBackupConfDao.queryAllCurrOrgByPage(dboBackupConfVO);
        logger.debug("当前查询本人所属机构数据信息的结果集数量为:"+dboBackupConfs.size());
        List<DboBackupConfVO> list = null;
        try {
            pageSet(dboBackupConfs, dboBackupConfVO);
            list = (List<DboBackupConfVO>) beansCopy(dboBackupConfs, DboBackupConfVO.class);
        } catch (Exception e) {
            logger.error("数据转换出现异常!", e);
        }

        return list;

    }


    /**
     * 查询当前机构及下属机构权限数据
     */
    @SuppressWarnings("unchecked")
    public List<DboBackupConfVO> queryAllCurrDownOrg(DboBackupConfVO dboBackupConfVo) {

        logger.debug("当前查询本人所属机构及以下数据信息的参数信息为:");
        List<DboBackupConf> dboBackupConfs = dboBackupConfDao.queryAllCurrDownOrgByPage(dboBackupConfVo);
        logger.debug("当前查询本人所属机构及以下数据信息的结果集数量为:"+ dboBackupConfs.size());
        List<DboBackupConfVO> list = null;
        try {
            pageSet(dboBackupConfs, dboBackupConfVo);
            list = (List<DboBackupConfVO>) beansCopy(dboBackupConfs, DboBackupConfVO.class);
        } catch (Exception e) {
            logger.error("数据转换出现异常!", e);
        }

        return list;

    }
}
