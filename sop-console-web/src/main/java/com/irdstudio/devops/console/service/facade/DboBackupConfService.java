package com.irdstudio.devops.console.service.facade;

import com.irdstudio.bfp.console.dao.domain.SubsCodeBase;
import com.irdstudio.devops.console.service.vo.DboBackupConfVO;

import java.util.List;

public interface DboBackupConfService {
    public List<DboBackupConfVO> queryAllOwner(DboBackupConfVO dboBackupConfVo);

    public List<DboBackupConfVO> queryAllCurrOrg(DboBackupConfVO dboBackupConfVo);

    public List<DboBackupConfVO> queryAllCurrDownOrg(DboBackupConfVO dboBackupConfVo);

    public int insertDboBackupConf(DboBackupConfVO inDboSqlLogVo);

    public int deleteByPk(DboBackupConfVO dboBackupConfVo);

    public int updateByPk(DboBackupConfVO dboBackupConfVo);

    public int updateBySubsCode(SubsCodeBase subsCodeBase);

    public DboBackupConfVO queryByPk(DboBackupConfVO dboBackupConfVo);
}
