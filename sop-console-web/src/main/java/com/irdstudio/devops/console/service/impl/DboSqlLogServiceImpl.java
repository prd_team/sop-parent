package com.irdstudio.devops.console.service.impl;

import java.util.*;

import com.irdstudio.bfp.console.service.vo.SSubsDatasourceVO;
import com.irdstudio.devops.console.dao.SSubsDatasourceDao;
import com.irdstudio.sdk.beans.core.base.FrameworkService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.irdstudio.devops.console.service.facade.DboSqlLogService;
import com.irdstudio.devops.console.dao.DboSqlLogDao;
import com.irdstudio.devops.console.dao.domain.DboSqlLog;
import com.irdstudio.devops.console.service.vo.DboSqlLogVO;
import com.irdstudio.bfp.console.dao.domain.SSubsDatasource;
/**
 * <p>ServiceImpl: 数据运维日志				<p>
 * @author ligm
 * @date 2020-11-04
 */
@Service("dboSqlLogServiceImpl")
public class DboSqlLogServiceImpl implements DboSqlLogService, FrameworkService {
	
	private static Logger logger = LoggerFactory.getLogger(DboSqlLogServiceImpl.class);

	@Autowired
	private DboSqlLogDao dboSqlLogDao;
	@Autowired
	private com.irdstudio.bfp.console.dao.SSubsDatasourceDao bfpSSubsDatasourceDao;

	/**
	 * 新增操作
	 */
	public int insertDboSqlLog(DboSqlLogVO inDboSqlLogVo) {
		logger.debug("当前新增数据为:"+ inDboSqlLogVo.toString());
		int num = 0;
		try {
			DboSqlLog dboSqlLog = new DboSqlLog();
			beanCopy(inDboSqlLogVo, dboSqlLog);
			num = dboSqlLogDao.insertDboSqlLog(dboSqlLog);
		} catch (Exception e) {
			logger.error("新增数据发生异常!", e);
			num = -1;
		}
		logger.debug("当前新增数据条数为:"+ num);
		return num;
	}

	/**
	 * 删除操作
	 */
	public int deleteByPk(DboSqlLogVO inDboSqlLogVo) {
		logger.debug("当前删除数据条件为:"+ inDboSqlLogVo);
		int num = 0;
		try {
			DboSqlLog dboSqlLog = new DboSqlLog();
			beanCopy(inDboSqlLogVo, dboSqlLog);
			num = dboSqlLogDao.deleteByPk(dboSqlLog);
		} catch (Exception e) {
			logger.error("删除数据发生异常!", e);
			num = -1;
		}
		logger.debug("根据条件:"+ inDboSqlLogVo +"删除的数据条数为"+ num);
		return num;
	}


	/**
	 * 更新操作
	 */
	public int updateByPk(DboSqlLogVO inDboSqlLogVo) {
		logger.debug("当前修改数据为:"+ inDboSqlLogVo.toString());
		int num = 0;
		try {
			DboSqlLog dboSqlLog = new DboSqlLog();
			beanCopy(inDboSqlLogVo, dboSqlLog);
			num = dboSqlLogDao.updateByPk(dboSqlLog);
		} catch (Exception e) {
			logger.error("修改数据发生异常!", e);
			num = -1;
		}
		logger.debug("根据条件:"+ inDboSqlLogVo +"修改的数据条数为"+ num);
		return num;
	}
	
	/**
	 * 查询操作
	 */
	public DboSqlLogVO queryByPk(DboSqlLogVO inDboSqlLogVo) {
		
		logger.debug("当前查询参数信息为:"+ inDboSqlLogVo);
		try {
			DboSqlLog queryDboSqlLog = new DboSqlLog();
			beanCopy(inDboSqlLogVo, queryDboSqlLog);
			DboSqlLog queryRslDboSqlLog = dboSqlLogDao.queryByPk(queryDboSqlLog);
			if (Objects.nonNull(queryRslDboSqlLog)) {
				DboSqlLogVO outDboSqlLogVo = beanCopy(queryRslDboSqlLog, new DboSqlLogVO());
				logger.debug("当前查询结果为:"+ outDboSqlLogVo.toString());
				return outDboSqlLogVo;
			} else {
				logger.debug("当前查询结果为空!");
			}
		} catch (Exception e) {
			logger.error("查询数据发生异常!", e);
		}
		return null;
	}

	/**
	 * 数据库连接测试
	 */
	@Override
	public String sqlConnection(SSubsDatasourceVO sSubsDatasourceVo) {
		try {
			SSubsDatasource sSubsDatasource = new SSubsDatasource();
			this.beanCopy(sSubsDatasourceVo, sSubsDatasource);
			SSubsDatasource rslSSubsDatasource=bfpSSubsDatasourceDao.queryByPk(sSubsDatasource);
			if (Objects.nonNull(sSubsDatasource)) {
				SSubsDatasourceDao sSubsDatasourceDao=new SSubsDatasourceDao();
				return sSubsDatasourceDao.sqlConnection(rslSSubsDatasource);
			}
			logger.debug("当前查询结果为空!");
		} catch (Exception var5) {
			logger.error("查询数据发生异常!", var5);
		}
		return  null;
	}

	@Override
	public List<String> getTableName(SSubsDatasourceVO sSubsDatasourceVo) {
		try {
			SSubsDatasource sSubsDatasource = new SSubsDatasource();
			this.beanCopy(sSubsDatasourceVo, sSubsDatasource);
			SSubsDatasource rslSSubsDatasource=bfpSSubsDatasourceDao.queryByPk(sSubsDatasource);
			if (Objects.nonNull(sSubsDatasource)) {
				SSubsDatasourceDao sSubsDatasourceDao=new SSubsDatasourceDao();
				return sSubsDatasourceDao.getTableName(rslSSubsDatasource);
			}
			logger.debug("当前查询结果为空!");
		} catch (Exception var5) {
			logger.error("查询数据发生异常!", var5);
		}
		return  null;
	}

	/**
	 * 执行运维SQL语句
	 */
	@Override
	public List<String> doExcetueSql(SSubsDatasourceVO sSubsDatasourceVo,String remark, String sql) {
		try {
			SSubsDatasource sSubsDatasource = new SSubsDatasource();
			this.beanCopy(sSubsDatasourceVo, sSubsDatasource);
			SSubsDatasource rslSSubsDatasource=bfpSSubsDatasourceDao.queryByPk(sSubsDatasource);
			if (Objects.nonNull(sSubsDatasource)) {
				SSubsDatasourceDao sSubsDatasourceDao=new SSubsDatasourceDao();
				return sSubsDatasourceDao.doExcetueSql(rslSSubsDatasource,remark,sql);
			}
			logger.debug("当前查询结果为空!");
		} catch (Exception var5) {
			logger.error("查询数据发生异常!", var5);
		}
		return  null;
	}

	/**
	 * 查询用户权限数据
	 */
	@SuppressWarnings("unchecked")
	public List<DboSqlLogVO> queryAllOwner(DboSqlLogVO dboSqlLogVo) {

		logger.debug("当前查询本人所属数据信息的参数信息为:");
		List<DboSqlLogVO> list = null;
		try {
			List<DboSqlLog> dboSqlLogs = dboSqlLogDao.queryAllOwnerByPage(dboSqlLogVo);
			logger.debug("当前查询本人所属数据信息的结果集数量为:"+ dboSqlLogs.size());
			pageSet(dboSqlLogs, dboSqlLogVo);
			list = (List<DboSqlLogVO>) beansCopy(dboSqlLogs, DboSqlLogVO.class);
		} catch (Exception e) {
			logger.error("数据转换出现异常!", e);
		}
		
		return list;
	
	}


	/**
	 * 查询当前机构权限数据
	 */
	@SuppressWarnings("unchecked")
	public List<DboSqlLogVO> queryAllCurrOrg(DboSqlLogVO dboSqlLogVo) {

		logger.debug("当前查询本人所属机构数据信息的参数信息为:");
		List<DboSqlLog> dboSqlLogs = dboSqlLogDao.queryAllCurrOrgByPage(dboSqlLogVo);
		logger.debug("当前查询本人所属机构数据信息的结果集数量为:"+dboSqlLogs.size());
		List<DboSqlLogVO> list = null;
		try {
			pageSet(dboSqlLogs, dboSqlLogVo);
			list = (List<DboSqlLogVO>) beansCopy(dboSqlLogs, DboSqlLogVO.class);
		} catch (Exception e) {
			logger.error("数据转换出现异常!", e);
		}
		
		return list;
	
	}


	/**
	 * 查询当前机构及下属机构权限数据
	 */
	@SuppressWarnings("unchecked")
	public List<DboSqlLogVO> queryAllCurrDownOrg(DboSqlLogVO dboSqlLogVo) {

		logger.debug("当前查询本人所属机构及以下数据信息的参数信息为:");
		List<DboSqlLog> dboSqlLogs = dboSqlLogDao.queryAllCurrDownOrgByPage(dboSqlLogVo);
		logger.debug("当前查询本人所属机构及以下数据信息的结果集数量为:"+ dboSqlLogs.size());
		List<DboSqlLogVO> list = null;
		try {
			pageSet(dboSqlLogs, dboSqlLogVo);
			list = (List<DboSqlLogVO>) beansCopy(dboSqlLogs, DboSqlLogVO.class);
		} catch (Exception e) {
			logger.error("数据转换出现异常!", e);
		}
		
		return list;
	
	}
}
