package com.irdstudio.devops.console.dao;

import com.irdstudio.bfp.console.dao.domain.SubsCodeBase;
import com.irdstudio.devops.console.dao.domain.DboBackupConf;
import com.irdstudio.devops.console.dao.domain.DboSqlLog;
import com.irdstudio.devops.console.service.vo.DboBackupConfVO;
import com.irdstudio.devops.console.service.vo.DboSqlLogVO;

import java.util.List;

/**
 * <p>DAO interface:数据运维配置				<p>
 * @author
 * @date 2020-11-08
 */
public interface DboBackupConfDao {

    public int insertDboBackupConf(DboBackupConf DboBackupConf);

    public int deleteByPk(DboBackupConf DboBackupConf);

    public int updateByPk(DboBackupConf DboBackupConf);

    public DboBackupConf queryByPk(DboBackupConf DboBackupConf);

    public int updateBySubsCode(SubsCodeBase subsCodeBase);

    public List<DboBackupConf> queryAllOwnerByPage(DboBackupConfVO DboBackupConf);

    public List<DboBackupConf> queryAllCurrOrgByPage(DboBackupConfVO DboBackupConf);

    public List<DboBackupConf> queryAllCurrDownOrgByPage(DboBackupConfVO DboBackupConf);

}