package com.irdstudio.devops.console.api.rest;

import java.util.List;

import com.irdstudio.bfp.console.service.vo.SSubsDatasourceVO;
import com.irdstudio.sdk.beans.core.vo.ResponseData;
import com.irdstudio.sdk.beans.web.controller.AbstractController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.irdstudio.devops.console.service.facade.DboSqlLogService;
import com.irdstudio.devops.console.service.vo.DboSqlLogVO;

@RestController
@RequestMapping("/api")
public class DboSqlLogController extends AbstractController {
	
	@Autowired
	@Qualifier("dboSqlLogServiceImpl")
	private DboSqlLogService dboSqlLogService;

	
	/**
	 * 列表数据查询
	 * @param
	 * @param dsCode
	 * @return
	 */
	@RequestMapping(value="/dbo/sql/logs/{dsCode}", method=RequestMethod.POST)
	public @ResponseBody
	ResponseData<List<DboSqlLogVO>> queryDboSqlLogAll(
			DboSqlLogVO vo,@PathVariable("dsCode") String dsCode) {
		vo.setDsCode(dsCode);
		List<DboSqlLogVO> outputVo = dboSqlLogService.queryAllOwner(vo);
		return getResponseData(outputVo);
		
	}
	
	/**
	 * 根据主键查询详情
	 * @return
	 */
	@RequestMapping(value="/dbo/sql/log/{recordKeyid}", method=RequestMethod.GET)
	public @ResponseBody ResponseData<DboSqlLogVO> queryByPk(@PathVariable("recordKeyid") String recordKeyid) {		
		DboSqlLogVO inVo = new DboSqlLogVO();
				inVo.setRecordKeyid(recordKeyid);
		DboSqlLogVO outputVo = dboSqlLogService.queryByPk(inVo);
		return getResponseData(outputVo);
		
	}
	
	/**
	 * 根据主键删除信息
	 * @param inDboSqlLogVo
	 * @return
	 */
	@RequestMapping(value="/dbo/sql/log", method=RequestMethod.DELETE)
	public @ResponseBody ResponseData<Integer> deleteByPk(@RequestBody DboSqlLogVO inDboSqlLogVo) {		
		int outputVo = dboSqlLogService.deleteByPk(inDboSqlLogVo);
		return getResponseData(outputVo);
		
	}
	
	/**
	 * 根据主键更新信息
	 * @param inDboSqlLogVo
	 * @return
	 */
	@RequestMapping(value="/dbo/sql/log", method=RequestMethod.PUT)
	public @ResponseBody ResponseData<Integer> updateByPk(@RequestBody DboSqlLogVO inDboSqlLogVo) {		
		int outputVo = dboSqlLogService.updateByPk(inDboSqlLogVo);
		return getResponseData(outputVo);
		
	}
	
	/**
	 * 新增数据
	 * @param inDboSqlLogVo
	 * @return
	 */
	@RequestMapping(value="/dbo/sql/log", method=RequestMethod.POST)
	public @ResponseBody ResponseData<Integer> insertDboSqlLog(@RequestBody DboSqlLogVO inDboSqlLogVo) {
		int outputVo = dboSqlLogService.insertDboSqlLog(inDboSqlLogVo);
		return getResponseData(outputVo);
		
	}

	/**
	 * 新增数据
	 * @param inSSubsDatasourceVo
	 * @return
	 */
	@RequestMapping(value="/dbo/sql/do/connection", method=RequestMethod.POST)
	public @ResponseBody ResponseData<String> sqlConnection(SSubsDatasourceVO inSSubsDatasourceVo) {
		String outputVo = dboSqlLogService.sqlConnection(inSSubsDatasourceVo);
		return getResponseData(outputVo);

	}

	/**
	 * 根据sql语句执行
	 * @param sSubsDatasourceVo
	 * @return
	 */
	@RequestMapping(value="/dbo/sql/run/{remark}/{sql}", method=RequestMethod.POST)
	public @ResponseBody
	ResponseData<List<String>> doExcetueSql(SSubsDatasourceVO sSubsDatasourceVo,@PathVariable("remark") String remark, @PathVariable("sql") String sql) {
		List<String> outputVo = dboSqlLogService.doExcetueSql(sSubsDatasourceVo,remark,sql);
		return getResponseData(outputVo);
	}

	/**
	 * 根据sql语句执行
	 * @param sSubsDatasourceVo
	 * @return
	 */
	@RequestMapping(value="/dbo/sql/get/table", method=RequestMethod.POST)
	public @ResponseBody
	ResponseData<List<String>> getTableName(SSubsDatasourceVO sSubsDatasourceVo) {
		List<String> outputVo = dboSqlLogService.getTableName(sSubsDatasourceVo);
		return getResponseData(outputVo);
	}
}
