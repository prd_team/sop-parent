package com.irdstudio.devops.console.api.rest;

import com.irdstudio.bfp.console.dao.domain.SubsCodeBase;
import com.irdstudio.devops.console.service.facade.DboBackupConfService;
import com.irdstudio.devops.console.service.vo.DboBackupConfVO;
import com.irdstudio.sdk.beans.core.vo.ResponseData;
import com.irdstudio.sdk.beans.web.controller.AbstractController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class DboBackupConfController extends AbstractController {

    @Autowired
    @Qualifier("dboBackupConfServiceImpl")
    private DboBackupConfService dboBackupConfService;

    /**
     * 列表数据查询
     * @param page
     * @param size
     * @return
     */
    @RequestMapping(value="/dbo/backup/confs/{subsCode}", method=RequestMethod.POST)
    public @ResponseBody
    ResponseData<List<DboBackupConfVO>> queryDboBackupConfAll(
            DboBackupConfVO vo,@PathVariable("subsCode") String subsCode) {
        vo.setSubsId(subsCode);
        List<DboBackupConfVO> outputVo = dboBackupConfService.queryAllOwner(vo);
        return getResponseData(outputVo);

    }

    /**
     * 根据主键查询详情
     * @return
     */
    @RequestMapping(value="/dbo/backup/conf/{jobCode}", method=RequestMethod.GET)
    public @ResponseBody ResponseData<DboBackupConfVO> queryByPk(@PathVariable("jobCode") String jobCode) {
        DboBackupConfVO inVo = new DboBackupConfVO();
        inVo.setJobCode(jobCode);
        DboBackupConfVO outputVo = dboBackupConfService.queryByPk(inVo);
        return getResponseData(outputVo);

    }

    /**
     * 根据主键删除信息
     * @param dboSqlLog
     * @return
     */
    @RequestMapping(value="/dbo/backup/conf", method=RequestMethod.DELETE)
    public @ResponseBody ResponseData<Integer> deleteByPk(@RequestBody DboBackupConfVO inDboBackupConfVo) {
        int outputVo = dboBackupConfService.deleteByPk(inDboBackupConfVo);
        return getResponseData(outputVo);

    }

    /**
     * 根据主键更新信息
     * @param inDboBackupConfVo
     * @return
     */
    @RequestMapping(value="/dbo/backup/conf", method= RequestMethod.PUT)
    public @ResponseBody
    ResponseData<Integer> updateByPk(@RequestBody DboBackupConfVO inDboBackupConfVo) {
        int outputVo = dboBackupConfService.updateByPk(inDboBackupConfVo);
        return getResponseData(outputVo);

    }

    /**
     * 更新subsCode
     * @return
     */
    @RequestMapping(value="/dbo/backup/conf/change/subs/code", method=RequestMethod.PUT)
    public @ResponseBody ResponseData<Integer> updateBySubsCode(@RequestBody SubsCodeBase subsCodeBase) {
        int outputVo = dboBackupConfService.updateBySubsCode(subsCodeBase);
        return getResponseData(outputVo);
    }

    /**
     * 新增数据
     * @param inDboBackupConfVo
     * @return
     */
    @RequestMapping(value="/dbo/backup/conf", method=RequestMethod.POST)
    public @ResponseBody ResponseData<Integer> insertDboBackupConf(@RequestBody DboBackupConfVO inDboBackupConfVo) {
        int outputVo = dboBackupConfService.insertDboBackupConf(inDboBackupConfVo);
        return getResponseData(outputVo);

    }
}
