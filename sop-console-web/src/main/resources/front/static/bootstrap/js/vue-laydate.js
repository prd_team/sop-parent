(function(Vue, laydate){
    let ROOT = window.location.pathname.substr(0, window.location.pathname.indexOf("/", 1)) || window.location.origin;
    if (ROOT.startsWith("/page")) {
        ROOT = window.location.origin;
    }
    let laydateSrc = ROOT + "/static/bootstrap/js/laydate/laydate-map.js";
    laydateSrc = laydateSrc.substring(0, laydateSrc.lastIndexOf('/') + 1);
    laydate.path = laydateSrc;

    Vue.component("b-laydate", {
        name:"b-laydate",
        props:{
            name: String,
            type: {
                type: String,
                default: "date"
            },
            format: {
                type : String,
                default: "yyyy-MM-dd"
            },
            placeholder: String,
            value: String,
            readonly: Boolean,
            disabled: Boolean,
        },
        data: function data(){
            return {
                localValue : this.value
            };
        },
        watch: {
            value(newValue, oldVale){
                this.localValue = newValue;
            }
        },
        methods:{
            onInput(val, old) {
                this.localValue = val;
                this.$emit('input', this.localValue)
            },
            laydateDone(value, date, endDate) {
                this.onInput(value, this.localValue);
            }
        },
        created(){
            console.log("vue lay date render")
        },
        mounted(){
            let self = this;
            laydate.render({
                elem: this.$el,
                type: this.type,
                format: this.format,
                done: function(value, date, endDate){
                    self.laydateDone(value, date, endDate)
                }
            });
        },
        destroyed(){

        },
        render(h){
            let self = this;
            return h('b-form-input',
                {
                    props: {
                        value: self.localValue,
                        name: self.name,
                        placeholder: self.placeholder,
                        disabled: self.disabled,
                        readonly: self.readonly,
                    },
                    on: {
                        input: self.onInput,
                        change: self.onInput,
                    }
                });
        }
    });
}(Vue, laydate));