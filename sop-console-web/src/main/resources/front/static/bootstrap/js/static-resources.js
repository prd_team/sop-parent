// 加载公共静态资源 js css
(function(jQuery) {
    let ROOT = window.location.pathname.substr(0, window.location.pathname.indexOf("/", 1)) || window.location.origin;
    if (ROOT.startsWith("/page")) {
        ROOT = window.location.origin;
    }
    let staticResources = [
        { url: "bootstrap/css/bootstrap.min.css", type: "css"},
        { url: "bootstrap/css/bootstrap-vue.min.css", type: "css"},
        { url: "bootstrap/js/polyfill.min.js", type: "js"},
        { url: "bootstrap/js/vue.js", type: "js"},
        { url: "bootstrap/js/bootstrap-vue.min.js", type: "js"},
        { url: "bootstrap/js/bootstrap-vue-icons.min.js", type: "js"},
        { url: "bootstrap/js/vee-validate.full.js", type: "js"},
        { url: "bootstrap/js/laydate/laydate-map.js", type: "js"},
        { url: "bootstrap/js/vue-laydate.js", type: "js"},
        { url: "/ffres/dict/all.js", type: "js"},
        { url: "bootstrap/js/jslib.util.js", type: "js"},
    ];
    function loadStaticResources(){
        if (staticResources && staticResources.length > 0) {
            staticResources.forEach((value, index) => {
                let url = value.url;
                if (url.startsWith("/")) {
                    url = url.substr(1)
                } else {
                    url = `static/${url}`;
                }
                if (value.type == 'js') {
                    let s = document.createElement("script");
                    s.type = "text/javascript";
                    s.src = `${ROOT}/${url}`;
                    $("head").append(s);
                } else if (value.type == "css"){
                    let link = document.createElement("link");
                    link.type = "text/css";
                    link.rel = "stylesheet";
                    link.href = `${ROOT}/${url}`;
                    $("head").append(link);
                }
            });
        }
    }
    loadStaticResources();
    // public vue mixin object
    window.$DEFAULT_MIXIN = {
        methods: {
            getValidationState({ dirty, validated, valid = null }) {
                return dirty || validated ? valid : null;
            },
        },
        components: {
            ValidationObserver: VeeValidate.ValidationObserver,
            ValidationProvider: VeeValidate.ValidationProvider
        }
    };
})(jQuery);