// 字典加载
var JLEUtil = JLEUtil || {};
(function(jQuery) {
	
	// 根据选项值得到选项描述
	JLEUtil.formatter_dict = function(value,dictObj){
		for(var i=0;i<dictObj.length;i++){
			if(value == dictObj[i].enname)
				return dictObj[i].cnname;
		}
		return value;
	};
	let rootPath = window.location.pathname.substr(0, window.location.pathname.indexOf("/", 1)) || window.location.origin;
    if (rootPath.startsWith("/page")) {
        rootPath = window.location.origin;
    }
	function processUrl(url, queryParam) {
		if (url.startsWith("/")) {
			url = url.substr(1)
		}
		if (url && url.indexOf('http://') != 0 && url.indexOf('https://') != 0) {
			// 不是以上下文路径为开头的，则补充上上下文路径
			url = rootPath + "/" + url;
		}
		if (queryParam) {
			url = url + "?_=" + Date.now();
			for (let k in queryParam) {
				url += `&${k}=${queryParam[k]}`
			}
		}
		return url;
	}
	/**
	 *
	 * @param url
	 * @param type POST|GET|DELETE|PUT
	 * @param queryParam url参数
	 * @param data 请示体参数
	 * @param onSuccess jqXHR.done(function( data, textStatus, jqXHR ) {});
	 * @param onError jqXHR.fail(function( jqXHR, textStatus, errorThrown ) {});
	 * @param contentType default application/x-www-form-urlencoded; charset=UTF-8
	 */
	JLEUtil.requestForm = function(url, type, queryParam,
   data,
							   onSuccess = $.noop,
							   onError = $.noop,
							   contentType = "application/x-www-form-urlencoded; charset=UTF-8"){
		url = processUrl(url, queryParam);

		let option = {
			url: url,
			type: type || "POST",
			data:data,
			contentType:contentType,
			dataType:"json",
			cache: false,
			xhrFields: { //跨域发送Ajax时，Request header中便会带上 Cookie 信息
				withCredentials: true
			},
			headers: {
				"X-Custom-Header": "Ajax"
			}
		};
		$.ajax(option).done(onSuccess).fail(onError);
	};
	/**
	 *
	 * @param url
	 * @param type POST|GET|DELETE|PUT
	 * @param queryParam url参数
	 * @param data 请示体参数
	 * @param onSuccess jqXHR.done(function( data, textStatus, jqXHR ) {});
	 * @param onError jqXHR.fail(function( jqXHR, textStatus, errorThrown ) {});
	 * @param contentType default application/json;charset=utf-8
	 */
	JLEUtil.request = function(url, type, queryParam, data,
							   onSuccess = $.noop,
							   onError = $.noop,
							   contentType = "application/json;charset=utf-8"){
		url = processUrl(url, queryParam);

		let option = {
			url: url,
			type: type || "POST",
			data:JSON.stringify(data),
			contentType:contentType,
			dataType:"json",
			cache: false,
			xhrFields: { //跨域发送Ajax时，Request header中便会带上 Cookie 信息
				withCredentials: true
			},
			headers: {
				"X-Custom-Header": "Ajax"
			}
		};
		$.ajax(option).done(onSuccess).fail(onError);
	};
	JLEUtil.linkNewPage = function (url) {
        var commonLinkObj = document.getElementById("commonLink");
        if(!commonLinkObj){
            commonLinkObj = document.createElement("a");
            commonLinkObj.target = "_blank";
        }
        commonLinkObj.href = url;
        commonLinkObj.click();
    }



})(jQuery);

$.ajaxSetup ({
	   cache: false //关闭AJAX相应的缓存
});