// 针对EASYUI进行扩展的辅助类，用于更简捷的操作EASYUI
var JLEUtil = JLEUtil || {};
(function(jQuery) {

	/**
	 * <p>
	 * <ul>提供基于iframe的dialog实现，保证引入页面的可用性</ul>
	 * </p>
	 * <ul>将iframe的高度改为99%，防止重复出现滚动条问题</ul>
	 * <ul>如果要顶层打开窗口，请使用top.JLEUtil.createwin</ul>
	 * @param options为配置参数对象{id:id,title:'标题',url:'url地址',width:800,height:300,modal:true,draggable:true,resizable:true,maximized:false,maximizable：true,toolbar}
	 * 参考dialog属性和方法
	 * id:唯一标识
	 * title:弹出窗口的标题
	 * url:弹出窗口的内容
	 * width:窗口宽度，默认600
	 * height:窗口高度，默认300
	 * modal:是否模态窗口,默认true
	 * draggable:是否可拖动，默认true
	 * resizable:是否可改变大小，默认false
	 * maximized:是否以最大化窗口的方式打开，默认true
	 * maximizable:是否有最大化窗口的按钮
	 * toolbar:工具栏
	 */
	JLEUtil.createwin=function(options) {
		var top = $(window).scrollTop();
		var opts=$.extend({
			width : 600,
			height : 300,
			modal:true,
			maximized:false,
			maximizable:true,
			onClose : function() {
				$(win).parent().removeClass('fixed-current-dialog')
				if (window.parent && !window.parent.win) {
					window.parent.win = $(window.parent.document).find('.top-dialog')[0]
				}
				$(window.parent.win).removeClass('fixed-dialog-position')
				$(this).dialog('destroy');
			}
		},options);
		if (options.url) {
			opts.content = '<iframe id="" src="" allowTransparency="true" fit="true" scrolling="auto" width="100%" height="99.5%" frameBorder="0" name=""></iframe>';
		}
		if (window.parent && !window.parent.win) {
			window.parent.win = $(window.parent.document).find('.top-dialog')[0]
		}
		if (opts.overParent && window.parent.win) {
			$(window.parent.win).addClass('fixed-dialog-position')
			options.height = options.height + 23 + 12
			$(window.parent.win).parent().css('height', options.height + 'px')
			setTimeout(function() {
				$(win).parent().addClass('fixed-current-dialog')
				$(window.win).panel('resize', {
					width: document.documentElement.offsetWidth,
					height: document.documentElement.offsetHeight
				})
			}, 50)
		}
		win=$('<div/>');
		var dia = win.dialog(opts);
		var diaWin = dia.dialog('window');
		if(opts.maximized){
			//修正以最大化窗口的方式打开时，如果父页面出现滚动条时则引起打开窗口错位的问题
			diaWin.css('top',top);
		}
		dia.find("iframe").attr("src",options.url);
		/*******************修改弹出框中button按钮的样式，开始，添加于20140902*********************/
		dia.find('.dialog-button').addClass('new_btn').css("margin-top","-20px");
		/*******************修改弹出框中button按钮的样式，结束*********************/
		return dia;
	};

	JLEUtil.createTopWin=function(options) {
		var scrollTop = $(window).scrollTop();
		var query = top.$;
		var opts=$.extend({
			width : 800,
			height : 450,
			modal:true,
			maximized:false,
			maximizable:true,
			onClose : function() {
				query(this).dialog('destroy');
			}
		},options);
		if (options.url) {
			opts.content = '<iframe id="" src="" allowTransparency="true" fit="true" scrolling="auto" width="100%" height="99.5%" frameBorder="0" name=""></iframe>';
		}
		//win=query('<div/>').appendTo('body').html(options.html);
		if (!top.win) {
			top.win = []
		}
		let localWin = query('<div class="top-dialog"/>').appendTo('body').html(options.html);
		top.win.push([localWin, window])
		localWin.dialog({width:options.width, height:options.height})
		localWin.window('center');
		var dia = localWin.dialog(opts);
		var diaWin = dia.dialog('window');
		if(opts.maximized){
			//修正以最大化窗口的方式打开时，如果父页面出现滚动条时则引起打开窗口错位的问题
			diaWin.css('top',scrollTop);
		}
		//重写top方法，使方法指向父页面
		// 判断当前页面是否为top，若是则不重写页面方法
		if(top.location!=window.location) {
			top.reloadList = function () {
				if (top.win && Array.isArray(top.win)) {
					// array from top.win
					let localWindow = top.win[top.win.length - 1][1];
					localWindow.reloadList();
					top.JLEUtil.closewin();
				} else {
					window.reloadList();
					top.JLEUtil.closewin();
				}

			}

			top.reloadTree=function(){
				if (top.win && Array.isArray(top.win)) {
					// array from top.win
					let localWindow = top.win[top.win.length -1][1];
					localWindow.reloadTree();
					top.JLEUtil.closewin();
				} else {
					window.reloadTree();
					top.JLEUtil.closewin();
				}
			}
		}

		dia.find("iframe").attr("src",options.url);
		/*******************修改弹出框中button按钮的样式，开始，添加于20140902*********************/
		dia.find('.dialog-button').addClass('new_btn').css("margin-top","-20px");
		/*******************修改弹出框中button按钮的样式，结束*********************/
		return dia;
	};

	JLEUtil.createTopModalWin=function(title,url) {
		var item=url.split("/");
		var topURL=top.document.URL.split("/");
		if(topURL.length>6 && item[0]==topURL[5]){
			url=item[1];
		}else if(topURL.length>6 && item[0]!=topURL[5]){
			url="../"+url;
		}
		var w = top.document.documentElement.offsetWidth || top.document.body.offsetWidth;
		var h = top.document.documentElement.offsetHeight || top.document.body.offsetHeight;
		var ob={title:title,url:url,width:parseInt(w*0.7),height:parseInt(h*0.7),draggable:false,modal:true};
		JLEUtil.createTopWin(ob);
	}
	JLEUtil.createTopFullModalWin=function(title,url) {
		if(top.document.URL.split("/").length>6){
			var item=url.split("/");
			url=item[1];
		}
		var w = top.document.documentElement.offsetWidth || top.document.body.offsetWidth;
		var h = top.document.documentElement.offsetHeight || top.document.body.offsetHeight;
		var ob={title:title,url:url,width:parseInt(w*1),height:parseInt(h*1),draggable:false,modal:true};
		JLEUtil.createTopWin(ob);
	}

	JLEUtil.createModalWin=function(title,url) {
		var w = document.documentElement.offsetWidth || document.body.offsetWidth;
		var h = document.documentElement.offsetHeight || document.body.offsetHeight;
		var ob={title:title,url:url,width:parseInt(w*0.7),height:parseInt(h*0.7),draggable:false,modal:true};
		JLEUtil.createwin(ob);
	};
	JLEUtil.createModalWinCustomSize=function(title,url,width,height) {
		var w = document.documentElement.offsetWidth || document.body.offsetWidth;
		var h = document.documentElement.offsetHeight || document.body.offsetHeight;
		var ob={title:title,url:url,modal:true,width:parseInt(w*width),height:parseInt(h*height),draggable:false};
		JLEUtil.createwin(ob);
	};
	JLEUtil.createListModalWin=function(title,url) {
		var w = document.documentElement.offsetWidth || document.body.offsetWidth;
		var h = document.documentElement.offsetHeight || document.body.offsetHeight;
		var ob={title:title,url:url,modal:true,width:parseInt(w*1),height:parseInt(h*1),draggable:false};
		JLEUtil.createwin(ob);
	};
	JLEUtil.createFullModalWin=function(title,url) {
		var w = document.documentElement.offsetWidth || document.body.offsetWidth;
		var h = document.documentElement.offsetHeight || document.body.offsetHeight;
		var ob={title:title,url:url,modal:true,width:parseInt(w*1),height:parseInt(h*1),draggable:false, overParent: true};
		JLEUtil.createwin(ob);
	};
	//创建领域变量窗口
	JLEUtil.createModalSelectWin=function(title,url) {
		var w = document.documentElement.offsetWidth || document.body.offsetWidth;
		var h = document.documentElement.offsetHeight || document.body.offsetHeight;
		var ob={title:title,url:url,width:parseInt(w*0.47),height:parseInt(h*0.85),draggable:false,modal:true};
		JLEUtil.createwin(ob);
	};


// 关闭打开的窗口
	JLEUtil.closewin=function(){
		try{
			if (win && Array.isArray(win)) {
				// array from top.win
				let localWin = win.pop()[0];
				localWin.dialog('close');
			} else {
				win.dialog('close');
			}
		} catch(e) {
			//ignore
			console.error(e);
		}
	};

	// 提交保存数据的AJAX请求,callback不传则默认处理
	JLEUtil.doSaveRecord=function(formName,url,callback){
		var param = $('#' + formName).serialize();
		if(!callback){
			JLEUtil.doAjax(url,param,function(data){
				$.messager.alert("提示", '记录保存成功!');
				window.parent.JLEUtil.closewin();
			});
		} else {
			JLEUtil.doAjax(url,param,callback);
		}

	};

	// 提交AJAX请求操作,successCall为交易成功后处理函数,failureCall交易失败为失败默认处理,不传则默认处理
	JLEUtil.doAjax=function(url,param,successCall,failureCall){
		$.ajax({
			type: "POST",
			url: url,
			data: param,
			async:false,
			success: function(data) {
				try {
					var jsonstr = eval("("+data+")");
					var flag = jsonstr.ec;
					if(flag=='0'){
						if(!successCall){
							$.messager.alert("提示", '操作成功!');
						} else {
							successCall(data);
						}
						return true;
					}else{
						if(!failureCall){
							$.messager.alert("提示", jsonstr.em);
						} else {
							failureCall(data);
						}
						return false;
					}
				} catch(e) {
					$.messager.alert("提示", e);
					return false;
				}
			}
		});
	};

	// 根据指定form的条件与指定的datagrid及指定的URL进行查询
	JLEUtil.queryForm=function(formName,dgName,url){
		// var queryUrl = url.indexOf("?") > 0
		// 		? url + "&" + decodeURIComponent($('#' + formName).serialize(),true)
		// 		: url + "?" + decodeURIComponent($('#' + formName).serialize(),true);
		// $('#' + dgName).datagrid('options').url = queryUrl;
		$('#' + dgName).datagrid({
			url : url,
			queryParams: $('#' + formName).serializeJson()
		});
		setTimeout(function(){
			try{
				$('#searchPanel').panel('resize');
			} catch(e) {
				// ignore 没有错就错
			}
			$('#' + dgName).datagrid('resize');
		}, 50)
	};

	// 直接根据URL来进行查询数据
	JLEUtil.queryOnly=function(dgName,url){
		$('#' + dgName).datagrid('options').url = url;
		$('#' + dgName).datagrid('load');
	};

	// 清空(重置)指定form填写的内容
	JLEUtil.clearForm=function(formName){
		$('#' + formName).form('clear');
	};

	// 根据选项值得到选项描述
	JLEUtil.formatter_dict = function(value,dictObj){
		for(var i=0;i<dictObj.length;i++){
			if(value == dictObj[i].enname)
				return dictObj[i].cnname;
		}
	};

	JLEUtil.paramUrl2Obj = function(url) {
		var search = url.split('?')[1]
		if (!search) {
			return {}
		}
		return JSON.parse('{"' + decodeURIComponent(search).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g, '":"').replace(/\n/g, '\\n') + '"}')
	};

})(jQuery);

//扩展jquery easyui tree的节点检索方法。使用方法： $("#treeId").tree("search", searchText);
(function($) {
	function init() {
		if (!$.fn.tree) {
			setTimeout(init, 500);
			return;
		}
		$.extend($.fn.tree.methods, {
			// searchText 检索的文本
			search: function(jqTree, text) {
				//easyui tree的tree对象。可以通过tree.methodName(jqTree)方式调用easyui tree的方法
				var tree = this;
				//获取所有的树节点
				var root = tree.getRoot(jqTree);
				var nodes = jqTree.tree("getData", root.target);
				var node=walkTree(jqTree,nodes.children,text);
				jqTree.tree('expandTo', node.target)
				jqTree.tree('scrollTo', node.target);
				node.target.click();
				return node;
			},
		});
	}

	function walkTree(jqTree,nodes,text){
		var result;
		for(var i in nodes){
			var node=jqTree.tree("getNode", nodes[i].target);
			if(!node.children && node.text===text){
				result = node;
			}else if(node.children){
				var child=walkTree(jqTree,node.children,text);
				if(child && child.text===text )
					result = child;
			}
		}
		return result;
	}

	init();
})(jQuery);

$.ajaxSetup ({
	cache: false //关闭AJAX相应的缓存
});